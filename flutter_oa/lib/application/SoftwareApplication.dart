import 'package:event_bus/event_bus.dart';
import 'package:provide/provide.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_sqflite/zpub_sqflite.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/engine/DBOrNetDataSourcePubConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/engine/DataSoucreEngineConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/engine/DataSourceEngine.dart';

/*
 * 类描述：软件初始化
 * 作者：郑朝军 on 2019/5/10
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改备注：
 */
class SoftwareApplication
{
  static SoftwareApplication mInstance;

  DataSoucreEngineConstant mDataSoucreEngineConstant;

  EventBus mEventBus;

  static SoftwareApplication getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new SoftwareApplication();
    }

    return mInstance;
  }

  void onCreate()
  {
    initDataSourceEngine();
    initAMapLocation();
    initDB();
    initEventBus();
//    initHttp();
    initHttpTest();
    initDeviceInfo();
  }

  /*
   * 初始化高德地图定位相关
   */
  void initAMapLocation()
  {
//    LocationManager.getInstance().initLocationAccuracy(); //.getLocations(); // 初始化一次位置
  }

  /*
   * 初始化数据源引擎
   */
  void initDataSourceEngine()
  {
    mDataSoucreEngineConstant = new DataSourceEngine();
  }

  /*
   * 初始化数据库
   */
  void initDB()
  {
    SQLiteConfigConstant.DATABASE_SQL_PATH.add("assets/sql/create_table.sql");
    SQLiteConfigConstant.DATABASE_SQL_PATH.add("assets/sql/zbcx.sql");
    SQLiteDBMethod.getInstance();
  }

  /*
   * 初始化EventBus对象
   */
  void initEventBus()
  {
    mEventBus = new EventBus();
  }

  /*
   * 初始化网络层
   */
  void initHttp()
  {
    UrlConstant.BASE_URL = "http://106.54.41.83:8080/oa";
    UrlConstant.BASE_BPM_URL = "http://106.54.41.83:8080/bpm/BpmSvr";
    UrlConstant.ENCRYPT = true;
    HttpLog.isDebug = true;
    Log.isDebug = false;
  }

  void initHttpTest()
  {
    UrlConstant.BASE_URL = "http://122.112.155.221:8083/testoa2";
    UrlConstant.BASE_BPM_URL = "http://122.112.155.221:8083/TestBpm2/BpmSvr";

    UrlConstant.ENCRYPT = false;
    HttpLog.isDebug = true;
    Log.isDebug = false;
  }

  /*
   * 初始化设备相关信息
   */
  void initDeviceInfo()
  {
//    DeviceInfoManager.getInstance();
  }

  /*
   * 初始化Providers对象
   */
  Providers initProviders()
  {
//    var providers = Providers();
//    providers.provide(Provider<ProvideFactory>.value(ProvideFactory.getInstance()));
//    return providers;
  }

  DataSoucreEngineConstant getDataSoucreEngineConstant()
  {
    return mDataSoucreEngineConstant;
  }

  DBOrNetDataSourcePubConstant getDBOrNetDataSourcePubConstant()
  {
    return mDataSoucreEngineConstant.getDbOrNetDataSourcePubConstant();
  }

  EventBus getEventBus()
  {
    return mEventBus;
  }
}
