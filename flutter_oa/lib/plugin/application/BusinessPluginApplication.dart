
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：其他模块的注册中心暂时写到此处，将来按照模块的方式依赖的时候再分开
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class BusinessPluginApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
//      if (plugin.classurl == BjbdnPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 笔记本电脑申请流程
//        PluginsFactory.getInstance().add(
//            plugin.name, new BjbdnPluginService());
//      }
    });
  }
}
