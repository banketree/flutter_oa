import 'package:flutteroa/plugin/MainPlugin.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';

import 'package:zpub_bas/zpub_bas.dart';

import '../LoginPlugin.dart';

/*
 * 类描述：公共程序启动注册模块：可以理解成Android中的AndroidManifest.xml
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class CommonPluginApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
      if (plugin.classurl == LoginPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 登录插件
        PluginsFactory.getInstance().add(plugin.name, new LoginPluginService());
      }
      else if (plugin.classurl == MainPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 主界面插件
        PluginsFactory.getInstance().add(plugin.name, new MainPluginService());
      }
//      else if (plugin.classurl == AttPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 属性插件
//        PluginsFactory.getInstance().add(plugin.name, new AttPluginService());
//      }
//      else if (plugin.classurl == AttSeePlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 属性查看插件
//        PluginsFactory.getInstance().add(plugin.name, new AttSeePluginService());
//      }
//      else if (plugin.classurl == AttSeeProcPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 属性查看流程插件
//        PluginsFactory.getInstance().add(plugin.name, new AttSeeProcPluginService());
//      }
//      else if (plugin.classurl == PhotoBrowsePlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 相册浏览插件
//        PluginsFactory.getInstance().add(
//            plugin.name, new PhotoBrowsePluginService());
//      }
//      else if (plugin.classurl == CopyPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 模版插件
//        PluginsFactory.getInstance().add(
//            plugin.name, new CopyPluginService());
//      }
//      else if (plugin.classurl == UniversalPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 通用组件，根据配置来进行生成组件的插件
//        PluginsFactory.getInstance().add(
//            plugin.name, new UniversalPluginService());
//      }
//      else if (plugin.classurl == MemberInfoPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 个人信息页
//        PluginsFactory.getInstance().add(
//            plugin.name, new MemberInfoPluginService());
//      }
//      else if (plugin.classurl == MemberResetPwdPlugin.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
//      { // 重置密码页
//        PluginsFactory.getInstance().add(
//            plugin.name, new MemberResetPwdPluginService());
//      }
    });
  }
}
