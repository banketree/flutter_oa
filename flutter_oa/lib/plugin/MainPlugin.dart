import 'package:bottom_tab_bar/bottom_tab_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutteroa/constant/PluginInitParamKeyConstant.dart';
import 'package:flutteroa/font/IconFont.dart';
import 'package:flutteroa/manager/ExitManager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 主页(框架用例) <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class MainPlugin extends PluginStatefulBase
{
  MainPlugin({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new MainPluginState();
  }

  static String toStrings()
  {
    return "MainPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class MainPluginState extends PluginBaseState<MainPlugin>
{
  /*
   * 当前选中的TAB
   */
  int mCurrentIndex;

  /*
   * 当前选中的TAB
   */
  String mSBadge = '0';

  /*
   * 底部导航栏Bar集合
   */
  List<BottomTabBarItem> mBottomNavigationBarList = [];

  /*
   * 底部导航栏Bar文字集合
   */
  List<dynamic> mNames;

  /*
   * 底部导航栏Bar图片集合
   */
  List<IconData> mBottomIcons = <IconData>[];

  /*
   * 底部导航栏Bar选[未]中颜色集合
   */
  List<Color> bottomColor = [
    Colors.blue,
    Colors.grey,
  ];

  MainPluginState()
  {
    mTitle = '欢迎 ' + UserMethod.getUser().getName();
  }

  void initState()
  {
    super.initState();
    initBottomBar();
    mCurrentIndex = mInitParaMap[PluginInitParamKeyConstant
        .PLUGIN_INIT_PARAM_MAIN_KEY_CURRENT_INDEX];
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        new WillPopScope(
            child: new IndexedStack(
              children: createChildrenWidget(),
              index: mCurrentIndex,
            ),
            onWillPop: ()
            {
              ExitManager.onBackPressed(this);
            }));
    return widget;
  }

  Widget buildOld(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        Text('测试'));
    return widget;
  }

  /*
   * 初始化底部导航拦
   */
  void initBottomBar()
  {
    Map<String, dynamic> map = mInitParaMap[PluginInitParamKeyConstant
        .PLUGIN_INIT_PARAM_MAIN_KEY_BOTTOM_ICON_TEXT];
    mNames = map.values.toList();
    map.keys.forEach((color)
    {
      mBottomIcons.add(
          new IconData(int.parse(color), fontFamily: IconFont.getFamily()));
    });
  }

  /*
   * 创建底部导航栏的函数
   */
  Widget createBottomNavigationBar()
  {
    Widget navigationBar = BottomTabBar(
      type: BottomTabBarType.fixed,
      badgeColor: Colors.red,
      items: createBottomNavigationBarItem(),
      onTap: (index)
      {
        setState(()
        {
          mCurrentIndex = index;
        });
      },);
    return navigationBar;
  }


  /*
   * 创建底部导航栏的BarItem函数
   */
  List<BottomTabBarItem> createBottomNavigationBarItem()
  {
    mBottomNavigationBarList.clear();
    for (int i = 0; i < mNames.length; i++)
    {
      mBottomNavigationBarList.add(new BottomTabBarItem(
          icon: new Icon(mBottomIcons[i], color: iconColor(i), size: 20.0),
          title: new Text(
            mNames[i].toString(),
            style: getTabTextStyle(i),
          ), badge: i == 1 && mSBadge != '0' ? WeBadge(
          child: mSBadge,
          color: Colors.red,
          textStyle: BasTextStyleRes.text_color_text1_smaller) : null));
    }
    return mBottomNavigationBarList;
  }

  /*
   * 获取颜色值
   */
  Color iconColor(int curIndex)
  {
    if (curIndex == mCurrentIndex)
    {
      return bottomColor[0];
    }
    return bottomColor[1];
  }

  /*
   * 获取样式
   */
  TextStyle getTabTextStyle(int curIndex)
  {
    if (curIndex == mCurrentIndex)
    {
      return BasTextStyleRes.text_color_text1_small;
    }
    return BasTextStyleRes.text_color_text1_small;
  }

  /*
   * 更新消息数量
   */
  void updateBadge(String badge)
  {
    setState(()
    {
      this.mSBadge = badge;
    });
  }
}


/*
 * 类描述：跳转到主界面提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class MainPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    StateManager.getInstance()
        .startWidegtStateAndRemove(new MainPlugin(), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new MainPlugin();
  }
}
