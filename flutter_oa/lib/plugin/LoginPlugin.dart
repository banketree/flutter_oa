import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutteroa/http/service/MemberService.dart';
import 'package:flutteroa/method/SlotMethod.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/InitSvr.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/MenuMethod.dart';
import 'package:zpub_third_shard/zpub_third_shard.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 功能：登录页
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class LoginPlugin extends FuctionStateFulBase
{
  State<StatefulWidget> createState()
  {
    return new LoginPluginState();
  }

  static String toStrings()
  {
    return "LoginPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class LoginPluginState extends FuctionStateBase<LoginPlugin>
{
  /*
   * 用户名输入框
   */
  TextEditingController mControllerUser = new TextEditingController();

  /*
   * 密码输入框
   */
  TextEditingController mControllerPassword = new TextEditingController();

  List<dynamic> mUser;

  LoginPluginState() : super()
  {
    mTitle = '登录';
    hideBackButton();
//    mControllerUser.text = SPUtil.get(MapKeyConstant.MAP_KEY_NAME, "");
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.fromLTRB(10, 20, 10, 10),
              decoration: new BoxDecoration(
                border:
                Border.all(color: Colors.grey.withOpacity(0.5), width: 1),
                borderRadius: BorderRadius.circular(20),
              ),
              child: new Row(
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 15.0),
                    child: Icon(
                      Icons.person_outline,
                      color: Colors.grey,
                    ),
                  ),
                  new Container(
                    height: 30.0,
                    width: 1.0,
                    color: Colors.grey.withOpacity(0.5),
                    margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                  ),
                  new Expanded(
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '请输入您的账号',
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        controller: mControllerUser,
                      ))
                ],
              ),
            ),
            new Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
              decoration: new BoxDecoration(
                border:
                Border.all(color: Colors.grey.withOpacity(0.5), width: 1),
                borderRadius: BorderRadius.circular(20),
              ),
              child: new Row(
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 15.0),
                    child: Icon(
                      Icons.lock_open,
                      color: Colors.grey,
                    ),
                  ),
                  new Container(
                    height: 30.0,
                    width: 1.0,
                    color: Colors.grey.withOpacity(0.5),
                    margin: const EdgeInsets.only(left: 00.0, right: 10.0),
                  ),
                  new Expanded(
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '请输入您的密码',
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        controller: mControllerPassword,
                        obscureText: true,
                      ))
                ],
              ),
            ),
            new Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: WeCheckboxGroup(
                  child: Row(
                      children: [
                        WeCheckbox(
                          value: '1',
                          child: Text('记住用户名'),
                        )
                      ]
                  ),
                  onChange: (value)
                  {
                    mUser = value;
                  },
                )),
            new Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
              height: 40,
              child: new RaisedButton(
                onPressed: ()
                {
                  doClickLoginButton();
                },
                child: new Text('登录'),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(28.0)),
                splashColor: Colors.white,
                color: Colors.blue,
              ),
            ),
            new GestureDetector(child: new Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child: new Align(
                  child: new Text('忘记密码？'),
                  alignment: Alignment.centerRight,
                )), onTap: ()
            {
//              StateManager.getInstance().startWidegtState(
//                  new MemberForgetPwdPlugin(), this);
            },)
          ],
        ));
    return widget;
  }

  void doClickLoginButton()
  {
    String user = mControllerUser.text;
    String password = mControllerPassword.text;
    if (CxTextUtil.isEmpty(user))
    {
      showToast("请输入用户名");
      return;
    }
    if (CxTextUtil.isEmpty(password))
    {
      showToast("请输入密码");
      return;
    }
    if (!CxTextUtil.isEmptyList(mUser))
    {
//      SPUtil.put(MapKeyConstant.MAP_KEY_NAME, user);
    }
    else
    {
//      SPUtil.put(MapKeyConstant.MAP_KEY_NAME, "");
    }
    MemberService.loginByEmail(user, password, this);
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "loginByEmail")
    {
      MemberService.queryInitSvr(values, this);
    }
    else if (method == "queryInitSvr")
    {
      InitSvr initSvr = InitSvrMethod.getInitSvr();
      if (!CxTextUtil.isEmptyList(initSvr.getPlugin()))
      {
        SlotMethod.slotAll();
        this.runPlugin();
      }
      else
      {
//        InitService.queryInitSvrPlugin(this);
      }
    }
    else if (method == "queryInitSvrPlugin")
    {
      this.runPlugin();
    }
  }


  void onNetWorkFaild(String method, Object values)
  {
    if (method == "loginByEmail")
    {
      if (values == "errPsw")
      {
        showToast("密码错误!");
      }
      else if (values == "errNoUser")
      {
        showToast("用户名不存在!");
      }
      else
      {
        super.onNetWorkFaild(method, values);
      }
    }
    else
    {
      super.onNetWorkFaild(method, values);
    }
  }

  void runPlugin()
  {
//    ProcsManager.getInstance().updateProc();
    MenuMethod.getInstance().createMenu();
    print('getInstance===${PluginsFactory.getInstance().get("main")}');
    PluginsFactory.getInstance().get("main").runPlugin(this);
  }
}


/*
 * 类描述：登录提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class LoginPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    StateManager.getInstance()
        .startWidegtStateAndRemove(new LoginPlugin(), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new LoginPlugin();
  }
}