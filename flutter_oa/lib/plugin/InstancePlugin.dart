import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutteroa/plugin/LoginPlugin.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/InitSvr.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/User.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/SysCfgManager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_third/zpub_third.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Syscfg.dart';
import 'package:zpub_third_shard/zpub_third_shard.dart';

void main()
{
  runApp(new InstancePlugin());
}

/*
 * 初始化页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class InstancePlugin extends FuctionStateFulBase
{
  State<StatefulWidget> createState()
  {
    return new InstancePluginState();
  }
}

/*
 * 页面功能 <br/>
 */
class InstancePluginState extends FuctionStateBase<InstancePlugin>
{
  /*
   * 系统版本号
   */
  String mSVersion = "";

  /*
   * 不使用入栈操作
   */
  bool usePushStack()
  {
    return false;
  }

  InstancePluginState()
  {
    resetProgressBar(text: "正在初始化...");
    hideTitleBar();
    setContentBackground(Colors.blue);
  }

  void initState()
  {
    super.initState();
    initData();
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        new Stack(
          children: <Widget>[
            new Container(
              width: ScreenUtil.getInstance().getScreenWidth(),
              height: ScreenUtil.getInstance().getScreenHeight(),
              child: new Image.asset(
                "assets/images/bg_guide.jpg",
                fit: BoxFit.fill,
              ),
            ),
            new Container(
              margin: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
              alignment: Alignment.bottomCenter,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new Text(mSVersion),
                ],
              ),
            )
          ],
        ));
    return widget;
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "querys" && values is Map)
    {
      SysCfgManager sysCfgManager = SysCfgManager.getInstance();
      List<Syscfg> syscfg = [];
      List data = values[BasMapKeyConstant.MAP_KEY_DATA];
      for(int i =0; i < data.length; i++)
      {
         dynamic item = data[i];
         syscfg.add(Syscfg.fromJson(item as  Map<String, dynamic>));
      }
      sysCfgManager.setSysCfg(syscfg);
//      SlotMethod.slotAll();
      initSvr();
    }
  }

  @override
  void onNetWorkFaild(String method, Object values)
  {
    if (method == "querys")
    {
//      StateManager.getInstance().startWidegtStateAndRemove(new LoginPlugin(), this);
    }
    else
    {
      super.onNetWorkFaild(method, values);
    }
  }

  /*
   * 初始化数据
   */
  void initData()
  {
    initVersionName();
    initForceLogin();
//    PermissionUtil.initPermission([
//      PermissionGroup.location,
//      PermissionGroup.storage,
//      PermissionGroup.phone,
//    ]);
  }

  /*
   * 初始化版本号
   */
  void initVersionName()
  {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo)
    {
      setState(()
      {
        mSVersion = packageInfo.version;
      });
    });
  }

  /*
   * 初始化强制登录
   */
  void initForceLogin()
  {
    SPUtil.init().then((sharedPreferences)
    {
      User user = UserMethod.getUser();
      if (user != null)
      {
        StateManager.getInstance().startWidegtStateAndRemove(new LoginPlugin(), this);
      }
      else
      {
        StateManager.getInstance().startWidegtStateAndRemove(new LoginPlugin(), this);
      }
    });
  }

  void initForceLoginOld()
  {
    SPUtil.init().then((sharedPreferences)
    {
      User user = UserMethod.getUser();
      if (user != null)
      {
        Map<String, dynamic> param = {
          HttpParamKeyValue.PARAM_KEY_MAJOR: SysMajMinConstant.SYS_NO_99,
          HttpParamKeyValue.PARAM_KEY_MINOR: SysMajMinConstant.MINOR_SYS_CFG,
          HttpParamKeyValue.PARAM_KEY_EXP: 'id>?',
          HttpParamKeyValue.PARAM_KEY_TYPES: 'i',
          HttpParamKeyValue.PARAM_KEY_VALS: '0',
        };
        SvrAreaSvrService.querys(this, param: param);
      }
      else
      {
        StateManager.getInstance().startWidegtStateAndRemove(new LoginPlugin(), this);
      }
    });
  }

  /*
   * 初始化缓存数据相关,如果缓存数据没有则从服务器中拉取
   */
  void initSvr()
  {
    InitSvr initSvr = InitSvrMethod.getInitSvr();
    if (initSvr != null)
    {
      initUpdateInfo();
    }
  }

  /*
   * 检查版本更新，如果版本不更新则跳转到主页面
   */
  void initUpdateInfo()
  {
//    SysCfgManager sysCfgManager = SysCfgManager.getInstance();
//    String version =
//    sysCfgManager.querySysCfgValue(SysCfgKeyConstant.SYSCFG_KEY_VERSION);
//    if (!CxTextUtil.isEmpty(version) &&
//        VersionUpdateManager.updateApp(mSVersion, version))
//    {
//      dynamic appUrl = sysCfgManager
//          .querySysCfgValue(SysCfgKeyConstant.SYSCFG_KEY_VERSION_URL);
//      if (!CxTextUtil.isEmpty(version))
//      {
//        VersionUpdateManager.showDownloadDialog(this, version, (result)
//        {
//          if (result)
//          {
//            startToHomePage();
//          }
//          else
//          {
//            updateDownloadApp(appUrl[Platform.operatingSystem]);
//          }
//        });
//      }
//      else
//      {
//        showToast("未配置apk下载地址");
//      }
//    }
//    else
//    {
//      startToHomePage();
//    }
  }

  /*
   * 更新下载app
   * @param AppUrl app下载路径
   */
  void updateDownloadApp(String appUrl)
  async
  {
    if (await canLaunch(appUrl))
    {
      await launch(appUrl);
    }
    else
    {
      showToast(appUrl);
    }
  }

  /*
   * 跳转到主页面
   */
  void startToHomePage()
  {
    PluginsFactory.getInstance().get("main").runPlugin(this);
  }
}
