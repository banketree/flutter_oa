/*
 * 功能：SP键常量
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SPConstant
{
  /*
   * 上班打卡时间：2019-05-17 01:00
   */
  static final String SP_AUTO_CLOCK_WORK_TIME = "auto_clock_work_time";

  /*
   * 下班打卡时间：2019-05-17 01:00
   */
  static final String SP_AUTO_CLOCK_UNWORK_TIME = "auto_clock_unwork_time";


  /*
   * ---------sp对应value值----------------------------
   */
  /*
   * sp对应string默认值
   */
  static final String SP_STR_DEFAULT_VALUE = "{}";
}
