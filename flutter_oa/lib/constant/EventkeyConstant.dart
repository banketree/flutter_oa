/*
 * 类描述：eventbus事件类型key值相关常量定义
 * 作者：郑朝军 on 2019/7/18
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/18
 * 修改备注：
 */
class EventkeyConstant
{
  static const int REFRESH = 0x900; //刷新
}
