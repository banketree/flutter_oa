import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutteroa/%20core/WidgetCreator.dart';
import 'package:flutteroa/application/SoftwareApplication.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:flutteroa/constant/PluginInitParamKeyConstant.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/bean/Pager.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';

/*
 * 主页(框架用例) <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class HomeWidget extends WidgetStatefulBase
{
  HomeWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new HomeWidgetState();
  }

  static String toStrings()
  {
    return "HomeWidget";
  }
}

/*
 * 页面功能 <br/>
 */
class HomeWidgetState extends WidgetBaseState<HomeWidget>
{
//  ClockWork m_ClockWork;

  /*
   * 组件中的孩子组件
   */
  List<Widget> m_WidgetList;

  void initState()
  {
    super.initState();
    initData();
    m_WidgetList = createChildrenKeyWidget();
//    m_ClockWork = ClockWork.getInstance();
//    m_ClockWork.queryHrClockin(this);
    query();
  }

  /*
   * 初始化监听相关
   */
  void initListener()
  {
    SoftwareApplication.getInstance().getEventBus().on().listen((event)
    {
//      if (event is MsgEvent && event.getType() == EventkeyConstant.REFRESH)
//      {
//        onRefresh();
//      }
    });
  }

  Widget build(BuildContext context)
  {
    return createCommonRefresh(new Column(
      children: <Widget>[
        new Container(
            height: MarginPaddingHeightConstant.APP_MARGIN_PADDING_232,
            child: new Stack(
              children: <Widget>[
                new Container(
                  height: 200,
                  child: new WeSwipe(
                      autoPlay: false,
                      itemCount: 1,
                      indicators: false,
                      itemBuilder: (index)
                      {
                        return WidgetCreator.createCommonImage(
                          mInitParaMap[PluginInitParamKeyConstant
                              .PLUGIN_INIT_PARAM_MAIN_KEY_IMAGE_URL],
                          fit: BoxFit.fitWidth,
                          alignment: Alignment.topCenter,
                        );
                      }
                  ),
                ),
                new Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  alignment: Alignment.bottomCenter,
                  child: m_WidgetList[DigitValueConstant.APP_DIGIT_VALUE_0],
                )
              ],
            )),
        new Card(
            margin: const EdgeInsets.only(left: 19, right: 19, top: 15),
            child: m_WidgetList[DigitValueConstant.APP_DIGIT_VALUE_1]),
        new Card(
            margin:
            const EdgeInsets.only(left: 19, right: 19, top: 15, bottom: 15),
            child: m_WidgetList[DigitValueConstant.APP_DIGIT_VALUE_2]),
      ],
    ));
  }

  /*
   * 查询我的部门信息
   */
  void query()
  {
//    InitService.queryDepadm(this);
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryMyTaskMax")
    {
      if (values is Map)
      {
        showToast("加载完成" + values[MapKeyConstant.MAP_KEY_SIZE].toString());
      }
    }
    else if (method == "queryHrClockin")
    {
      if (values is Map)
      {
        // 检测是否打上班卡，没有则自动打上班卡
//        List listWork = values[MapKeyConstant.MAP_KEY_FIRST];
//        if (CxTextUtil.isEmptyList(listWork) &&
//            TimeUtil.isNowBeforeHHmmss(m_ClockWork.getWorkTime()))
//        {
//          HrClockinService.createHrClockin(this,
//              DigitValueConstant.APP_DIGIT_VALUE_0, GDMapConstant.ADDRESS);
//        }
      }
    }
    else if (method == "createHrClockin")
    {
//      AudioManager.getInstance().play(mInitParaMap[MapKeyConstant.MAP_KEY_AUDIO]);
//      String time = DateUtil.getDateStrByTimeStr(
//          DateUtil.getNowDateStr(),
//          format: DateFormat.HOUR_MINUTE_SECOND);
//      Function close;
//      close = weDrawer(context)(
//          placement: WeDrawerPlacement.top,
//          child: Align(
//              alignment: Alignment.center,
//              child: Padding(
//                  padding: EdgeInsets.only(left: 10, right: 10, top: 45, bottom: 12),
//                  child: new DrawerTopWidget(
//                    mBtnText: "知道了",
//                    mTitle: "上班自动打卡成功",
//                    mContent: "$time上班打卡",
//                    mIcon: Icons.alarm,
//                    mOnClick: ()
//                    {
//                      close();
//                    },)
//              )
//          )
//      );
    }
    else if (method == "queryDepadm")
    {}
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void onNetWorkFaild(String method, Object values)
  {
    if (method == "queryHrClockin" &&
        values == MessageConstant.MSG_SERVER_ENMPTY)
    {
//      if (TimeUtil.isNowBeforeHHmmss(m_ClockWork.getWorkTime()))
//      {
//        HrClockinService.createHrClockin(this,
//            DigitValueConstant.APP_DIGIT_VALUE_0, GDMapConstant.ADDRESS);
//      }
    }
    else
    {
      super.onNetWorkFaild(method, values);
    }
  }

  @override
  Future<void> onRefresh()
  {
//    mChildrenItem.forEach((key, widgetChild)
//    {
//      if (widgetChild is HomeWillCardWidget &&
//          key.currentState is HomeWillCardState)
//      {
//        HomeWillCardState state = key.currentState;
//        state.query(1);
//      }
//      else if (widgetChild is HomeProcressWidget &&
//          key.currentState is HomeProcressState)
//      {
//        HomeProcressState state = key.currentState;
//        state.query(1);
//      }
//    });
    return super.onRefresh();
  }

  @override
  Future<void> loadMore()
  {
//    mChildrenItem.forEach((key, widgetChild)
//    {
//      if (widgetChild is HomeProcressWidget &&
//          key.currentState is HomeProcressState)
//      {
//        HomeProcressState state = key.currentState;
//        Pager pager = state.m_pager;
//        if (pager.getPageNo() < (pager.getTotalPage() + 1))
//        {
//          state.query(pager.getPageNo());
//        }
//      }
//    });
    return super.loadMore();
  }
}

/*
 * 类描述：首页组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class HomeWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new HomeWidget();
  }
}
