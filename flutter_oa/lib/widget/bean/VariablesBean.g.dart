// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'VariablesBean.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VariablesBean _$VariablesBeanFromJson(Map<String, dynamic> json) {
  return VariablesBean()
    ..reason = json['reason'] as String
    ..applyUserName = json['applyUserName'] as String
    ..applyId = json['applyId'] as String
    ..total = json['total'] as String
    ..businessKey = json['businessKey'] as String
    ..bill = json['bill'] as String
    ..position = json['position'] as String
    ..depart = json['depart'] as String
    ..table = json['table'] as String
    ..applyUserId = json['applyUserId'] as String
    ..NAME_didaReview = json['NAME_didaReview'] as String
    ..USER_didaReview = json['USER_didaReview'] as String
    ..TQ_didaReview = json['TQ_didaReview'] as String;
}

Map<String, dynamic> _$VariablesBeanToJson(VariablesBean instance) =>
    <String, dynamic>{
      'reason': instance.reason,
      'applyUserName': instance.applyUserName,
      'applyId': instance.applyId,
      'total': instance.total,
      'businessKey': instance.businessKey,
      'bill': instance.bill,
      'position': instance.position,
      'depart': instance.depart,
      'table': instance.table,
      'applyUserId': instance.applyUserId,
      'NAME_didaReview': instance.NAME_didaReview,
      'USER_didaReview': instance.USER_didaReview,
      'TQ_didaReview': instance.TQ_didaReview
    };
