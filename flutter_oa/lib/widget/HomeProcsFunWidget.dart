import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutteroa/%20core/CommonWidgetCreator.dart';
import 'package:flutteroa/%20core/WidgetCreator.dart';
import 'package:flutteroa/constant/FldConstant.dart';
import 'package:flutteroa/constant/MapKeyConstant.dart';
import 'package:flutteroa/font/IconFont.dart';
import 'package:zpub_sqflite/zpub_sqflite.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';

/*
 * 类描述：流程常用功能组件
 * 作者：郑朝军 on 2019/5/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/6
 * 修改备注：
 */
class HomeProcsFunWidget extends WidgetStatefulBase
{
  HomeProcsFunWidget({Key key, plugin}) : super(key: key, plugin: plugin);

  @override
  State<StatefulWidget> createState()
  {
    return new HomeDailyFunState();
  }

  static String toStrings()
  {
    return "HomeProcsFunWidget";
  }
}

class HomeDailyFunState extends WidgetBaseState<HomeProcsFunWidget>
{
  Widget widget_body;

  @override
  void initState()
  {
    super.initState();
    initData();
    query();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Column(
      children: <Widget>[
        new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Chip(label: new Text(
                "应用功能", style: BasTextStyleRes.text_color_text1_larger),
                labelPadding: EdgeInsets.only(left: 5),
                avatar: new Icon(
                  IconFont.icon_application,
                  size: 18,
                  color: Colors.blue,
                ), backgroundColor: Colors.white),
            new GestureDetector(child: new Chip(
                label: new Text("自定义",
                    style: BasTextStyleRes.text_color_text_grey_smaller),
                labelPadding: EdgeInsets.only(right: 0),
                deleteIcon: new Icon(
                  Icons.arrow_forward_ios, size: 15, color: Colors.grey,),
                backgroundColor: Colors.white,
                onDeleted: doClickRunPlugin), onTap: doClickRunPlugin,),
          ],
        ),
        WidgetCreator.createCommonDevider(),
        new Container(
            child: widget_body == null
                ? WidgetCreator.createCommonNoData(
                text: BasStringRes.progressbar_text)
                : widget_body),
      ],
    );
  }

  void query()
  {
//    ProcsFunService.queryMyProcsFun(this);
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryMyProcsFun")
    {
      setState(()
      {
        if (values is List)
        {
          List<Widget> children = <Widget>[];
          values.forEach((map)
          {
            children.add(
                CommonWidgetCreator.createCommonTopImgText(map, onTap: ()
                {
                  mChildUniversalInitPara[MapKeyConstant.MAP_KEY_PROCS] = map;
                  runPluginName(map[FldConstant.FLD_PLUGINS]);
                }));
          });
          widget_body = WidgetCreator.createCommonGridViewWrap(children);
        }
      });
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void doClickRunPlugin()
  {
    runPluginName("FuncPlugin").then((value)
    {
      if (value != null && value)
      {
        SQLiteDBMethod.getInstance().close();
        query();
      }
    });
  }
}


/*
 * 类描述：首页组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class HomeDailyFunWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new HomeProcsFunWidget(key: initPara[MapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
