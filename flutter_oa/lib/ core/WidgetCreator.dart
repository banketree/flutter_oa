import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutteroa/constant/SoftwareConstant.dart';
import 'package:flutteroa/font/IconFont.dart';
import 'package:photo_view/photo_view.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:transparent_image/transparent_image.dart';

/*
 * 功能：组件库,用于创建常用的组件：类似Android中layout文件中common_progressbar.xml相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class WidgetCreator
{
  /*
   * 创建公有网络进度栏<br/>
   */
  static Widget createCommonProgressBar({String text = BasStringRes.progressbar_text})
  {
    Widget progressBar = new Container(
      width: 198,
      height: 63,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 110),
      decoration: BoxDecoration(
          color: Color.fromARGB(127, 127, 127, 127),
          border: Border.all(color: Colors.blue, width: 0.1),
          borderRadius: BorderRadius.circular(4)),
      child: new Row(
        children: <Widget>[
          new CircularProgressIndicator(),
          new Container(
            width: 115,
            child: new Text(
              CxTextUtil.isEmpty(text) ? BasStringRes.progressbar_text : text,
              textScaleFactor: 1.1,
            ),
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
    );

    return progressBar;
  }

  /*
   * 创建一条横线<br/>
   */
  static Widget createCommonDevider()
  {
    return new Divider(height: 1, color: Colors.grey);
  }

  /*
   * 创建一条竖线<br/>
   */
  static Widget createCommonVerticalDevider()
  {
    Widget devider = new Container(
      height: 30.0,
      width: 1.0,
      color: Colors.grey.withOpacity(0.5),
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
    );
    return devider;
  }

  static Widget createCommonVerticalDeviderOld()
  {
    return new VerticalDivider(color: Colors.grey);
  }

  /*
   * 创建暂无数据<br/>
   */
  static Widget createCommonNoData({String text = BasStringRes.no_data, GestureTapCallback onTap})
  {
    Widget devider = new GestureDetector(onTap: onTap, child: new Container(
        margin: const EdgeInsets.symmetric(vertical: 40, horizontal: 40),
        child: new Align(alignment: Alignment.center, child: new Text(text))));
    return devider;
  }

  /*
   * 创建暂无数据<br/>
   */
  static List<Widget> createCommonNoDataList({String text = BasStringRes.no_data})
  {
    List<Widget> list = <Widget>[];
    Widget devider = new Container(
      margin: const EdgeInsets.symmetric(vertical: 40, horizontal: 40),
      child: new Align(alignment: Alignment.center, child: new Text(text)),
    );
    list.add(devider);
    return list;
  }

  /*
   * 创建公共网络图片:支持本地，iconfont，和网络图片<br/>
   */
  static Widget createCommonImage(dynamic imageUrl,
      {BoxFit fit,
        double scale: 1.0,
        int third: 1,
        AlignmentGeometry alignment: Alignment.center,
        LoadingErrorWidgetBuilder errorWidget,
        PlaceholderWidgetBuilder placeholder,
        double width,
        Color color,
        double size,
        IconData icon,
        double height})
  {
    Widget image;
    if (icon != null || imageUrl.length < DigitValueConstant.APP_DIGIT_VALUE_10)
    {
      image = new Icon(
        icon ?? IconData(int.parse(imageUrl), fontFamily: IconFont.getFamily()),
        color: color, size: size,);
    }
    else if (imageUrl.contains(SoftwareConstant.SOFTWARE_ASSETS))
    {
      image = new Image(
        fit: fit,
        alignment: alignment,
        width: width,
        height: height,
        color: color,
        image: AssetImage(imageUrl),
      );
    }
    else
    {
      if (third == DigitValueConstant.APP_DIGIT_VALUE_1)
      {
        image = new CachedNetworkImage(
          fit: fit,
          alignment: alignment,
          imageUrl: imageUrl,
          color: color,
          errorWidget: errorWidget ?? (BuildContext context, String url, Object error)
          {
            return Image(
              image: AssetImage('assets/images/ic_default.png'),
            );
          },
          placeholder: placeholder ?? (BuildContext context, String url)
          {
            return Image(
              image: AssetImage('assets/images/ic_default.png'),
            );
          },
        );
      }
      else
      {
        image = FadeInImage(
          placeholder: AssetImage('assets/images/ic_default.png'),
          image: new NetworkImage(
            imageUrl,
            scale: scale,
          ),
          fit: fit,
          alignment: alignment,
          width: width,
          height: height,);
      }
    }
    return image;
  }

  /*
   * 创建公共可以缩放的图片<br/>
   */
  static Widget createCommonZoomableImage(ImageProvider image)
  {
    return PhotoView(imageProvider: image);
  }

  /*
   * 创建公共可以缩放的图片<br/>
   */
  static GridView createCommonGridViewWrap(List<Widget> children)
  {
    return new GridView.count(
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        children: children);
  }

  /*
   * 创建公共可以缩放的图片<br/>
   */
  static GridView createMiddleGridViewWrap(List<Widget> children)
  {
    return new GridView.count(
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: 3,
        padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        children: children);
  }
}
