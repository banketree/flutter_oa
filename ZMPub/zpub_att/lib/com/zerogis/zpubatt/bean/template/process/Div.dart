import 'package:json_annotation/json_annotation.dart';

part 'Div.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
@JsonSerializable()
class Div
{
  @JsonKey(name: 'ref')
  String ref;

  @JsonKey(name: 'class')
  String clazz;

  @JsonKey(name: 'data-options')
  String dataOptions;

  Div();

  String getRef()
  {
    return ref;
  }

  void setRef(String ref)
  {
    this.ref = ref;
  }

  String getClazz()
  {
    return clazz;
  }

  void setClazz(String clazz)
  {
    this.clazz = clazz;
  }

  String getDataOptions()
  {
    return dataOptions;
  }

  void setDataOptions(String dataOptions)
  {
    this.dataOptions = dataOptions;
  }

  factory Div.fromJson(Map<String, dynamic> srcJson) => _$DivFromJson(srcJson);

  Map<String, dynamic> toJson()
  => _$DivToJson(this);
}
