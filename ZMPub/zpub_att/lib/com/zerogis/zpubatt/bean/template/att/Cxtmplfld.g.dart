// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Cxtmplfld.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cxtmplfld _$CxtmplfldFromJson(Map<String, dynamic> json) {
  return Cxtmplfld(
      json['colname'] as String,
      json['item-style'] as String,
      json['fldChange'] as String,
      json['valButton'] as String,
      json['selDyna'] as String);
}

Map<String, dynamic> _$CxtmplfldToJson(Cxtmplfld instance) => <String, dynamic>{
      'colname': instance.colname,
      'item-style': instance.itemstyle,
      'fldChange': instance.fldChange,
      'valButton': instance.valButton,
      'selDyna': instance.selDyna
    };
