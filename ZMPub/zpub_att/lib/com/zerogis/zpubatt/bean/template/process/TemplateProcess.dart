import 'package:json_annotation/json_annotation.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/process/Elform.dart';

part 'TemplateProcess.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
@JsonSerializable()
class TemplateProcess
{
  @JsonKey(name: 'el-form')
  Elform elform;

  TemplateProcess()
  {}

  factory TemplateProcess.fromJson(Map<String, dynamic> srcJson) =>
      _$TemplateProcessFromJson(srcJson);

  Map<String, dynamic> toJson()
  => _$TemplateProcessToJson(this);
}
