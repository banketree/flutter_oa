import 'package:json_annotation/json_annotation.dart';

part 'Cfg.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/14
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/14
 * 修改备注：
 */
@JsonSerializable()
class Cfg
{
  @JsonKey(name: 'colname')
  String colname;

  @JsonKey(name: 'title')
  String title;

  @JsonKey(name: 'type')
  String type;

  @JsonKey(name: 'rows')
  String rows;

  @JsonKey(name: 'editable')
  String editable;

  @JsonKey(name: 'value')
  String value;

  @JsonKey(name: 'required')
  String required;

  Cfg();


  String getColname()
  {
    return colname;
  }

  void setColname(String colname)
  {
    this.colname = colname;
  }

  String getTitle()
  {
    return title;
  }

  void setTitle(String title)
  {
    this.title = title;
  }

  String getType()
  {
    return type;
  }

  void setType(String type)
  {
    this.type = type;
  }

  String getRows()
  {
    return rows;
  }

  void setRows(String rows)
  {
    this.rows = rows;
  }

  String getEditable()
  {
    return editable;
  }


  void setEditable(String editable)
  {
    this.editable = editable;
  }

  String getValue()
  {
    return value;
  }

  void setValue(String value)
  {
    this.value = value;
  }

  int getEditables()
  {
    return editable == "false" ? 0 : 1;
  }

  int getNullable()
  {
    return required == "false" ? 1 : 0;
  }

  factory Cfg.fromJson(Map<String, dynamic> srcJson) => _$CfgFromJson(srcJson);

  Map<String, dynamic> toJson()
  => _$CfgToJson(this);

}
