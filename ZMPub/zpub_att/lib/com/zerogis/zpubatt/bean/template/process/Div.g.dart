// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Div.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Div _$DivFromJson(Map<String, dynamic> json) {
  return Div()
    ..ref = json['ref'] as String
    ..clazz = json['class'] as String
    ..dataOptions = json['data-options'] as String;
}

Map<String, dynamic> _$DivToJson(Div instance) => <String, dynamic>{
      'ref': instance.ref,
      'class': instance.clazz,
      'data-options': instance.dataOptions
    };
