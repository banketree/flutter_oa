// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Elform.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Elform _$ElformFromJson(Map<String, dynamic> json) {
  return Elform()
    ..model = json['model'] as String
    ..rules = json['rules'] as String
    ..clazz = json['class'] as String
    ..labelposition = json['label-position'] as String
    ..labelwidth = json['label-width'] as String
    ..size = json['size'] as String
    ..cxtmplfld = (json['cx-tmplfld'] as List)
        ?.map((e) =>
            e == null ? null : Cxtmplfld.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ElformToJson(Elform instance) => <String, dynamic>{
      'model': instance.model,
      'rules': instance.rules,
      'class': instance.clazz,
      'label-position': instance.labelposition,
      'label-width': instance.labelwidth,
      'size': instance.size,
      'cx-tmplfld': instance.cxtmplfld
    };
