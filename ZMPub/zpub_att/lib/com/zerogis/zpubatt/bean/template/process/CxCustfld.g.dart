// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CxCustfld.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CxCustfld _$CxCustfldFromJson(Map<String, dynamic> json) {
  return CxCustfld()
    ..itemstyle = json['item-style'] as String
    ..fldChange = json['fldChange'] as String
    ..cfg = json['cfg'] as String;
}

Map<String, dynamic> _$CxCustfldToJson(CxCustfld instance) => <String, dynamic>{
      'item-style': instance.itemstyle,
      'fldChange': instance.fldChange,
      'cfg': instance.cfg
    };
