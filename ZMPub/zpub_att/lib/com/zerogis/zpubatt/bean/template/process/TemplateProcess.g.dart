// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TemplateProcess.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TemplateProcess _$TemplateProcessFromJson(Map<String, dynamic> json) {
  return TemplateProcess()
    ..elform = json['el-form'] == null
        ? null
        : Elform.fromJson(json['el-form'] as Map<String, dynamic>);
}

Map<String, dynamic> _$TemplateProcessToJson(TemplateProcess instance) =>
    <String, dynamic>{'el-form': instance.elform};
