import 'package:json_annotation/json_annotation.dart';

part 'CxCustfld.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
@JsonSerializable()
class CxCustfld
{
  @JsonKey(name: 'item-style')
  String itemstyle;

  @JsonKey(name: 'fldChange')
  String fldChange;

  @JsonKey(name: 'cfg')
  String cfg;

  CxCustfld();

  String getCfg()
  {
    return cfg;
  }

  factory CxCustfld.fromJson(Map<String, dynamic> srcJson) =>
      _$CxCustfldFromJson(srcJson);

  Map<String, dynamic> toJson()
  => _$CxCustfldToJson(this);
}
