import 'package:json_annotation/json_annotation.dart';

part 'Cxtmplfld.g.dart';

/*
 * 类描述：
 * 作者：郑朝军 on 2019/6/13
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/13
 * 修改备注：
 */
@JsonSerializable()
class Cxtmplfld
{
  @JsonKey(name: 'colname')
  String colname;

  @JsonKey(name: 'item-style')
  String itemstyle;

  @JsonKey(name: 'fldChange')
  String fldChange;

  @JsonKey(name: 'valButton')
  String valButton;

  @JsonKey(name: 'selDyna')
  String selDyna;

  String getColname()
  {
    return colname;
  }

  Cxtmplfld(this.colname, this.itemstyle, this.fldChange, this.valButton,
      this.selDyna,);

  factory Cxtmplfld.fromJson(Map<String, dynamic> srcJson) =>
      _$CxtmplfldFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CxtmplfldToJson(this);
}
