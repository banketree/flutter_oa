import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/http/TemplateService.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

/*
 * 纯网络模板组件 <br/>
 * @param {Object 必传} fromKey             取模板的key
 * @param {Object 选传} id                  id值 id = processDefinitionId
 * @param {Object 必传} variable            变量
 * @param {Object 必传} att                 属性参数 att = { _major:  子类型, _minor:  子类型 id:  id值}
 * @param {Object 必传} plugin              插件对象
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttOnlyTemplateWidget extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  AttOnlyTemplateWidget({Key key, this.mInitPara, plugin})
      : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttOnlyTemplateWidgetState();
  }

  static String toStrings()
  {
    return "AttOnlyTemplateWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class AttOnlyTemplateWidgetState extends WidgetBaseState<AttOnlyTemplateWidget>
{
  /*
   * 当前属性的列
   */
  List<Fld> fldList = <Fld>[];

  /*
   * 当前属性所有的组件
   */
  Map<GlobalKey<State<StatefulWidget>>, Widget> childrenAttItem = {};

  /*
   * 当前存放每一个条目的GlobalKey的值
   */
  Map<dynamic, dynamic> mGlobalKeys = {};

  void initState()
  {
    super.initState();
    query();
  }

  /*
   * 初始化数据相关
   */
  void initData()
  {
    super.initData();
  }

  Widget build(BuildContext context)
  {
    return mEmptyView;
  }

  void query()
  {
    TemplateService.queryNetTemplates(
        widget.mInitPara[BasMapKeyConstant.MAP_KEY_FROMKEY], widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID], this,
        variable: widget.mInitPara[BasMapKeyConstant.MAP_KEY_VARIABLE]);
  }

  /*
   * 创建属性集合的条目
   */
  List<Widget> createAttItemList()
  {
    childrenAttItem.clear();
    for (int i = 0; i < fldList.length; i++)
    {
      Fld fld = fldList[i];
      GlobalKey key = createOrQueryGlobalKey(fld);
      initFlds(key, fld);
      childrenAttItem[new GlobalKey<AttItemWidgetState>()] =
          SysWidgetCreator.createCommonDevider();
    }
    return childrenAttItem.values.toList();
  }

  /*
   * 网络层接口回调
   */
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryNetTemplates")
    {
      Map<String, Object> param = {
        HttpParamKeyValue.PARAM_KEY_MAJOR: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant.FLD_MAJOR],
        HttpParamKeyValue.PARAM_KEY_MINOR: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant.FLD_MINOR],
        HttpParamKeyValue.PARAM_KEY_ID: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ATT][DBFldConstant.FLD_ID]
      };
      SvrAreaSvrService.getAtt(this, param: param);
      mGlobalKeys[method] = values;
    }
    else if (method == "getAtt" && values is Map)
    {
      this.fldList = mGlobalKeys['queryNetTemplates'];
      setState(()
      {
        fldList.forEach((fld)
        {
          if (fld.getColname() == 'starttime' || fld.getColname() == 'endtime' || fld.getColname() == 'dest'
              || fld.getColname() == 'model' || fld.getColname() == 'detail' || fld.getColname() == 'total'
              || fld.getColname() == 'value')
          {
            fld.setValue(values[fld.getColname()].toString());
          }
        });

        mEmptyView = Column(children: createAttItemList());
      });
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void onNetWorkFaild(String method, Object values)
  {
  }

  /*
   * 查询属性的键值
   * @retrun result = [{fld: Instance of 'Fld', value: 测试},{fld: Instance of 'Fld', value: 测试},]
   */
  List<Map> queryAttKeyValue()
  {
    List<Map> result = <Map>[];
    childrenAttItem.forEach((key, widgetChild)
    {
      if (widgetChild is AttItemWidget)
      {
        if (key.currentState is AttItemWidgetState)
        {
          AttItemWidgetState state = key.currentState;
          Map map = state.queryItemKeyValue();
          result.add(map);
        }
      }
    });
    return result;
  }


  /*
   * 查询或创建GlobalKey
   * @param fld listview中一个条目生成的item值
   * return GlobalKey
   */
  GlobalKey createOrQueryGlobalKey(Fld fld)
  {
    dynamic globalKey = mGlobalKeys[fld.getNamec()];
    if (CxTextUtil.isEmptyObject(globalKey))
    {
      mGlobalKeys[fld.getNamec()] = new GlobalKey<AttItemWidgetState>();
    }
    return mGlobalKeys[fld.getNamec()];
  }

  /*
   * 初始化数据输入值
   * @method
   * @param   {Fld}       flds              所有字段信息
   */
  void initFlds(GlobalKey key, Fld fld)
  {
    childrenAttItem[key] = new AttItemWidget(fld, mValue: fld.getValue(), key: key);
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class AttOnlyTemplateWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new AttOnlyTemplateWidget(mInitPara: initPara);
  }
}
