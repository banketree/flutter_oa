import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/widget/AttItemWidget.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/resource/BasStringRes.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/EntityManager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/EntityManagerConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldManager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldManagerConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:flutter/widgets.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/FldValue.dart';

/*
 * 根据主子类型生成属性组件:不传ID则不请求网络生成属性 <br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} id  id值
 * @param {Object 选传} mAttVals 属性对象
 * @param {Object 选传} mValueChangedMethod 网络层返回数据之后结果回调对象
 * @param {Object 选传} plugin 插件对象
 * @param {Object 选传} attState 属性状态
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttWidget extends WidgetStatefulBase
{
  /*
   * 属性主类型
   */
  int mMajor;

  /*
   * 属性子类型
   */
  int mMinor;

  /*
   * 属性表中对应的ID值
   */
  int mId;

  /*
   * 当前属性的值
   */
  Map<String, dynamic> mAttVals;

  /*
   * 查询属性数据回调方法
   */
  ValueChanged<dynamic> mValueChangedMethod;

  /*
   *【属性是否查看=否必传=attState=1参数说明：[0=新增，录入][1=查看][2=编辑]，默认为新增】
   */
  int attState;

  AttWidget(this.mMajor, this.mMinor,
      {Key key, @required this.mId = -1, this.mAttVals, this.mValueChangedMethod, this.attState: AttFldValueConstant
          .ATT_STATE_DEFAULT, plugin})
      :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttWidgetState();
  }

  static String toStrings()
  {
    return "AttWidget";
  }
}

/*
 * 属性组件功能 <br/>
 */
class AttWidgetState<T extends AttWidget> extends WidgetBaseState<T>
{
  /*
   * 当前属性的表名
   */
  String mTabName;

  /*
   * 当前属性的列
   */
  List<Fld> mFldList;

  /*
   * 当前属性所有的组件
   */
  Map<GlobalKey<State<StatefulWidget>>, Widget> mChildrenAttItem = {};

  /*
   * 当前存放每一个条目的GlobalKey的值
   */
  Map<dynamic, dynamic> mGlobalKeys = {};

  /*
   * 当前属性的值
   */
  Map<String, dynamic> mAttWidgetValues = {};

  void initState()
  {
    super.initState();
    query();
    if (!CxTextUtil.isEmptyMap(widget.mAttVals))
    {
      mAttWidgetValues = widget.mAttVals;
    }
  }

  Widget build(BuildContext context)
  {
    Widget widget = new ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: createAttList(),
    );
    return widget;
  }

  /*
   * 查询属性
   */
  void query()
  {
    if (widget.mId != DigitValueConstant.APP_DIGIT_VALUE__1)
    {
      CommonService.queryAttJo(widget.mMajor, widget.mMinor, widget.mId, this);
    }
  }

  /*
   * 初始化数据 
   */
  void initData()
  {
    super.initData();
    EntityManagerConstant entityManagerConstant = EntityManager.getInstance();
    FldManagerConstant fldManagerConstant = FldManager.getInstance();
    mTabName =
        entityManagerConstant.queryEntityTabbatt(widget.mMajor, widget.mMinor);
    mFldList = fldManagerConstant.queryFldDispDep(mTabName);
  }

  /*
   * 创建属性集合
   */
  List<Widget> createAttList()
  {
    if (widget.mId == DigitValueConstant.APP_DIGIT_VALUE__1)
    {
      return createAttItemList();
    }
    else if (CxTextUtil.isEmptyMap(mAttWidgetValues))
    {
      return SysWidgetCreator.createCommonNoDataList(
          text: BasStringRes.progressbar_text);
    }
    else
    {
      return createAttItemList();
    }
  }

  /*
   * 创建属性集合的条目
   */
  List<Widget> createAttItemList()
  {
    mChildrenAttItem.clear();
    for (int i = 0; i < mFldList.length; i++)
    {
      Fld fld = mFldList[i];
      editableFld(fld);
      if (filterFldItem(fld))
      {
        GlobalKey key = createOrQueryGlobalKey(fld);
        initFlds(key, fld);
        mChildrenAttItem[new GlobalKey<AttItemWidgetState>()] =
            SysWidgetCreator.createCommonDevider();
      }
    }
    return mChildrenAttItem.values.toList();
  }

  /*
   * 是否过滤fld的条目
   * @param fld listview中一个条目生成的item值
   * return [false=过滤]
   */
  bool filterFldItem(Fld fld)
  {
    if (fld.getDisporder() > DigitValueConstant.APP_DIGIT_VALUE_0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /*
   * 查询或创建GlobalKey
   * @param fld listview中一个条目生成的item值
   * return GlobalKey
   */
  GlobalKey createOrQueryGlobalKey(Fld fld)
  {
    dynamic globalKey = mGlobalKeys[fld.getId()];
    if (CxTextUtil.isEmptyObject(globalKey))
    {
      mGlobalKeys[fld.getId()] = new GlobalKey<AttItemWidgetState>();
    }
    return mGlobalKeys[fld.getId()];
  }

  /*
   * 初始化数据输入值
   * @method
   * @param   {Fld}       flds              所有字段信息
   */
  void initFlds(GlobalKey key, Fld fld)
  {
    mChildrenAttItem[key] = new AttItemWidget(fld, mValue: mAttWidgetValues[fld.getColname()], key: key);
  }

  /*
   * 编辑Fld字段的Editable值
   * @method
   * @param   {Fld}       flds              所有字段信息
   */
  void editableFld(Fld fld)
  {
    if (widget.attState == AttFldValueConstant.ATT_STATE_NEW_EDIT)
    {
      // 新增，录入
      fld.setEditable(
          fld.getNewedit() == AttFldValueConstant.NEW_EDIT_NO
              ? AttFldValueConstant.ATT_STATE_NEW_EDIT
              : AttFldValueConstant
              .ATT_STATE_DEFAULT);
    }
    else if (widget.attState == AttFldValueConstant.ATT_STATE_DEFAULT)
    {
      // 查看
      fld.setEditable(AttFldValueConstant.ATT_STATE_NEW_EDIT);
    }
    else if (widget.attState == AttFldValueConstant.ATT_STATE_EDIT_ABLE)
    {
      // 编辑
      fld.setEditable(
          fld.getEditable() == AttFldValueConstant.EDIT_ABLE_NO
              ? AttFldValueConstant.ATT_STATE_NEW_EDIT
              : AttFldValueConstant
              .ATT_STATE_DEFAULT);
    }
  }

  /*
   * 网络层接口回调
   */
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryAttJo")
    {
      dealQueryAttJo(values);
    }
  }

  /*
   * 处理网络层查询属性
   */
  void dealQueryAttJo(Object values)
  {
    if (values is Map)
    {
      setState(()
      {
        mAttWidgetValues = values;
      });
      if (widget.mValueChangedMethod != null)
      {
        widget.mValueChangedMethod(values);
      }
    }
  }

  /*
   * 查询属性的键值
   * @retrun result = [{fld: Instance of 'Fld', value: 测试},{fld: Instance of 'Fld', value: 测试},]
   */
  List<Map> queryAttKeyValue()
  {
    List<Map> result = <Map>[];
    mChildrenAttItem.forEach((key, widgetChild)
    {
      if (widgetChild is AttItemWidget)
      {
        if (key.currentState is AttItemWidgetState)
        {
          AttItemWidgetState state = key.currentState;
          Map map = state.queryItemKeyValue();
          result.add(map);
        }
      }
    });
    return result;
  }

  /*
   * 查询属性的键值
   * @retrun result = {name: 张三, value: 测试}
   */
  Map<String, Object> queryAttKeyMap()
  {
    Map<String, Object> result = {};
    mChildrenAttItem.forEach((key, widgetChild)
    {
      if (widgetChild is AttItemWidget)
      {
        if (key.currentState is AttItemWidgetState)
        {
          AttItemWidgetState state = key.currentState;
          Map map = state.queryItemKeyValue();
          Fld fld = map[BasMapKeyConstant.MAP_KEY_FLD];
          result[fld.getColname()] = disptypeValueE(fld, map);
        }
      }
    });
    return result;
  }


  /*
   * 查询属性的条件
   * @retrun result = {_exp: 'code LIKE ? AND name LIKE ?', types: 's,s', vals: '%1%:@:%1%'}
   */
  Map<String, dynamic> queryAttKeyExp({String separator: BasStringValueConstant.STR_COMMON_COLON_A_COLON})
  {
    // 取exp，types，vals
    Map<String, dynamic> result = {
      HttpParamKeyValue.PARAM_KEY_EXP: '',
      HttpParamKeyValue.PARAM_KEY_TYPES: '',
      HttpParamKeyValue.PARAM_KEY_VALS: '',
    };
    mChildrenAttItem.forEach((key, widgetChild)
    {
      if (widgetChild is AttItemWidget)
      {
        if (key.currentState is AttItemWidgetState)
        {
          AttItemWidgetState state = key.currentState;
          Map map = state.queryItemKeyExp();

          if (map[HttpParamKeyValue.PARAM_KEY_EXP] != null)
          {
            result[HttpParamKeyValue.PARAM_KEY_EXP] += map[HttpParamKeyValue.PARAM_KEY_EXP] + DBExpConstant.EXP_AND;
            result[HttpParamKeyValue.PARAM_KEY_TYPES] +=
                map[HttpParamKeyValue.PARAM_KEY_TYPES] + BasStringValueConstant.STR_COMMON_COMMA;
            result[HttpParamKeyValue.PARAM_KEY_VALS] += map[HttpParamKeyValue.PARAM_KEY_VALS] + separator;
          }
        }
      }
    });

    // 去除多余的串
    if (!CxTextUtil.isEmpty(result[HttpParamKeyValue.PARAM_KEY_EXP]))
    {
      String exp = result[HttpParamKeyValue.PARAM_KEY_EXP];
      result[HttpParamKeyValue.PARAM_KEY_EXP] = exp.substring(0, exp.lastIndexOf(DBExpConstant.EXP_AND));

      String types = result[HttpParamKeyValue.PARAM_KEY_TYPES];
      result[HttpParamKeyValue.PARAM_KEY_TYPES] =
          types.substring(0, types.lastIndexOf(BasStringValueConstant.STR_COMMON_COMMA));

      String vals = result[HttpParamKeyValue.PARAM_KEY_VALS];
      result[HttpParamKeyValue.PARAM_KEY_VALS] = vals.substring(0, vals.lastIndexOf(separator));
    }
    return result;
  }

  /*
   * 根据disptype返回value值
   * @param   {Fld}       flds              所有字段信息
   * @param   {Map}       map               属性条目所有信息
   */
  Object disptypeValueE(Fld fld, Map map)
  {
    if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_2)
    {
      // 下拉框
      FldValue fldValue = map[BasMapKeyConstant.MAP_KEY_VALUE];
      return fldValue == null ? '' : fldValue.getDbvalue();
    }
    else if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_6)
    {
      // 下拉(可改)
      return map[BasMapKeyConstant.MAP_KEY_VALUE];
    }
    if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_7)
    {
      // 自定义按钮
      return map[BasMapKeyConstant.MAP_KEY_VALUE];
    }
    if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_9)
    {
      // 动态选择
      return map[BasMapKeyConstant.MAP_KEY_VALUE];
    }
    else
    {
      return map[BasMapKeyConstant.MAP_KEY_VALUE];
    }
  }
}

/*
 * 类描述：首页组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class AttWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    if (initPara == null)
    {
      return SysWidgetCreator.createCommonNoData();
    }
    else
    {
      return new AttWidget(
        initPara[DBFldConstant.FLD_MAJOR],
        initPara[DBFldConstant.FLD_MINOR],
        mId: initPara[DBFldConstant.FLD_ID],
        attState: initPara[AttConstant.ATT_STATE],
        key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],
      );
    }
  }
}
