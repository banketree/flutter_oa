import 'package:flutter/material.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/FldValue.dart';
import 'package:zpub_att/com/zerogis/zpubatt/http/TemplateService.dart';
import 'package:zpub_third/com/zerogis/zpubthird/application/ThirdApplication.dart';

/*
 * 网络模板按钮组件 <br/>
 * @param {Object 必传} mId  
 * @param {Object 选传} mSfromKey   
 * @param {Object 选传} mTableItemData 表格条目数据
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class TemplateBtnBas extends WidgetStatefulBase
{
  Map<String, dynamic> initPara;

  TemplateBtnBas({this.initPara, Key key, plugin}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new TemplateBtnBasState();
  }

  static String toStrings()
  {
    return "TemplateBtnBas";
  }
}

/*
 * 组件功能 <br/>
 */
class TemplateBtnBasState<T extends TemplateBtnBas> extends WidgetBaseState<T>
{
  /*
   * buttons:2，移交，1，不同意，0，同意,transferType:all,transferParams:
   */
  Map<String, dynamic> mButtons = {};

  /*
   * 用户号："ASSIGNEE_", "1607"
   */
  String mNo;

  /*
   * 审批人
   */
  bool mAssignee = false;
  
  void initState()
  {
    super.initState();
    query();
  }

  Widget build(BuildContext context)
  {
    return mEmptyView;
  }

  /*
   * 网络层接口回调
   */
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryNetTemplateBtn" && values is Map)
    {
      mButtons = values;
      mEmptyView = Padding(padding: EdgeInsets.all(
          MarginPaddingHeightConstant.APP_MARGIN_PADDING_8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: createChildBtnList(),),);
    }
    else if (method == "completeTask" || method == "assignTask" || method == "returnTask")
    {
      finish();
      showToast(values);
      ThirdApplication.getInstance().getEventBus().fire(BasMsgEvent(BasEvnBusKeyConstant.BAS_COM_TASK_REFRESH));
    }
  }

  void query()
  {
    TemplateService.queryNetTemplateBtn(
        widget.initPara[BasMapKeyConstant.MAP_KEY_FROMKEY], widget.initPara[SvrBPMConstant.SVR_PROCESS_DEF_ID], this);
  }

  List<Widget> createChildBtnList()
  {
    List<Widget> list = <Widget>[];
    String buttons = mButtons[BasMapKeyConstant.MAP_KEY_BUTTONS];
    List<String> result = buttons.split(BasStringValueConstant.STR_COMMON_COMMA_ZH);
    for (int i = 0; i < (result.length * 0.5); i ++)
    {
      String itemKey = result[i * 2];
      String itemValue = result[(i * 2) + 1];
      list.add(createBtn(itemKey, itemValue));
    }
    return list;
  }

  Widget createBtn(String key, String textValue)
  {
   if(DigitValueConstant.APP_DIGIT_VALUE_3.toString() == key)
   {
     mAssignee = true;
   }
    return new Expanded(flex: DigitValueConstant.APP_DIGIT_VALUE_1,
        child: new WeButton(textValue, onClick: ()
        {
          doClickTempButton(int.parse(key), textValue);
        },));
  }

  void doClickTempButton(int key, event)
  {
    // 按钮判断点击事件
    if (DigitValueConstant.APP_DIGIT_VALUE_0 == key)
    {
      doClickTemp0Button(key, event);
    }
    else if (DigitValueConstant.APP_DIGIT_VALUE_1 == key)
    {
      doClickTemp1Button(key, event);
    }
    else if (DigitValueConstant.APP_DIGIT_VALUE_2 == key)
    {
      doClickTemp2Button(key, event);
    }
    else if (DigitValueConstant.APP_DIGIT_VALUE_3 == key)
    {
      doClickTemp3Button(key, event);
    }
    else if (DigitValueConstant.APP_DIGIT_VALUE__1 == key)
    {
      doClickTemp_1Button(key, event);
    }
  }

  void doClickTemp0Button(int key, event)
  {
  }

  void doClickTemp1Button(int key, event)
  {
  }

  void doClickTemp2Button(int key, event)
  {
    runPluginModel((item)
    {
      dynamic node = item.mObject;
      if (CxTextUtil.isEmptyList(node['children']))
      {
        Navigator.pop(node['context']);
      }
    });
  }

  void doClickTemp3Button(int key, event)
  {
  }

  void doClickTemp_1Button(int key, event)
  {
  }

  State getState<T>()
  {
    Map tables = widget.initPara[BasMapKeyConstant.MAP_KEY_TABLE];
    List<GlobalKey<State<StatefulWidget>>> keys = tables.keys.toList();
    State result;
    for (int i = 0; i < keys.length; i ++)
    {
      GlobalKey<State<StatefulWidget>> item = keys[i];
      if (item.currentState is T)
      {
        result = item.currentState;
        break;
      }
    }
    return result;
  }

  /*
   * 获取纯网络模板属性的值
   */
  Map<String, String> queryAttKeyValue(state)
  {
    // 需要重模版属性中获取数据所以需要依赖
    Map<String, String> map = {};

    List<Map> list = state.queryAttKeyValue();
    Iterator<Map> iterator = list.iterator;
    while (iterator.moveNext())
    {
      Map result = iterator.current;
      Fld fld = result[BasMapKeyConstant.MAP_KEY_FLD];
      dynamic value = result[BasMapKeyConstant.MAP_KEY_VALUE];
      String colname = fld.getColname();

      if (fld.getNullable() == DigitValueConstant.APP_DIGIT_VALUE_0 &&
          CxTextUtil.isEmptyObject(value))
      {
        map.clear();
        showToast(fld.getNamec() + MessageConstant.MSG_PARAM_NOT_NULL);
        break;
      }
      else if (fld.getColname() == HttpParamKeyValue.PARAM_KEY_COMMENT)
      {
        map[fld.getColname()] = UserMethod.getUser().getName() +
            BasStringValueConstant.STR_COMMON_A_COLON + value +
            BasStringValueConstant.STR_COMMON_A_COLON +
            widget.initPara[BasMapKeyConstant.MAP_KEY_NAME];
      }
      else if (value is FldValue)
      {
        map["FK_" + colname] = value.getDbvalue().toString();
      }
      else
      {
        map["FK_" + colname] = value;
      }
    }
    return map;
  }

  /*
   * 过滤属性字段和值
   */
  void filterAttKeyValue(Fld fld, Map result, Map map)
  {
    if (fld.getColname() == HttpParamKeyValue.PARAM_KEY_COMMENT)
    {
      String value = result[BasMapKeyConstant.MAP_KEY_VALUE];
      if (fld.getNullable() == DigitValueConstant.APP_DIGIT_VALUE_0 &&
          CxTextUtil.isEmpty(value))
      {
        map.clear();
        showToast(fld.getNamec() + MessageConstant.MSG_PARAM_NOT_NULL);
        return;
      }
      map[fld.getColname()] = UserMethod.getUser().getName() +
          BasStringValueConstant.STR_COMMON_A_COLON + value +
          BasStringValueConstant.STR_COMMON_A_COLON +
          widget.initPara[BasMapKeyConstant.MAP_KEY_NAME];
    }
    else
    {
      map["FK_" + fld.getColname()] = result[BasMapKeyConstant.MAP_KEY_VALUE];
    }
  }

  /*
   * 保存附件
   */
  void saveDuplicateDoc(state, {Map<String, dynamic> variables})
  {
    // 准备参数
    Map<String, dynamic> param = {
      DBFldConstant.FLD_MAJOR: widget.initPara[BasMapKeyConstant.MAP_KEY_ENTITY].major.toString(),
      DBFldConstant.FLD_MINOR: widget.initPara[BasMapKeyConstant.MAP_KEY_ENTITY].minor.toString(),
      DBFldConstant.FLD_MEDIA_ENTITYID: widget.initPara[BasMapKeyConstant.MAP_KEY_ATT][BasMapKeyConstant.MAP_KEY_ID],

    };

    if (!CxTextUtil.isEmptyMap(variables))
    {
      param.addAll(variables);
    }

    state.saveTempFile(param);
  }

  /*
   * 取流程参数
   * @method
   * @param   {Object}    att               属性(当前需要发起流程的属性)
   */
  void getProcVariables(Map<String, String> mProcVariables, dynamic variables, int key)
  {
    
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class TemplateBtnBasService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new TemplateBtnBas();
  }
}

