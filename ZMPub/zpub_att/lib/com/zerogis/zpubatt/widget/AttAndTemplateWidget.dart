import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/bean/template/att/Cxtmplfld.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/http/TemplateService.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

/*
 * 普通网络模板+属性组件<br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 必传} from  取模板的key
 * @param {Object 选传} id  id值
 * @param {Object 选传} mAttVals 属性对象
 * @param {Object 选传} mValueChangedMethod 网络层返回数据之后结果回调对象
 * @param {Object 选传} plugin 插件对象
 * @param {Object 选传} attState 属性状态
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttAndTemplateWidget extends AttWidget
{
  Map<String, dynamic> initPara;

  AttAndTemplateWidget(major, minor,
      {Key key, id = -1, attVals, plugin, ValueChanged<
          dynamic> mValueChangedMethod, this.initPara, attState: AttFldValueConstant.ATT_STATE_NEW_EDIT,})
      : super(
      major, minor, key: key,
      mId: id,
      mAttVals: attVals,
      mValueChangedMethod: mValueChangedMethod,
      attState: attState,
      plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttAndTemplateWidgetState();
  }

  static String toStrings()
  {
    return "AttAndTemplateWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class AttAndTemplateWidgetState<T extends AttAndTemplateWidget> extends AttWidgetState<T>
{
  /*
   * 所有条目模版
   */
  List<Cxtmplfld> _mCxtmplfld = <Cxtmplfld>[];

  /*
   * 查网络模版
   */
  void query()
  {
    super.query();
    TemplateService.queryTemplate(widget.initPara[BasMapKeyConstant.MAP_KEY_FROM], this);
  }

  /*
   * 是否过滤fld的条目
   * @param fld listview中一个条目生成的item值
   * return [false=过滤]
   */
  bool filterFldItem(Fld fld)
  {
    bool filter = false;
    Iterator<Cxtmplfld> iterator = _mCxtmplfld.iterator;
    while (iterator.moveNext())
    {
      if (iterator.current.getColname() == fld.getColname())
      {
        filter = true;
        break;
      }
    }
    return filter;
  }

  /*
   * 网络层接口回调
   */
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryTemplate")
    {
      setState(()
      {
        _mCxtmplfld = values;
      });
    }
    else if (method == "queryAttJo")
    {
      dealQueryAttJo(values);
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class TemplateWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new AttAndTemplateWidget(initPara[DBFldConstant.FLD_MAJOR],
        initPara[DBFldConstant.FLD_MINOR], id: initPara[DBFldConstant.FLD_ID],
        plugin: initPara,
        key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY]);
  }
}
