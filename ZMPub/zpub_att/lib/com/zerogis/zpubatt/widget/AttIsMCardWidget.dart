import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Fld.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldValuesManagerConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldValuesManager.dart';

/*
 * 根据主子类型生成属性查看组件:不传ID则不请求网络生成IsMcard属性：还需要加：fldvalue添加 <br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 选传} id  id值
 * @param {Object 选传} mAttVals 属性对象
 * @param {Object 选传} mValueChangedMethod 网络层返回数据之后结果回调对象
 * @param {Object 选传} plugin 插件对象
 * @param {Object 选传} attState 属性状态
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class AttIsMCardWidget extends AttWidget
{
  AttIsMCardWidget(major, minor,
      {Key key, id = -1, attVals, plugin, ValueChanged<dynamic> mValueChangedMethod, attState: AttFldValueConstant
          .ATT_STATE_DEFAULT,})
      : super(
      major, minor, key: key,
      mId: id,
      mAttVals: attVals,
      mValueChangedMethod: mValueChangedMethod,
      attState: attState,
      plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new AttIsMCardWidgetState();
  }

  static String toStrings()
  {
    return "AttIsMCardWidget";
  }
}

/*
 * 属性组件功能 <br/>
 */
class AttIsMCardWidgetState<T extends AttIsMCardWidget> extends AttWidgetState<T>
{
  Widget build(BuildContext context)
  {
    return Card(child: Column(children: createAttList(),));
  }

  /*
   * 创建属性集合的条目
   */
  List<Widget> createAttItemList()
  {
    mChildrenAttItem.clear();
    for (int i = 0; i < mFldList.length; i++)
    {
      Fld fld = mFldList[i];
      if (filterFldItem(fld))
      {
        GlobalKey key = createOrQueryGlobalKey(fld);
        initFlds(key, fld);
      }
    }
    return mChildrenAttItem.values.toList();
  }

  /*
   * 是否过滤fld的条目
   * @param fld listview中一个条目生成的item值
   * return [false=过滤]
   */
  bool filterFldItem(Fld fld)
  {
    if (fld.getIsmcard() == DigitValueConstant.APP_DIGIT_VALUE_1 &&
        fld.getDisporder() > DigitValueConstant.APP_DIGIT_VALUE_0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /*
   * 初始化数据输入值
   * @method
   * @param   {Fld}       flds              所有字段信息
   */
  void initFlds(GlobalKey key, Fld fld)
  {
    mChildrenAttItem[key] = AttWidgetCreator.createCommonText2(fld.getNamec(), disptypeValue(fld));
  }

  /*
   * 根据disptype返回value值
   */
  String disptypeValue(Fld fld)
  {
    if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_2)
    {
      // 下拉框
      return queryFldValueDispc(fld);
    }
    else if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_6)
    {
      // 下拉(可改)
      return mAttWidgetValues[fld.getColname()].toString();
    }
    if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_7)
    {
      // 自定义按钮
      return mAttWidgetValues[fld.getColname()].toString();
    }
    if (fld.disptype == DigitValueConstant.APP_DIGIT_VALUE_9)
    {
      // 动态选择
      return queryFldValueDispc(fld);
    }
    else
    {
      return mAttWidgetValues[fld.getColname()].toString();
    }
  }

  /*
   * 根据：表名和列名(tabName，colname) 查询到fldvalue集合(根据DbValue进行排好序的fldvalue集合)
   */
  dynamic queryFldValueDispc(Fld fld)
  {
    FldValuesManagerConstant fldValuesManagerConstant = FldValuesManager.getInstance();
    return fldValuesManagerConstant.queryFldValueDispc(
        fld.getTabname(), fld.getColname(), mAttWidgetValues[fld.getColname()].toString());
  }
}
