// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserDep.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserDep _$UserDepFromJson(Map<String, dynamic> json) {
  return UserDep(
      json['id'] as int,
      json['glid'] as int,
      json['creator'] as int,
      json['no'] as String,
      json['name'] as String,
      json['fno'] as String,
      json['hrno'] as String,
      json['nickname'] as String,
      json['gender'] as int,
      json['email'] as String,
      json['cell'] as String,
      json['telno'] as String,
      json['faxno'] as String,
      json['address'] as String,
      json['zipcode'] as String,
      json['jjlxr'] as String,
      json['jjlxdh'] as String,
      json['memo'] as String,
      json['organid'] as int,
      json['userid'] as int,
      json['organname'] as String,
      json['username'] as String,
      json['position'] as String);
}

Map<String, dynamic> _$UserDepToJson(UserDep instance) => <String, dynamic>{
      'id': instance.id,
      'glid': instance.glid,
      'creator': instance.creator,
      'no': instance.no,
      'name': instance.name,
      'fno': instance.fno,
      'hrno': instance.hrno,
      'nickname': instance.nickname,
      'gender': instance.gender,
      'email': instance.email,
      'cell': instance.cell,
      'telno': instance.telno,
      'faxno': instance.faxno,
      'address': instance.address,
      'zipcode': instance.zipcode,
      'jjlxr': instance.jjlxr,
      'jjlxdh': instance.jjlxdh,
      'memo': instance.memo,
      'organid': instance.organid,
      'userid': instance.userid,
      'organname': instance.organname,
      'username': instance.username,
      'position': instance.position
    };
