import 'dart:io';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/SvrExpConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/SvrExpTypesConstant.dart';

/*
 * 功能：附件模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SvrDocService extends BaseService
{
  /*
   * 查询附件<br/>
   * @param {Object 必传} _major 主类型
   * @param {Object 必传} _minor 子类型
   * @param {Object 必传} entityID 子类型
   * @param {Object 必传} _exp  条件
   * @param {Object 必传} types  类型
   * @param {Object 必传} vals   值
   * @param {Object 选传} param 键值对
   */
  static void queryMedia2(HttpListener listener, {Map<String, Object> param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_DocSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryMedia" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return response.data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryMedia" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "queryMedia" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询附件<br/>
   */
  static void queryMedia(Object major, Object minor, Object entityID, HttpListener listener)
  {
    String vals = major.toString() +
        BasStringValueConstant.STR_COMMON_COMMA +
        minor.toString() +
        BasStringValueConstant.STR_COMMON_COMMA +
        entityID.toString();
    BaseService.showProgressBar(listener, text: "正在查询附件");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_DocSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addParam(HttpParamKeyValue.PARAM_KEY_EXP, SvrExpConstant.EXP_MAJOR_MINOR_ENTITYID)
        .addParam(HttpParamKeyValue.PARAM_KEY_TYPES, SvrExpTypesConstant.EXP_TYPES_I_I_I)
        .addParam(HttpParamKeyValue.PARAM_KEY_VALS, vals)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("queryMedia", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return response.data;
    }).then((data)
    {
      BaseService.sendMessage("queryMedia", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("queryMedia", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 添加附件<br/>
   */
  static void createMedia(Object entityID, File file, HttpListener listener)
  {
    FuctionStateBase state = StateManager.getInstance().currentState();
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_DocSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Upload)
        .addParam(DBFldConstant.FLD_MEDIA_ENTITYID, entityID)
        .addParam(HttpParamKeyValue.PARAM_KEY_TEMP_FILE, 1)
        .addFilesSync(file)
        .uploadFile(onSendProgress: (int count, int total)
        {
            state.showToastShort("正在上传：${((count/total) * 100).toInt()}%");
        })
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("createMedia", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      jo[BasMapKeyConstant.MAP_KEY_PATH] = file.path;
      return jo;
    }).then((data)
    {
      BaseService.sendMessage("createMedia", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("createMedia", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 删除附件<br/>
   * @param ids 文档ID列表(多个文件ID之间用,分隔)
   * @param listener 当前UI对象
   */
  static void deleteMedia(String ids, HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在删除附件");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_DocSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Delete)
        .addParam(DBFldConstant.FLD_IDS, ids)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("deleteMedia", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          "deleteMedia", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          "deleteMedia", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }


  /*
   * 保存附件<br/>
   * @param filenames 举例：filenames: 0/2/0/282.jpg,0/2/0/283.jpg
   */
  static void saveTempFile(String filenames, HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在保存附件");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_DocSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_SaveTempFile)
        .addParam(DBFldConstant.FLD_FILENAME, filenames)
        .post()
        .then((response)
    {
      return MessageConstant.MSG_EMPTY;
    }).then((data)
    {
      BaseService.sendMessage("saveTempFile", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("saveTempFile", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 添加附件记录<br/>
   * @param map 举例：map = {major: 98, minor: 72, entityid: 65, task: 发起流程, titles: file, filenames: 0/3/0/307.file, filelens: 147330}
   */
  static void inserts(Map<String, Object> map, HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在添加附件");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_DocSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Inserts)
        .addParam(HttpParamKeyValue.PARAM_KEY_SYS, SysMajMinConstant.SYS_NO_21)
        .addParam(DBFldConstant.FLD_USERNAME, UserMethod.getUser().getName())
        .addParam("fid", 0)
        .addParam(DBFldConstant.FLD_GLID, 0)
        .addMap(map)
        .post()
        .then((response)
    {
      return MessageConstant.MSG_EMPTY;
    }).then((data)
    {
      BaseService.sendMessage("inserts", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("inserts", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 保存附件<br/>
   * @param filenames 举例：filenames: 0/2/0/282.jpg,0/2/0/283.jpg
   */
  static void downloadTempFile(HttpListener listener) async
  {
//    String sDCardDirq = (await getExternalStorageDirectory()).path;
//    print(sDCardDirq);

//    String sDCardDirb = (await getLibraryDirectory()).path;
//    print(sDCardDirb);

//    String sDCardDirc = (await getApplicationSupportDirectory()).path;
//    print(sDCardDirc);
//
//    String sDCardDird = (await getApplicationDocumentsDirectory()).path;
//    print(sDCardDird);
//
//    String sDCardDie = (await getTemporaryDirectory()).path+'/'+'test.xlsx';
//    print(sDCardDie);
//
//    String sDCardDir = (await getExternalStorageDirectory()).path+'/'+'test.xlsx';
//    print(sDCardDir);

    BaseService.showProgressBar(listener, text: "正在保存附件");
    HttpProtocol protocol = new HttpProtocol();
    protocol.setUrl('http://122.112.155.221:8081/testoa2/temp/2020-04-17.04.xlsx');
    protocol
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_SaveTempFile)
        .requestDownload('')
        .then((response)
    {
      print(response.data);
      return MessageConstant.MSG_EMPTY;
    }).then((data)
    {
      BaseService.sendMessage("saveTempFile", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("saveTempFile", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
