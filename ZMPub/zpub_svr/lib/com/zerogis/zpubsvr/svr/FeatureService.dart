import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/http/HttpProtocol.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import '../constant/HttpParamKeyValue.dart';

/*
 * 功能：服务-Feature 空间查询服务服务，针对entity表中type为12，13，14的图层要素表
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class FeatureService extends BaseService
{
  /*
    * 查询<br/>
    *
    * @param {_major	Number 必传}			实体主类型
    * @param {_minor	Number 必传}			实体子类型
    * @param {_exp	String 选传}		  	SQL条件表达式，参数用?代替，如 'fld1 = ? and fld2 like ?'
    * @param {cols	String 选传}	      需要取的列，默认取全部列（用,分隔）
    * @param {types	String 选传}	      exp中有？时必传，参数类型(用,分隔)(1/i/long-整数,2/r/double-实数,3/s/string-字符串,4/d/date-日期,5/t/datetime-日期时间)
    * @param {vals	String 选传}	      exp中有？时必传，参数值，与_exp参数一一对应(用separator参数分隔)
    * @param {orderby	String 选传}    	排序,默认不排序（降序 - fldname DESC, 升序 - fldname ASC）
    * @param {separator	String 选传}	   ','	数值中的分隔符（可以不传,默认是,）(考虑到字符串中可能有,可以设置特别分隔符)
    * @param {init	String 选传}	      0/1 首次调用为0, 非第一次为1
    * @param {pageno	String 选传}	    页码(1开始)
    * @param {pagesize	String 选传}   每页数据条数
    * @param {geometry  选传}	        0	是否返回图形数据，1-返回，0-不返回
    * @param {xy	String 选传}	        空间查询的范围坐标串，xy有值时开启空间查询模式，gtype必须要传
    * @param {gtype 选传}	            空间范围的几何对象类型，3-矩形，4-多边形，5-圆心半径圆，1-缓冲区圆
    * @param {gr 选传}                 0	圆形空间查询时的半径，gtype是圆时必须传
    *
    * @param {Object 选传} param 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void query(final HttpListener listener, {dynamic param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_Feature)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "query" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "query" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "query" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 取全部数据(内部调用query())<br/>
    *
    * @param {_major	Number 必传}			实体主类型
    * @param {_minor	Number 必传}			实体子类型
    * @param {params	Object 必传}			参数(除已给参数外，别的要求同query函数)
    *
    * @param {Object 选传} param 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryAll(final HttpListener listener, {dynamic param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_Feature)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryAll" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryAll" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "queryAll" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 取全部数据(内部调用query())<br/>
    *
    * @param {_major	Number 必传}			实体主类型
    * @param {_minor	Number 必传}			实体子类型
    * @param {params	Object 必传}			参数(除已给参数外，别的要求同query函数)
    *
    * @param {Object 选传} param 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryUnion(final HttpListener listener, {dynamic param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_Feature)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryUnion)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryUnion" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryUnion" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "queryUnion" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
