import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/HttpKeyValueConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';

/*
 * 功能：公共服务模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SvrAreaSvrService extends BaseService
{
  /*
   * 查询属性<br/>
   * @param {Object 必传} _major 主类型
   * @param {Object 必传} _minor 子类型
   * @param {Object 必传} id  条件
   */
  static void getAtt(HttpListener listener, {Map<String, Object> param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_GetAtt)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "getAtt" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "getAtt" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "getAtt" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 查询<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} _exp  条件
    * @param {Object 必传} types  类型
    * @param {Object 必传} vals   值
    * @param {Object 选传} map 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  @Deprecated('建议使用querys方法')
  static void query(int _major, int _minor, String _exp, String types, Object vals, final HttpListener listener,
      {dynamic map, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, _major)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, _minor)
        .addParam(HttpParamKeyValue.PARAM_KEY_EXP, _exp)
        .addParam(HttpParamKeyValue.PARAM_KEY_TYPES, types)
        .addParam(HttpParamKeyValue.PARAM_KEY_VALS, vals)
        .addMap(map)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "query" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return response.data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "query" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "query" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 查询<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} _exp  条件
    * @param {Object 必传} types  类型
    * @param {Object 必传} vals   值
    * @param {Object 选传} map 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
    * @param {Object 选传} tag   tag对象
   */
  static void querys(final HttpListener listener, {dynamic param, String method, dynamic tag})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "querys" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      jo[BasMapKeyConstant.MAP_KEY_TAG] = tag;
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "querys" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "querys" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }


  /*
    * 查询<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} _exp  条件
    * @param {Object 必传} types  类型
    * @param {Object 必传} vals   值
    * @param {Object 选传} map 键值对
    * @param {Object 选传} pager  pager对象
    * @param {Object 选传} page  第几页
    * @param {Object 选传} pageSize 每一页数默认15条
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
    * @param {Object 选传} tag   tag对象
   */
  static void queryPages(final HttpListener listener, {dynamic param, Pager pager, int page = 1, int pageSize: BasSoftwareConstant
      .PAGER_SIZE, String method, dynamic tag})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addMap(param)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryPages" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      Map<String, dynamic> data = {};
      data[BasMapKeyConstant.MAP_KEY_TAG] = tag;

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryPages" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(CxTextUtil.isEmpty(method) ? "queryPages" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
    * 查询分页<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} _exp  条件
    * @param {Object 必传} types  类型
    * @param {Object 必传} vals   值
    * @param {Object 选传} map 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryPage(int _major, int _minor, String _exp, String types, Object vals, Pager pager,
      final HttpListener listener,
      {Map<String, Object> param, String method, int page = 1, int pageSize: BasSoftwareConstant
          .PAGER_MIN_SIZE, String separator: BasStringValueConstant.STR_COMMON_COLON_A_COLON})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, _major)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, _minor)
        .addParam(HttpParamKeyValue.PARAM_KEY_EXP, _exp)
        .addParam(HttpParamKeyValue.PARAM_KEY_TYPES, types)
        .addParam(HttpParamKeyValue.PARAM_KEY_VALS, vals)
        .addParam(HttpParamKeyValue.PARAM_KEY_SEPARATOR, separator)
        .addMap(param)
        .addPagerParam(page)
        .addParam(HttpKeyValueConstant.PARAM_PAGER_SIZE, pageSize)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryPage" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }

      Map<String, dynamic> data = {};

      // 取page对象数据
      if (jo[BasMapKeyConstant.MAP_KEY_TOTAL] != null)
      {
        PagerUtil.createPage(
            pager ?? (pager = new Pager()), page, jo[BasMapKeyConstant.MAP_KEY_TOTAL], pageSizeValue: pageSize);
      }

      // 存list数据和page数据
      data[BasMapKeyConstant.MAP_KEY_LIST] = jo[BasMapKeyConstant.MAP_KEY_DATA] ?? response.data;
      if (pager.getPageNo() != null && !CxTextUtil.isEmptyList(data[BasMapKeyConstant.MAP_KEY_LIST]))
      {
        pager.setPageNo(pager.getPageNo() + 1);
        data[BasMapKeyConstant.MAP_KEY_PAGER] = pager;
      }
      return data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryPage" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryPage" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }


  /*
    * 联合查询<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} _exp  条件
    * @param {Object 必传} types  类型 举例：d,s
    * @param {Object 必传} vals   值 举例：2019-11-28,员工
    * @param {Object 必传} tables 值 举例： t;ud:99,17
    * @param {Object 必传} cols   值 举例： t:*
    * @param {Object 选传} map 键值对
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryUnion(int _major, int _minor, String _exp, String types, String vals, String tables, String cols,
      final HttpListener listener,
      {dynamic map, String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryUnion)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, _major)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, _minor)
        .addParam(HttpParamKeyValue.PARAM_KEY_EXP, _exp)
        .addParam(HttpParamKeyValue.PARAM_KEY_TYPES, types)
        .addParam(HttpParamKeyValue.PARAM_KEY_VALS, vals)
        .addParam(HttpParamKeyValue.PARAM_KEY_TABLES, tables)
        .addParam(HttpParamKeyValue.PARAM_KEY_COLS, cols)
        .addMap(map)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryUnion" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return response.data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryUnion" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryUnion" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }


  /*
   * 添加<br/>
   * @param {Object 必传} _major 主类型
   * @param {Object 必传} _minor 子类型
   * @param {Map 必传}    value  map对象
   * @param {String 必传} method 方法
   */
  static void add(int _major, int _minor, HttpListener listener, {Map<String, dynamic> value, String method})
  {
    BaseService.showProgressBar(listener, text: "正在添加");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Add)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, _major)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, _minor)
        .addMap(value)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "add" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "add" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "add" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 添加<br/>
   * @param {Object 必传} _major 主类型
   * @param {Object 必传} _minor 子类型
   * @param {Map 选传}    value  map对象
   * @param {int 选传}    retAtt 是否返回属性【默认值为0】
   * @param {String 必传} method 方法
   */
  static void addWithId(int _major, int _minor, HttpListener listener,
      {Map<String, dynamic> value, int retAtt: 0, String method})
  {
    BaseService.showProgressBar(listener, text: "正在添加");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_AddWithId)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, _major)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, _minor)
        .addParam(HttpParamKeyValue.PARAM_KEY_RETATT, retAtt)
        .addMap(value)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "addWithId" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "addWithId" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "addWithId" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 更新流程属性<br/>
   */
  static void updateAtt(HttpListener listener, {Map<String, Object> param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在更新");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_UpdateAtt)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "updateAtt" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "updateAtt" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "updateAtt" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 删除属性<br/>
   * @param {Object 必传} _major 主类型
   * @param {Object 必传} _minor 子类型
   * @param {Object 选传}    id
   */
  static void delete(HttpListener listener, {Map<String, Object> param, String method})
  {
    BaseService.showProgressBar(listener, text: "正在删除");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Delete)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "delete" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return jo;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "delete" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "delete" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
