import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';

/*
 * 功能：组织机构模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class TreeService extends BaseService
{
  /*
   * 查询Organ表<br/>
   */
  static void queryOrgan(final HttpListener listener)
  {
    BaseService.showProgressBar(listener, text: "正在查询组织机构");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Query)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, SysMajMinConstant.MAJOR_SYS)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, SysMajMinConstant.MINOR_SYS_ORGAN)
        .addParam(HttpParamKeyValue.PARAM_KEY_EXP, DBExpConstant.EXP_ID_MORE_THAN_QUESTION)
        .addParam(HttpParamKeyValue.PARAM_KEY_TYPES, DBExpTypesConstant.EXP_TYPES_I)
        .addParam(HttpParamKeyValue.PARAM_KEY_VALS, 0)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("queryOrgan", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      // 业务代码 升序排布
      List<dynamic> result = response.data as List;
      result.sort((dynamic a, dynamic b)
      {
        return a['id'].compareTo(b['id']);
      });
      return result;
    }).then((data)
    {
      BaseService.sendMessage("queryOrgan", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("queryOrgan", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询用户表和用户部门表<br/>
   */
  static void queryUnionUserOrg(List<dynamic> organs, HttpListener listener)
  {
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_AreaSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_QueryUnion)
        .addParam(HttpParamKeyValue.PARAM_KEY_MAJOR, SysMajMinConstant.MAJOR_SYS)
        .addParam(HttpParamKeyValue.PARAM_KEY_MINOR, SysMajMinConstant.MINOR_SYS_USER1)
        .addParam(HttpParamKeyValue.PARAM_KEY_TABLES, "u;ud:99,17")
        .addParam(HttpParamKeyValue.PARAM_KEY_EXP, "u.id=ud.userid and ud.organid>?")
        .addParam(HttpParamKeyValue.PARAM_KEY_COLS, "u:*;ud:*")
        .addParam(HttpParamKeyValue.PARAM_KEY_TYPES, DBExpTypesConstant.EXP_TYPES_I)
        .addParam(HttpParamKeyValue.PARAM_KEY_VALS, "0")
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid("queryUnionUserOrg", jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      // 业务代码

      // 将用户数据的ID，glid进行转换
      List obj = response.data as List;
      int organ = organs.last['id'];
      obj.forEach((item)
      {
        int id = item['id'];
        int glid = item['glid'];
        item['id'] = ++organ;
        item['_id'] = id;
        item['glid'] = item['organid'];
        item['_glid'] = glid;
      });

      // 将用户数据添加到部门集合
      organs.addAll(obj);

      // 通过父亲找孩子节点
      List<dynamic> result = TreeUtil.buildTree(organs, 0);
      return result;
    }).then((data)
    {
      BaseService.sendMessage("queryUnionUserOrg", data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage("queryUnionUserOrg", e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
