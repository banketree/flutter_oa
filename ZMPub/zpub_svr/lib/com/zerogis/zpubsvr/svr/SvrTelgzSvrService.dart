import 'dart:io';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/HttpKeyValueConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/MessageConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/constant/WhatConstant.dart';
import 'package:zpub_http/zpub_http.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_http/com/zerogis/zpubhttp/service/BaseService.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/UserMethod.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/SvrExpConstant.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/SvrExpTypesConstant.dart';

/*
 * 功能：电信相关服务模块
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class SvrTelgzSvrService extends BaseService
{
  /*
    * 缓冲区管网 gtype=3 矩形  4 多边形  5 圆  r-缓冲区半径<br/>
    * @param {Object 必传} _major 主类型
    * @param {Object 必传} _minor 子类型
    * @param {Object 必传} id  坐标
    * @param {Object 必传} listener 接口
    * @param {Object 选传} method 方法名称可以不传递
   */
  static void queryOBD(dynamic param, final HttpListener listener, {String method})
  {
    BaseService.showProgressBar(listener, text: "正在查询");
    HttpProtocol protocol = new HttpProtocol();
    protocol
        .setMethod(HttpParamKeyValue.PARAM_KEY_METHOD_TelgzSvr)
        .addParam(HttpParamKeyValue.PARAM_KEY_CMD, HttpParamKeyValue.PARAM_VALUE_Getobd)
        .addMap(param)
        .post()
        .then((response)
    {
      Map<String, dynamic> jo = BaseService.responseData(response);
      if (BaseService.isDataInvalid(CxTextUtil.isEmpty(method) ? "queryOBD" : method, jo, listener))
      {
        return MessageConstant.MSG_EMPTY;
      }
      return response.data;
    }).then((data)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryOBD" : method, data, WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e)
    {
      BaseService.sendMessage(
          CxTextUtil.isEmpty(method) ? "queryOBD" : method, e, WhatConstant.WHAT_EXCEPITON, listener);
    });
  }
}
