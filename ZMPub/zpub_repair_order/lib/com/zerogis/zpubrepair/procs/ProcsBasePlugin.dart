import 'package:flutter/material.dart';
import 'package:zpub_plugin/zpub_plugin.dart';

/*
 * 流程基类 <br/>
 * 为什么要流程处理类？
 *  1：工作流中每一个节点点击办理按钮的时候，可能需要一个类或者一个方法去处理那个节点的数据，InitSvr中已经定义好了
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
abstract class ProcsBasePlugin extends PluginStatefulBase
{
  Map<String, dynamic> mPros;

  Map<String, dynamic> attVals;

  ProcsBasePlugin({Key key, this.mPros, this.attVals, plugin})
      : super(key: key, plugin: plugin);
}

/*
 * 发起流程基类 <br/>
 */
abstract class ProcsBasePluginState<T extends ProcsBasePlugin> extends PluginBaseState<T>
{

}

/*
 * 类描述：流程处理基类
 *  为什么要流程处理类？
 *  1：工作流中每一个节点点击办理按钮的时候，可能需要一个类或者一个方法去处理那个节点的数据，InitSvr中已经定义好了
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
abstract class ProcBase
{
  /*
   * 流程变量值
   */
  Map<String, dynamic> mProcVariables = {};

  /*
   * 流程节点取流程变量
   * @param attValues 属性key和value值
   * @param proc 流程对象
   */
  void execute(Map<String, dynamic> attValues,dynamic proc);

  /*
   * 取流程变量
   * @param procVariables 提交complete流程变量
   * @param proc 流程对象
   */
  void getProcVariables(Map<String, String> procVariables,dynamic proc);
}