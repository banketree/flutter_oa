import 'package:flutter/material.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/bean/Pager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:weui/weui.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/plugin/PairDetailPlugin.dart';

/*
 * 插件参数配置基类(和内存中的参数进行匹配)<br/>
 * 举例：{"name":"处理环节","processName":"处理环节","createTime":"创建时间",
 * "variables":{"bill":“工单编号”,"depart":“测试部”,"applyUserName":“发起人”,"reason":“事由”}}
 *
 * 需要传入的键：<br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class PluginParamBas extends PluginStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  PluginParamBas({Key key, this.mInitPara, plugin}) :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PluginParamBasState();
  }

  static String toStrings()
  {
    return "PluginParamBas";
  }
}

/*
 * 页面功能 <br/>
 */
class PluginParamBasState<T extends PluginParamBas> extends PluginBaseState<T>
{
  /*
   * 分页对象
   */
  Pager m_pager = new Pager();

  /*
   * 查询过来的ismcard列表集合
   */
  List mList = new List();

  void initState()
  {
    super.initState();
    this.query(1);
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBodyWithRefresh(context, mEmptyView);
    return widget;
  }

  void query(int page)
  {
    if(page == DigitValueConstant.APP_DIGIT_VALUE_1)
    {
      mList.clear();
      mEmptyView = SysWidgetCreator.createCommonNoData();
    }
  }

  /*
   * 处理查询结果
   */
  void dealQuery(Object values)
  {
    if (values is Map && !CxTextUtil.isEmptyList(values[BasMapKeyConstant.MAP_KEY_LIST]))
    {
      setState(()
      {
        m_pager = values[BasMapKeyConstant.MAP_KEY_PAGER] ?? m_pager;
        mList.addAll(values[BasMapKeyConstant.MAP_KEY_LIST]);
        mEmptyView = SingleChildScrollView(child: Column(children: createBodyWidget()),);
      });
    }
  }

  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    mList.forEach((item)
    {
      widgets.add(GestureDetector(child: Card(child: Column(children: createMemoryList(item),)), onTap: ()
      {
        doClickListItem(item);
      },));
    });
    return widgets;
  }

  /*
   * 创建孩子组件,在内存中匹配组件
   * @param {Object 必传} item 一个条目
   */
  List<Widget> createMemoryList(item)
  {
    List<Widget> list = [];
    Map<String, dynamic> listitem = mInitParaMap[BasMapKeyConstant.MAP_KEY_LIST_ITEM];
    childParaInit(listitem, list, item);
    return list;
  }

  /*
   * 创建孩子组件,在内存中匹配组件
   * @param {Object 必传} item 一个条目
   */
  void childParaInit(Map<String, dynamic> listitem, list, item)
  {
    listitem.forEach((key, value)
    {
      if (value is Map)
      {
        childParaInit(value, list, item[key]);
      }
      else if (value is String)
      {
        list.add(AttWidgetCreator.createCommonText2(value, item[key]));
      }
    });
  }

  @override
  Future<void> loadMore()
  {
    if (m_pager.getPageNo() != null && (m_pager.getPageNo() < (m_pager.getTotalPage() + 1)))
    {
      query(m_pager.getPageNo());
    }
    else
    {
      setState(()
      {
        mLoadingText = "已加载全部";
      });
    }
    return super.loadMore();
  }

  @override
  Future<void> onRefresh()
  {
    query(1);
    return super.onRefresh();
  }

  void doClickListItem(item)
  {
    if(CxTextUtil.isEmptyList(mPlugins))
    {
      runPluginParam(PairDetailPlugin.toStrings(), item);
    }
    else
    {
      runPluginParam(mPlugins[0]['name'], item);
    }
  }
}
