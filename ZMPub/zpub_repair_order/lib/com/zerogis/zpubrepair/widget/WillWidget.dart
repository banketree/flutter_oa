import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_repair_order/com/zerogis/zpubrepair/widget/PluginParamBas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:weui/weui.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'WillWidget', '候选', '候选', 1, 3, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'WillWidget', '', '', '{"listitem":{"name":"处理环节","processName":"流程名称","createTime":"创建时间","variables":{"bill":"工单编号","depart":"测试部","applyUserName":"发起人","reason":"事由"}}}', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 候选组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class WillWidget extends PluginParamBas
{
  WillWidget({Key key, plugin, mInitPara}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new WillWidgetState();
  }

  static String toStrings()
  {
    return "WillWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class WillWidgetState<T extends WillWidget> extends PluginParamBasState<T>
{
  /*
   * 是否使用入栈操作
   */
  bool usePushStack()
  {
    return false;
  }

  void initState()
  {
    super.initState();
  }

  /*
   * 初始化相关
   */
  void init()
  {
    initData();
  }

  /*
   * 初始化数据相关
   */
  void initData()
  {
    queryInitPara();
    queryInitParaPlugins();
    queryWidgetsList();
  }

  Widget build(BuildContext context)
  {
    Widget widget = createCommonRefresh(mEmptyView);
    return widget;
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryUserTask")
    {
      dealQuery(values);
    }
    else if (method == "claimTask")
    {
      query(1);
      showToast(values);
    }
  }

  void query(int page)
  {
    super.query(page);
    SvrBpmService.queryUserTask(m_pager, this, page: page);
  }

  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    mList.forEach((item)
    {
      widgets.add(GestureDetector(child: Stack(children: <Widget>[
        Card(child: Column(children: createMemoryList(item),)),
        Positioned(child: WeButton(
          '签收',
          theme: WeButtonType.warn,
          size: WeButtonSize.mini,
          onClick: ()
          {
            doClickQS(item);
          },
        ), bottom: 10, right: 10,)
      ],),onTap: ()
      {
        doClickListItem(item);
      },));
    });
    return widgets;
  }

  void doClickListItem(item)
  {
    Map<String, dynamic> param = {};
    EntityManagerConstant entityManagerConstant = EntityManager.getInstance();
    String tabbatt = item[SvrBPMConstant.SVR_VARIABLES][BasMapKeyConstant.MAP_KEY_TABLE];
    List<int> list = entityManagerConstant.queryEntityMajorMinor(tabbatt);
    param[DBFldConstant.FLD_MAJOR] = list[0];
    param[DBFldConstant.FLD_MINOR] = list[1];
    param[BasMapKeyConstant.MAP_KEY_ATT] = item[SvrBPMConstant.SVR_VARIABLES];
    param[BasMapKeyConstant.MAP_KEY_VALUE_TITLE] = mTitle;
    param[AttConstant.ATT_STATE] = AttFldValueConstant.ATT_STATE_DEFAULT;
    runPluginParam(AttFjPlugin.toStrings(), param);
  }

  void doClickQS(item)
  {
    SvrBpmService.claimTask(item[BasMapKeyConstant.MAP_KEY_ID], this);
  }

  @override
  Future<void> onRefresh()
  {
    query(1);
    return super.onRefresh();
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class WillWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new WillWidget(mInitPara: initPara, key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
