import 'package:flutter/material.dart';
import 'package:zpub_svr/com/zerogis/zpubsvr/constant/HttpParamKeyValue.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

import 'CtrlDocWidgetBas.dart';

/*
 * 网络组件：附件组件,支持本地，网络图片显示，支持查看，编辑,新增 <br/>
 * @param {Object 必传} isEdit   是否为编辑【true=编辑状态】
 * @param {Object 必传} nullable 是否为可空[1=不可空，必须要上传附件]
 * @param {Object 必传} initPara  初始化参数{major:1,minor:1,id:1}
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlPngWidget extends CtrlDocWidgetBas {
  CtrlPngWidget({Key key, plugin, initPara}) : super(key: key, plugin: plugin, initPara: initPara);

  State<StatefulWidget> createState() {
    return new CtrlPngWidgetState();
  }

  static String toStrings() {
    return "CtrlPngWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class CtrlPngWidgetState extends CtrlDocWidgetBasState<CtrlPngWidget> {

  Widget build(BuildContext context)
  {
    return new UIPngWidget(
      mMediaList: mediaList,
      mIsEdit: widget.initPara[BasMapKeyConstant.MAP_KEY_ISEDIT],
      mNullable: widget.initPara[BasMapKeyConstant.MAP_KEY_NULLABLE],
      valueChangedMethod: (item)
      {
        doClickDuplicateWidget(item);
      },
      valueDeleteMethod: (map)
      {
        if (map[BasMapKeyConstant.MAP_KEY_PATH].contains(BasSoftwareConstant.NETWORK_HTTP) ||
            map[BasMapKeyConstant.MAP_KEY_PATH].contains(BasSoftwareConstant.NETWORK_HTTPS))
        {
          dynamic item = mQueryMedia[map[BasMapKeyConstant.MAP_KEY_POSITION]];
          SvrDocService.deleteMedia(item[BasMapKeyConstant.MAP_KEY_ID].toString(), this);
        }
        else
        {
          Map<String, dynamic> item;
          for (int i = 0; i < mMediaUpload.length; i ++)
          {
            Map<String, dynamic> index = mMediaUpload[i];
            if (map[BasMapKeyConstant.MAP_KEY_PATH] == index[BasMapKeyConstant.MAP_KEY_PATH])
            {
              item = index;
              break;
            }
          }
          if (!CxTextUtil.isEmptyMap(item))
          {
            mMediaUpload.remove(item);
          }
        }
      },);
  }

  /*
   * 网络层接口回调
   */
  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryMedia")
    {
      SysCfgManager sysCfgManager = SysCfgManager.getInstance();
      List result = sysCfgManager.queryForamt(values, DBFldConstant.FLD_SYSCFG_IMG);
      super.onNetWorkSucceed(method, result);
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  /*
   * 查询图片
   */
  void query() {
    if (!CxTextUtil.isEmptyObject(widget.initPara) &&
        !CxTextUtil.isEmptyObject(widget.initPara[DBFldConstant.FLD_ID])) {
      Map<String, Object> param = {
        HttpParamKeyValue.PARAM_KEY_EXP: 'major=? AND minor=? AND entityid=?',
        HttpParamKeyValue.PARAM_KEY_TYPES: 'i,i,i',
        HttpParamKeyValue.PARAM_KEY_VALS: widget.initPara[DBFldConstant.FLD_MAJOR].toString() +
            BasStringValueConstant.STR_COMMON_COMMA +
            widget.initPara[DBFldConstant.FLD_MINOR].toString() +
            BasStringValueConstant.STR_COMMON_COMMA +
            widget.initPara[DBFldConstant.FLD_ID].toString()
      };
      SvrDocService.queryMedia2(this, param: param);
    }
  }

  /*
   * 写入media表数据
   */
  void inserts(Map<String, Object> map) {
    String title = ArrayUtil.mapKeyToSubString(mMediaUpload, "title");
    String filename = ArrayUtil.mapKeyToString(mMediaUpload, "filename");
    String filelens = ArrayUtil.mapKeyToString(mMediaUpload, "filelength");
    map["titles"] = title.length > 60 ? title.substring((title.length * 0.5) as int, title.length - 1) : title;
    map["filenames"] = filename;
    map["filelens"] = filelens;
    map["bustype"] = DBFldValueConstant.MEDIA_BUSYTPE_DBVALUE_8;
    SvrDocService.inserts(map, this);
  }
}

/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class CtrlPngWidgetService extends InterfaceBaseImpl {
  @override
  Widget runWidget({dynamic initPara}) {
    return new CtrlPngWidget(initPara: initPara);
  }
}
