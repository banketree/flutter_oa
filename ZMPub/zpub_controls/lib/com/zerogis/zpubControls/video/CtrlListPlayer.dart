import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/constant/CtrlBigDataKeyConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_ui/zpub_ui.dart';

/*
 * 列表播放-支持网络播放和本地播放 <br/>
 * @param {Object 必传} initParam['url'] url        播放地址
 * @param {Object 必传} initParam['height'] height  高度-默认高度是300
 * @param {Object 必传} initParam['width']  width   高度-默认高度是300
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
abstract class CtrlListPlayer extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CtrlListPlayer({this.initParam, plugin,Key key,}) : super(key: key, plugin: plugin);
}

abstract class CtrlListPlayerState<T extends CtrlListPlayer> extends WidgetBaseState<T>
{
  void initState()
  {
    super.initState();
    query();
  }

  void initView()
  {
    // 给柱状图设置一个默认的UI配置
    if(widget.initParam[CtrlBigDataKeyConstant.UI_CFG] == null)
    {
      return;
    }
    Map map = widget.initParam[CtrlBigDataKeyConstant.UI_CFG];
    if(map[ChartKeyConstant.ANIMATE] == null)
    {
      map[ChartKeyConstant.ANIMATE] = true;
    }
    if(map[BasMapKeyConstant.MAP_KEY_WIDTH] == null)
    {
      map[BasMapKeyConstant.MAP_KEY_WIDTH] = ScreenUtil.getInstance().getScreenWidth();
    }
    if(map[BasMapKeyConstant.MAP_KEY_HEIGHT] == null)
    {
      map[BasMapKeyConstant.MAP_KEY_HEIGHT] = 200.0;
    }
    if(map[ChartKeyConstant.VERTICAL] == null)
    {
      map[ChartKeyConstant.VERTICAL] = false;
    }
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "querys" && values is Map)
    {
      List list = values[BasMapKeyConstant.MAP_KEY_DATA];
      setState(()
      {
        widget.initParam[CtrlBigDataKeyConstant.UI_CFG][UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.TEXT] = list.length;
      });
    }
  }

  // 查询被统计的数据
  void query()
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[DBFldConstant.FLD_MAJOR]))
    {
      return;
    }

    Map<String, Object> param =
    {
      HttpParamKeyValue.PARAM_KEY_MAJOR: widget.initParam[DBFldConstant.FLD_MAJOR],
      HttpParamKeyValue.PARAM_KEY_MINOR: widget.initParam[DBFldConstant.FLD_MINOR],
      HttpParamKeyValue.PARAM_KEY_EXP: widget.initParam[HttpParamKeyValue.PARAM_KEY_EXP],
      HttpParamKeyValue.PARAM_KEY_TYPES: widget.initParam[HttpParamKeyValue.PARAM_KEY_TYPES],
      HttpParamKeyValue.PARAM_KEY_VALS: widget.initParam[HttpParamKeyValue.PARAM_KEY_VALS],
      HttpParamKeyValue.PARAM_KEY_SEPARATOR: widget.initParam[HttpParamKeyValue.PARAM_KEY_SEPARATOR]??BasStringValueConstant.STR_COMMON_COLON_A_COLON,
    };
    SvrAreaSvrService.querys(this,param: param);
  }

  /*
   * 取满足条件数据
   * 计算fld中有多少个数量
   */
  Map gets2()
  {
    List list = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_LIST];
    String fld = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD];
    return ArrayUtil.gets2(list, fld);
  }
}
