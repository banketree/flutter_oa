import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：通用模块组件启动注册模块：可以理解成Android中的AndroidManifest.xml
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class CtrlWidgetApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
//      if (plugin.classurl == TodoWidget.toStrings() &&
//          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_3)
//      { // 待办组件
//        WidgetsFactory.getInstance().add(plugin.name, new TodoWidgetService());
//      }
    });
  }
}
