import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

/*
 * 图层树组件(控制图层显示与隐藏) 树节点中必须包含：'layer', 'glid' ，'icon'信息<br/>
 * @param {Object 必传} valueChangedMethod   点击事件
 * @param {Object 选传} treeData             树控件数据:[{'id':1,'icon':2043,'layer':1,'glid':0,'childern':[{'id':1,'icon':2043,'layer':0,'glid':1}]}]
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class LayerControlWidget extends WidgetStatefulBase
{
  /*
   * 点击事件
   */
  ValueChanged<dynamic> valueChangedMethod;

  LayerControlWidget({Key key, plugin, @required this.valueChangedMethod})
      : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new LayerControlWidgetState();
  }

  static String toStrings()
  {
    return "LayerControlWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class LayerControlWidgetState extends WidgetBaseState<LayerControlWidget>
{
  /*
   * 树控件数据:[{'id':1,'glid':2,'childern':[{'id':1,'glid':1}]}]
   */
  List<dynamic> treeData;

  void initState()
  {
    super.initState();
    LayerManagerConstant layerManagerConstant = LayerManager.getInstance();
    treeData = layerManagerConstant.queryTreeLayerCheck();
  }

  Widget build(BuildContext context)
  {
    return createTree();
  }

  /*
   * 创建树
   */
  Widget createTree()
  {
    if (CxTextUtil.isEmptyList(treeData))
    {
      return mEmptyView;
    }
    else
    {
      return Container(width: ScreenUtil.getInstance().getScreenWidth() * 0.25,
        height: ScreenUtil.getInstance().getScreenHeight(),
        child: Tree(treeData, onTap: (node)
        {
          widget.valueChangedMethod(node);
        }, mTextSize: 18),);
    }
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class LayerControlWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new LayerControlWidget(
      key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],
      valueChangedMethod: initPara[BasMapKeyConstant.MAP_KEY_VALUE_CHANGE_METHOD],);
  }
}
