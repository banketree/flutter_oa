import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/constant/CtrlBigDataKeyConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_ui/zpub_ui.dart';

/*
 * 查数量,支持传数据(内存)和网络查数据:(例子项目数量76 个) <br/>
 * @param {Object 必传} initParam['uicfg'] ui配置项
 * @param {Object 必传} initParam['uicfg']['head'] text  总投资
 * @param {Object 选传} initParam['uicfg']['head'] fontSize 大小
 * @param {Object 选传} initParam['uicfg']['head'] color    颜色 [默认=0xfff69c1b]
 *
 * @param {Object 必传} initParam['uicfg']['foot'] text   2080
 * @param {Object 必传} initParam['uicfg']['foot'] fontSize 大小
 * @param {Object 选传} initParam['uicfg']['foot'] color    颜色 [默认=0xfff69c1b]
 *
 * @param {Object 必传} initParam['uicfg']['foot']['dw'] text   万元
 * @param {Object 选传} initParam['uicfg']['foot']['dw'] fontSize  大小
 *
 * 自己查数据
 * @param {Object 选传} initParam['major']  主类型
 * @param {Object 选传} initParam['minor']  子类型
 * @param {Object 选传} initParam['exp']    条件
 * @param {Object 选传} initParam['types']  类型
 * @param {Object 选传} initParam['vals']   值
 * @param {Object 选传} initParam['separator']  分隔符
 * @param {Object 选传} initParam['map']         键值对
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlCardData extends WidgetStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CtrlCardData({this.initParam, plugin,Key key,}) : super(key: key, plugin: plugin);

  @override
  CtrlCardDataState createState()
  => CtrlCardDataState();
}

class CtrlCardDataState extends WidgetBaseState<CtrlCardData>
{
  void initState()
  {
    super.initState();
    query();
  }

  @override
  Widget build(BuildContext context)
  {
    return CardData(initParam: widget.initParam[CtrlBigDataKeyConstant.UI_CFG],);
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "querys" && values is Map)
    {
      List list = values[BasMapKeyConstant.MAP_KEY_DATA];
      setState(()
      {
        widget.initParam[CtrlBigDataKeyConstant.UI_CFG][UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.TEXT] = list.length;
      });
    }
  }

  void query()
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[DBFldConstant.FLD_MAJOR]))
    {
      return;
    }

    Map<String, Object> param =
    {
      HttpParamKeyValue.PARAM_KEY_MAJOR: widget.initParam[DBFldConstant.FLD_MAJOR],
      HttpParamKeyValue.PARAM_KEY_MINOR: widget.initParam[DBFldConstant.FLD_MINOR],
      HttpParamKeyValue.PARAM_KEY_EXP: widget.initParam[HttpParamKeyValue.PARAM_KEY_EXP],
      HttpParamKeyValue.PARAM_KEY_TYPES: widget.initParam[HttpParamKeyValue.PARAM_KEY_TYPES],
      HttpParamKeyValue.PARAM_KEY_VALS: widget.initParam[HttpParamKeyValue.PARAM_KEY_VALS],
      HttpParamKeyValue.PARAM_KEY_SEPARATOR: widget.initParam[HttpParamKeyValue.PARAM_KEY_SEPARATOR]??BasStringValueConstant.STR_COMMON_COLON_A_COLON,
    };
    SvrAreaSvrService.querys(this,param: param);
  }
}
