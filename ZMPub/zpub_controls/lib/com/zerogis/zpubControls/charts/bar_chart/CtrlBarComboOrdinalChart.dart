import 'package:flutter/material.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/charts/base/CtrlChartBas.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/constant/CtrlBigDataKeyConstant.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:charts_flutter/flutter.dart' as charts;

/*
 * 数量统计,柱状组合，依次，有序图,支持传数据(内存)和网络查数据<br/>
 * @param {Object 必传} initParam['uicfg'] ui配置项
 * @param {Object 选传} initParam['uicfg']['series'] text  总投资
 * @param {Object 选传} initParam['uicfg']['animate'] bool  动画效果
 * @param {Object 选传} initParam['uicfg']['vertical'] bool  垂直或水平
 * @param {Object 选传} initParam['uicfg']['width'] bool   宽度
 * @param {Object 选传} initParam['uicfg']['height'] bool  高度
 *
 *
 * 自己查数据
 * @param {Object 选传} initParam['major']  主类型
 * @param {Object 选传} initParam['minor']  子类型
 * @param {Object 选传} initParam['exp']    条件
 * @param {Object 选传} initParam['types']  类型
 * @param {Object 选传} initParam['vals']   值
 * @param {Object 选传} initParam['separator']  分隔符
 * @param {Object 选传} initParam['map']         键值对
 *
 * 否则:内存查
 * @param {Object 选传} initParam['data']
 * @param {Object 选传} initParam['data']['list']  被统计的集合对象
 * @param {Object 选传} initParam['data']['fld']   对哪个字段统计
 * @param {Object 选传} initParam['data']['fld2']  对被分组的哪个字段统计
 * @param {Object 必传} initParam['data']['colors']  对被分组的哪个字段统计 {'key':0xff2ec7c9,'key':0xff2ec7c9,'key':0xff2ec7c9}
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlBarComboOrdinalChart extends CtrlChartBas
{
  CtrlBarComboOrdinalChart({initParam, plugin,Key key,}) : super(initParam: initParam,key: key, plugin: plugin);

  @override
  CtrlBarComboOrdinalChartState createState()
  => CtrlBarComboOrdinalChartState();
}

class CtrlBarComboOrdinalChartState extends CtrlChartBasState<CtrlBarComboOrdinalChart>
{
  void initView()
  {
    super.initView();
    Map map = widget.initParam[CtrlBigDataKeyConstant.UI_CFG];
    if(map[ChartKeyConstant.SERIES] == null)
    {
      map[ChartKeyConstant.SERIES] = BarComboOrdinalChart.createSampleData();
    }
  }

  @override
  Widget build(BuildContext context)
  {
    return BarComboOrdinalChart(initParam: widget.initParam[CtrlBigDataKeyConstant.UI_CFG],);
  }

  /*
   * 数量统计计算
   */
  void statistics()
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[BasMapKeyConstant.MAP_KEY_DATA]))
    {
      return;
    }

    // 统计图表配置
    List list = widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_LIST];
    Map prjphase = ArrayUtil.gets2(list, widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD2]);
    Map city = gets2();
    dynamic colors = widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.COLORS];

    // 大柱子
    List listPrj = prjphase.keys.toList();
    List listCity = city.keys.toList();
    List result = [];
    for(int j = 0;j < city.keys.length; j ++)
    {// 小柱子
      List<OrdinalSales> desktopSalesData = [];
      for(int i = 0; i < prjphase.keys.length; i ++)
      {// 大柱子
        // 取毕节市下面的方案设计
        List bjs = city[listCity[j]];
        List<dynamic> bjsFasj = ArrayUtil.gets(bjs, {widget.initParam[BasMapKeyConstant.MAP_KEY_DATA][BasMapKeyConstant.MAP_KEY_FLD2]:listPrj[i]});

        OrdinalSales ordinalSales = OrdinalSales(listPrj[i], bjsFasj.length);
        // 设置颜色
        dynamic color = colors[listCity[j]];
        if(!CxTextUtil.isEmptyObject(color))
        {
          if(color is List)
          {
            ordinalSales.color = charts.ColorUtil.fromDartColor(Color.fromARGB(255, color[0], color[1], color[2]));
          }
          else
          {
            ordinalSales.color = charts.ColorUtil.fromDartColor(Color(color));
          }
        }
        desktopSalesData.add(ordinalSales);
      }
      result.add(desktopSalesData);
    }
    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = BarComboOrdinalChart.createSampleData2(result);
  }

  /*
   * 外面计算，重新刷新数据
   * @param data = [[Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales'],
   *  [Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales'],
   *  [Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales']]
   */
  createSampleData2(data)
  {
    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = BarComboOrdinalChart.createSampleData2(data);
  }

  /*
   * 计算统计图需要的数据
   * @param city     = 被统计的x轴，比如：x轴上显示城市：毕节市，遵义市，六盘水市               结构：【'毕节市':[],'遵义市':[],'六盘水市':[]】
   * @param paybatch = 具体被统计的值 比如：毕节市下面中央资金是多少钱                        结构：【'中央资金':[],'省级资金':[],'地方资金':[]】
   * @param cityKey =  取paybatch中相同城市下面的集合  举例：中央资金value的值匹配毕节市的集合  结构：city
   * @param paynum =   具体被统计的值进行相加   具体被计算的值                               结构：paynum
   *
   * @retrun [[Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales'],
   *  [Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales'],
   *  [Instance of 'OrdinalSales', Instance of 'OrdinalSales', Instance of 'OrdinalSales']]
   */
  calculation(Map city, Map paybatch, String cityKey, String paynum)
  {
    dynamic colors = widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.COLORS];
    List listCity = city.keys.toList();// 大柱子
    List listPayBatch = paybatch.keys.toList();
    List result = [];
    for(int j = 0;j < paybatch.keys.length; j ++)
    { // 小柱子
      List<OrdinalSales> desktopSalesData = [];
      for(int i = 0; i < city.keys.length; i ++)
      { // 大柱子
        List zyzj = paybatch[listPayBatch[j]];
        List<dynamic> bjsFasj = ArrayUtil.gets(zyzj, {cityKey:listCity[i]});
        double sum = 0.0;
        bjsFasj.forEach((item)
        {
          if(item[paynum] is int)
          {
            sum+=item[paynum];
          }
        });

        String listCityItem = listCity[i];
        OrdinalSales ordinalSales = OrdinalSales(listCityItem.substring(0, listPayBatch.length > listCityItem.length ? listCityItem.length : listPayBatch.length), sum.toInt());
        // 设置颜色
        dynamic color = colors[listPayBatch[j]];
        if(!CxTextUtil.isEmptyObject(color))
        {
          if(color is List)
          {
            ordinalSales.color = charts.ColorUtil.fromDartColor(Color.fromARGB(255, color[0], color[1], color[2]));
          }
          else
          {
            ordinalSales.color = charts.ColorUtil.fromDartColor(Color(color));
          }
        }
        desktopSalesData.add(ordinalSales);
      }
      result.add(desktopSalesData);
    }
    return result;
  }
}
