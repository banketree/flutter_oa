import 'package:flutter/material.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/charts/base/CtrlChartBas.dart';
import 'package:zpub_controls/com/zerogis/zpubControls/constant/CtrlBigDataKeyConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_ui/zpub_ui.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 数量统计,支持传数据(内存)和网络查数据：包含一些简单的API外面可以直接调用<br/>
 * @param {Object 必传} initParam['uicfg'] ui配置项
 * @param {Object 选传} initParam['uicfg']['series'] text  总投资
 * @param {Object 选传} initParam['uicfg']['animate'] bool  动画效果
 * @param {Object 选传} initParam['uicfg']['vertical'] bool  垂直或水平
 * @param {Object 选传} initParam['uicfg']['width'] bool   宽度
 * @param {Object 选传} initParam['uicfg']['height'] bool  高度
 *
 *
 * 自己查数据
 * @param {Object 选传} initParam['major']  主类型
 * @param {Object 选传} initParam['minor']  子类型
 * @param {Object 选传} initParam['exp']    条件
 * @param {Object 选传} initParam['types']  类型
 * @param {Object 选传} initParam['vals']   值
 * @param {Object 选传} initParam['separator']  分隔符
 * @param {Object 选传} initParam['map']         键值对
 *
 * 否则:内存查
 * @param {Object 选传} initParam['data']
 * @param {Object 选传} initParam['data']['list']  被统计的集合对象
 * @param {Object 选传} initParam['data']['fld']   对哪个字段统计
 * @param {Object 选传} initParam['data']['colors'] 柱状图颜色
 *
 *  搞一个空的缺点：
 *    重新刷新数据的时候有问题
 *  搞一个有默认数据的
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CtrlBarChart extends CtrlChartBas
{
  CtrlBarChart({initParam, plugin,Key key,}) : super(initParam: initParam,key: key, plugin: plugin);

  @override
  CtrlBarChartState createState()
  => CtrlBarChartState();
}

class CtrlBarChartState extends CtrlChartBasState<CtrlBarChart>
{
  void initView()
  {
    super.initView();
    Map map = widget.initParam[CtrlBigDataKeyConstant.UI_CFG];
    if(map[ChartKeyConstant.SERIES] == null)
    {
      map[ChartKeyConstant.SERIES] = BarChart.createSampleData();
    }
  }
  @override
  Widget build(BuildContext context)
  {
    return BarChart(initParam: widget.initParam[CtrlBigDataKeyConstant.UI_CFG],);
  }

  /*
   * 数量统计计算
   */
  void statistics()
  {
    if(CxTextUtil.isEmptyObject(widget.initParam[BasMapKeyConstant.MAP_KEY_DATA]))
    {
      return;
    }

    Map map = gets2();

    // 统计图表配置
    List<OrdinalSales> data = [];
    map.forEach((key,value)
    {
      data.add(OrdinalSales(key, value.length));
    });
    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = BarChart.createSampleData2(data);
  }
  
  /*
   * 外面计算，重新刷新数据
   */
  createSampleData2(data)
  {
    widget.initParam[CtrlBigDataKeyConstant.UI_CFG][ChartKeyConstant.SERIES] = BarChart.createSampleData2(data);
  }
}
