import 'package:event_bus/event_bus.dart';

/*
 * 类描述：第三方库的初始化
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class ThirdApplication
{
  static ThirdApplication mInstance;

  EventBus mEventBus;

  static ThirdApplication getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new ThirdApplication();
    }
    return mInstance;
  }

  ThirdApplication()
  {
    onCreate();
  }

  void onCreate()
  {
    initEventBus();
  }

  /*
   * 初始化EventBus对象
   */
  void initEventBus()
  {
    mEventBus = new EventBus();
  }

  EventBus getEventBus()
  {
    return mEventBus;
  }
}
