library zpub_bas;

// FuctionStateFul基类
export './com/zerogis/zpubbase/plugin/FuctionStateFulBase.dart';

// 组件库,用于创建常用的组件：类似Android中layout文件中common_progressbar.xml相关
export './com/zerogis/zpubbase/core/SysWidgetCreator.dart';

// 数据响应事件
export './com/zerogis/zpubbase/listener/HttpListener.dart';

//activity自定义栈
export './com/zerogis/zpubbase/manager/StateManager.dart';

// 文本工具类
export './com/zerogis/zpubbase/util/CxTextUtil.dart';
export './com/zerogis/zpubbase/util/TimeUtil.dart';
// Log统一管理类
export './com/zerogis/zpubbase/util/Log.dart';
// 数组处理工具类
export './com/zerogis/zpubbase/util/ArrayUtil.dart';
// Assets工具类
export './com/zerogis/zpubbase/util/AssetsUtil.dart';
// 字符串工具类
export './com/zerogis/zpubbase/util/StringUtil.dart';
export './com/zerogis/zpubbase/util/TreeUtil.dart';
export './com/zerogis/zpubbase/util/ScreenUtil.dart';
export './com/zerogis/zpubbase/util/ObjUtil.dart';

// 颜色类似Android资源color.xml文件相关
export './com/zerogis/zpubbase/resource/BasColorRes.dart';
export './com/zerogis/zpubbase/resource/BasStringRes.dart';
export './com/zerogis/zpubbase/resource/BasSizeRes.dart';
export './com/zerogis/zpubbase/resource/BasIdsRes.dart';
export './com/zerogis/zpubbase/resource/BasTextStyleRes.dart';


// 注册中心服务基类
export './com/zerogis/zpubbase/slot/impl/InterfaceBaseImpl.dart';

// A界面调用B界面的函数  传递参数调用B界面的函数且返回数据给A界面
export './com/zerogis/zpubbase/slot/factory/FunctionFactory.dart';

// 插件集合类
export './com/zerogis/zpubbase/slot/factory/PluginsFactory.dart';

// 组件集合类
export './com/zerogis/zpubbase/slot/factory/WidgetsFactory.dart';
export './com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
export './com/zerogis/zpubbase/constant/BasEvnBusKeyConstant.dart';
export './com/zerogis/zpubbase/util/PagerUtil.dart';
export './com/zerogis/zpubbase/constant/BasSoftwareConstant.dart';
export './com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
export './com/zerogis/zpubbase/constant/DigitValueConstant.dart';
export './com/zerogis/zpubbase/bean/Pager.dart';
export './com/zerogis/zpubbase/bean/BasMsgEvent.dart';
export './com/zerogis/zpubbase/util/FileUtil.dart';
export './com/zerogis/zpubbase/util/BasUtil.dart';



