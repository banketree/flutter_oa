import 'dart:ui';

import 'package:flutter/material.dart';

/*
 * 功能：颜色类似Android资源ids.xml文件相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class BasIdsRes
{
//  <!-- common_root相关 -->
//  <item name="common_root" type="id"></item>
//  <item name="viewgroup_common_content" type="id"></item>
//  <item name="viewgroup_common_title" type="id"></item>
//  <item name="viewgroup_common_title_content" type="id"></item>
//  <item name="viewgroup_common_title_move" type="id"></item>
//  <item name="viewgroup_common_title_immersion_fullscreen" type="id"></item>
//  <item name="toolbar_title_view" type="id"></item>
//  <item name="contentView" type="id"></item>
//  <item name="customTitleView" type="id"></item>
//
//  <!-- 标题栏相关 -->
//  <item name="toolbar" type="id"></item>
//  <item name="tv_title" type="id"></item>
//  <item name="btn_back" type="id"></item>
//  <item name="btn_right" type="id"></item>
//  <item name="iv_border" type="id"></item>
//
//  <!-- SearchView相关 -->
//  <item name="btn_search" type="id"></item>
//
//  <!-- ProgressBar相关 -->
//  <item name="viewgroup_progressbar" type="id"></item>
//  <item name="tv_progressbar" type="id"></item>
//
//  <!-- Tab相关 -->
//  <item name="tab_parent" type="id"></item>
//  <item name="tab_tv_text" type="id"></item>
//  <item name="tab_iv_icon" type="id"></item>
//
//  <!-- View相关 -->
//  <item name="refreshListView" type="id"></item>
//  <item name="refreshGridView" type="id"></item>
//  <item name="refreshScrollView" type="id"></item>
//  <item name="listView" type="id"></item>
//  <item name="gridView" type="id"></item>
//  <item name="tv_empty" type="id"></item>
//  <item name="emptyParentView" type="id"></item>
//  <item name="searchView" type="id"></item>
//  <item name="viewPager" type="id"></item>
//  <item name="scrollView" type="id"></item>
//  <item name="webView" type="id"></item>
//  <item name="net_indicate_view" type="id"></item>
//  <item name="expandableListView" type="id"></item>
//  <item name="spinnerView" type="id"></item>
//  <item name="mapView" type="id"></item>
//  <item name="photoView" type="id"></item>
//  <item name="tagView" type="id"></item>
//  <item name="radioButton" type="id"></item>
//  <item name="spaceView" type="id"></item>
//  <item name="framLayout" type="id"></item>
//  <item name="tag_key_1" type="id"></item>
//  <item name="tag_key_2" type="id"></item>
//
//  <!-- item相关 -->
//  <item name="list_item" type="id"></item>
//  <item name="grid_item" type="id"></item>
//  <item name="grid_item_small" type="id"></item>
//
//
//  <!-- 分界线相关 -->
//  <item name="devider_view" type="id"></item>
//  <item name="devider_vertical_view" type="id"></item>
//  <item name="guide_view" type="id"></item>
//
//  <!-- ViewPager+Fragment相关 -->
//  <item name="tv_guide1" type="id"></item>
//  <item name="tv_guide2" type="id"></item>
//  <item name="tv_guide3" type="id"></item>
//  <item name="tv_guide4" type="id"></item>
//  <item name="tv_guide5" type="id"></item>
//  <item name="tv_guide6" type="id"></item>
//  <item name="iv_cursor" type="id"></item>
//
//  <!-- Button相关 -->
//  <item name="btn_ok" type="id"></item>
//  <item name="btn_cancel" type="id"></item>
//  <item name="tv_ok" type="id"></item>
//  <item name="tv_cancel" type="id"></item>
//
//  <!-- SwipeDeleteListView相关 -->
//  <item name="front_view" type="id"></item>
//  <item name="back_view" type="id"></item>
//
//  <!-- 常用相关 -->
//  <item name="et_search" type="id"></item>
//  <item name="et_content" type="id"></item>
//  <item name="tv_common" type="id"></item>
//  <item name="tv_icon" type="id"></item>
//  <item name="tv_name" type="id"></item>
//  <item name="tv_text" type="id"></item>
//  <item name="tv_type" type="id"></item>
//  <item name="tv_value" type="id"></item>
//  <item name="tv_version" type="id"></item>
//  <item name="tv_content" type="id"></item>
//  <item name="tv_num" type="id"></item>
//  <item name="tv_info" type="id"></item>
//  <item name="tv_price" type="id"></item>
//  <item name="tv_time" type="id"></item>
//  <item name="tv_sum" type="id"></item>
//  <item name="tv_toast" type="id"></item>
//  <item name="tv_current_price" type="id"></item>
//  <item name="tv_original_price" type="id"></item>
//  <item name="tv_address" type="id"></item>
//  <item name="tv_tag" type="id"></item>
//  <item name="tv_descript" type="id"></item>
//  <item name="tv_intro" type="id"></item>
//  <item name="tv_distance" type="id"></item>
//  <item name="tv_navigation" type="id"></item>
//  <item name="iv_icon" type="id"></item>
//  <item name="iv_arrow" type="id"></item>
//  <item name="iv_click" type="id"></item>
//  <item name="iv_audio" type="id"></item>
//  <item name="iv_video" type="id"></item>
//  <item name="iv_content" type="id"></item>
//  <item name="tv_reply" type="id"></item>
//  <item name="iv_speech" type="id"></item>
//  <item name="iv_select" type="id"></item>
//  <item name="iv_edit" type="id"></item>
//  <item name="btn_delete" type="id"></item>
//  <item name="iv_value" type="id"></item>
//  <item name="iv_delete" type="id"></item>
//  <item name="common_image" type="id"></item>
//
//  <!-- audioView相关 -->
//  <item name="audioView" type="id"></item>
//  <item name="audio_bg" type="id"></item>
//  <item name="audio_play" type="id"></item>
//  <item name="audio_time" type="id"></item>
//  <item name="audio_delete" type="id"></item>
//
//  <!-- replyView相关 -->
//  <item name="replyView" type="id"></item>
//  <item name="reply_speak" type="id"></item>
//  <item name="reply_content" type="id"></item>
//  <item name="reply_record" type="id"></item>
//  <item name="reply_photo" type="id"></item>
//  <item name="reply_send" type="id"></item>
//
//  <!-- recordView相关 -->
//  <item name="record_start_view" type="id"></item>
//  <item name="record_volume_view" type="id"></item>
//  <item name="record_time_view" type="id"></item>
//  <item name="record_cancel_view" type="id"></item>
//
//  <!-- recordTextView相关 -->
//  <item name="record_text_view" type="id"></item>
//
//  <!-- commentPraiseNumView相关 -->
//  <item name="commentPraiseNumView" type="id"></item>
//  <item name="tv_comment" type="id"></item>
//  <item name="tv_praise" type="id"></item>
//
//
//  <!-- 地图图层相关TAG -->
//  <item name="isconfirm" type="id"></item>
//  <item name="point" type="id"></item> <!-- 点 -->
//  <item name="line" type="id"></item> <!-- 线 -->
//  <item name="pol" type="id"></item> <!-- 面 -->
//  <item name="track" type="id"></item> <!-- 轨迹 -->
//  <item name="route" type="id"></item> <!-- 路线 -->
//  <item name="overlaydraw" type="id"></item> <!-- 勾绘图层 -->
//  <item name="dltb" type="id"></item> <!-- 图斑图层 -->
//  <item name="tbzj" type="id"></item> <!-- 图斑注记图层 -->
//  <item name="handlayerdraw" type="id"></item>
//  <item name="rl_track_operation_second" type="id"></item><!--   -->
//  <item name="btn_gpssurface_second" type="id"></item><!--   -->

//  <!-- 分界线 -->
  static const int tabwidget_text = 10;
  static const int textview_text = 10;
  static const int textview_text_small = 10;
  static const int textview_text_smaller = 10;
  static const int textview_text_small_middle = 10;
  static const int textview_text_small_middler = 10;
  static const int textview_text_smallest = 10;
  static const int textview_text_middle = 10;
  static const int textview_text_large = 10;
  static const int edittext_text = 10;
  static const int button_text = 10;
  static const int title_text = 10;
}
