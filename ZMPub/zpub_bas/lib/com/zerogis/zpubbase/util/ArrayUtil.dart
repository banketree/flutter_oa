import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：数组处理工具类
 * 作者：郑朝军 on 2019/7/19
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/19
 * 修改备注：
 */
class ArrayUtil
{
  /*
   * String数组转int数组
   *
   * @param strings
   * @return [255,255,255,255]
   */
  static List<int> stringToIntArray(String string)
  {
    List<int> result = <int>[];
    if (CxTextUtil.isEmpty(string))
    {
      return result;
    }
    List<String> list = string.split(BasStringValueConstant.STR_COMMON_COMMA);
    list.forEach((value)
    {
      result.add(int.parse(value));
    });
    return result;
  }


  /*
   * String数组转int数组
   *
   * @param map 举例：[{ret: 0, filename: 0/2/0/289.file},{ret: 0, filename: 0/2/0/200.file}]
   * @param key 举例：filename
   * @return 0/2/0/289.file,0/2/0/200.file
   */
  static String mapKeyToString(List<Map<String, dynamic>> map, String key)
  {
    String result = "";
    map.forEach((value)
    {
      result = result + (value[key].toString()) + BasStringValueConstant.STR_COMMON_COMMA;
    });
    if (!CxTextUtil.isEmpty(result))
    {
      result = result.substring(0, result.length - 1);
    }
    return result;
  }


  /*
   * String数组转int数组
   *
   * @param map 举例：[{ret: 0, filename: 0/2/0/289.file},{ret: 0, filename: 0/2/0/200.file}]
   * @param key 举例：filename
   * @return 0/2/0/289.file,0/2/0/200.file
   */
  static String mapKeyToSubString(List<Map<String, dynamic>> map, String key)
  {
    String result = "";
    String item = "";
    int length = 0;
    map.forEach((value)
    {
      item = (value[key].toString());
      length = item.length;
      result = result + (length > 20 ? item.substring((length * 0.5).toInt(), length) : item);
      result = result + BasStringValueConstant.STR_COMMON_COMMA;
    });
    if (!CxTextUtil.isEmpty(result))
    {
      result = result.substring(0, result.length - 1);
    }
    return result;
  }


  /*
   * 过滤重复的值
   * @param result  [{"type":1,"objectid":"MyTask"},{"type":1,"objectid":"UserTask"},{"type":1,"objectid":"UserTask"}]
   * @param key = objectid
   * @return [{"type":1,"objectid":"MyTask"},{"type":1,"objectid":"UserTask"}]
   */
  static void filterMapKey(List<dynamic> result, String key)
  {
    List<dynamic> list = new List();
    result.sort((dynamic a, dynamic b)
    {
      if (a[key] == b[key])
      {
        list.add(a);
      }
      return 1;
    });
    list.forEach((value)
    {
      result.remove(value);
    });
  }

  /*
   * 根据key匹配对应应到键的值
   * @param list  集合
   * @param key   键
   * @param value 值
   */
  static dynamic filter(List<dynamic> list, String key, Object value)
  {
    dynamic result;
    for (dynamic item in list)
    {
      if (item[key] == value)
      {
        result = item;
        break;
      }
    }
    return result;
  }

  /*
   * 创建一个节点
   * @param list  集合
   * @param key   键
   * @param value 值
   */
  static void createNode(List<dynamic> list, dynamic key, dynamic value)
  {
    list.forEach((item)
    {
      item[key] = value;
    });
  }

  /*
   * 取满足条件的所有元素
   * @param list  集合 [{'prjname':'贵州1'},{'prjname':'贵州1'},{'prjname':'贵州'}]
   * @param exp   条件 {'prjname':'贵州'}
   * @return          [{'prjname':'贵州'}]
   */
  static List<dynamic> gets(List<dynamic> aar, Map<String, dynamic> exp)
  {
    List<dynamic> result = [];
    String key = exp.keys.first;
    aar.forEach((item)
    {
      if(item[key] == exp[key])
      {
        result.add(item);
      }
    });
    return result;
  }

  /*
   * 取满足条件的所有元素
   * @param list         集合 [{'prjname':'贵州1'},{'prjname':'贵州1'},{'prjname':'贵州'}]
   * @param exp          条件 prjname
   * @return             {'prjname': [{'prjname':'贵州1'},{'prjname':'贵州1'},{'prjname':'贵州'}]}
   */
  static Map gets2(List list,String fld)
  {
    Map map = {};
    list.forEach((item)
    {
      if(!CxTextUtil.isEmptyObject(map[item[fld]]))
      {
        List value = map[item[fld]];
        value.add(item);
      }
      else
      {
        List value = [];
        value.add(item);
        map[item[fld]] = value;
      }
    });
    return map;
  }

  /*
   * 取满足条件的所有元素(相反集合)
   * @param list         集合 ['资金','资金1']
   * @param fldsNot      条件 fldsNot ['资金']
   * @return             ['资金1']
   */
  static List filtersNot(List list,List fldsNot)
  {
    List result = [];
    result.addAll(list);
    fldsNot.forEach((item)
    {
      result.remove(item);
    });
    return result;
  }
}
