import 'dart:io';

/*
 * 类描述：设备相关工具类
 * 作者：郑朝军 on 2019/8/30
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/8/30
 * 修改备注：
 */
class FileUtil
{
  /*
   * 判断media表里面的filename是否是图片
   * @param file 文件对象
   */
  static bool containFilePic(String mediaFilename)
  {
    String filename = mediaFilename.split('/').last;
    return filename.contains('BPM') || filename.contains('bpm')
        || filename.contains('GIF') || filename.contains('gif')
        || filename.contains('JPG') || filename.contains('jpg')
        || filename.contains('JPEG') || filename.contains('jpeg')
        || filename.contains('TIF') || filename.contains('tif')
        || filename.contains('PNG') || filename.contains('png');
  }

  /*
   * 是否是图片
   * @param file 文件对象
   */
  static bool containPic(File file)
  {
    String filename = getFileName(file);
    return filename.contains('BPM') || filename.contains('bpm')
        || filename.contains('GIF') || filename.contains('gif')
        || filename.contains('JPG') || filename.contains('jpg')
        || filename.contains('JPEG') || filename.contains('jpeg')
        || filename.contains('TIF') || filename.contains('tif')
        || filename.contains('PNG') || filename.contains('png');
  }

  /*
   * 获取文件名称
   */
  static String getFileName(File file)
  {
    return file.path
        .split('/')
        .last;
  }
}
