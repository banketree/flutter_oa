/*
 * 类描述：树工具类
 * 作者：郑朝军 on 2019/7/4
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/4
 * 修改备注：
 */
class TreeUtil
{
  /*
   * 生成树
   * @param {Array} list 数组数据
   * @param {Object} parentId  父亲节点id
   */
  static List<dynamic> buildTree(List<dynamic> list, int parentId)
  {
    List<dynamic> tree = new List();
    list.forEach((menu)
    {
      int id = menu["id"];
      int glid = menu["glid"];
      if (parentId == glid)
      {
        List<dynamic> menuLists = buildTree(list, id);
        menu['children'] = menuLists;
        tree.add(menu);
      }
    });
    return tree;
  }

  /*
   * 生成树
   * @param {Array} list 数组数据
   * @param {Object} parentId   父亲节点id
   * @param {Object} parentKey  关联key  （比如：layer表中的layer字段 glKey=layer）
   * @param {Object} childKey   父亲key （比如：layer表中的glid字段 glKey=glid）
   */
  static List<dynamic> buildTreeKey(List<dynamic> list, int parentId, dynamic parentKey, dynamic childKey)
  {
    List<dynamic> tree = new List();
    list.forEach((menu)
    {
      int id = menu[parentKey];
      int glid = menu[childKey];
      if (parentId == glid)
      {
        List<dynamic> menuLists = buildTreeKey(list, id, parentKey, childKey);
        menu['children'] = menuLists;
        tree.add(menu);
      }
    });
    return tree;
  }
}
