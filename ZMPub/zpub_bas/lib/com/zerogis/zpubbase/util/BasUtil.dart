import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import '../constant/BasStringValueConstant.dart';

/*
 * 类描述：所有界面调用的公有代码
 * 作者：郑朝军 on 2019/4/27
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/4/27
 * 修改备注：
 */
class BasUtil
{
  static BasUtil mInstance;

  static BasUtil getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new BasUtil();
    }
    return mInstance;
  }

  /*
   * 随机颜色
   */
  Color randomColor()
  {
    return Color.fromARGB(255, Random().nextInt(256) + 0, Random().nextInt(256) + 0, Random().nextInt(256) + 0);
  }

  /*
   * 排序list数据
   * @param list 集合
   * @param key 集合中的key
   */
  void sortDate(List list, dynamic key)
  {
    list.sort((a, b)
    {
      return a[key].compareTo(b[key]);
    });
  }

  /*
   * 处理默认路由名称转换成对象{'plugin':'插件名称','title':'标题','initpara':'初始化插件param参数'}
   * @param defaultRouteName 默认为路由名称次
   */
  dynamic defaultRouteName(String defaultRouteName)
  {
    if(defaultRouteName != BasStringValueConstant.STR_COMMON_SLASH)
    {
      return json.decode(defaultRouteName);
    }
    else
    {
      return defaultRouteName;
    }
  }

  /*
   * 是否是有包含，包括大小写
   * @param imgTypes  【imgTypes=PNG,JPG,JPEG,BMP,TIF,GIF】
   * @param path      【path=0/0/0/90.jpg】
   */
  bool isContains(String imgTypes, String path)
  {
      bool isPng = false;
      List<String> imgs = imgTypes.split(BasStringValueConstant.STR_COMMON_COMMA);
      for (int i = 0; i < imgs.length; i++)
      {
        if (path.contains(imgs[i]) || path.contains(imgs[i].toLowerCase()))
        {
          isPng = true;
          break;
        }
      }
      return isPng;
  }
}