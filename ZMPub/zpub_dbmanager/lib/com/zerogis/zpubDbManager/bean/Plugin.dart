import 'package:json_annotation/json_annotation.dart';

import 'base/BaseModel.dart';

part 'Plugin.g.dart';

/*
 * 类描述：插件表
 * 作者：郑朝军 on 2019/5/29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/29
 * 修改备注：
 */
@JsonSerializable()
class Plugin extends BaseModel
{
  /*
   * 系统号
   */
  String sys;

  /*
   * 分类
   */
  String cata;

  /*
   * 名称
   */
  String name;

  /*
   * 父插件
   */
  String pname;

  /*
   * 插件中文标题
   */
  String titlec;

  /*
   * 插件英文标题
   */
  String titlee;

  /*
   * 是否可关闭
   */
  int closeable;

  /*
   *类文件
   */
  String classurl;

  /*
   * 插件所在目录
   */
  String dir;

  /*
   * 图标文件
   */
  String icon;

  /*
   * 初始化参数
   */
  dynamic initpara;

  /*
   * 多例运行
   */
  int runn;

  /*
   * 插件或者组件[1=对话框,2=TAB页显示,3=组件,4=插件]
   */
  int uitype;

  /*
   * 插件里面配置服务
   */
  String service;

  /*
   * 高度[只有为组件的时候才有用到]
   */
  int height;

  /*
   * 宽度[只有为组件的时候才有用到]
   */
  int width;

  /*
   * 是否模态对话框[默认为0]
   */
  int modal;

  Plugin()
  {}

  String getCata()
  {
    return cata;
  }

  void setCata(String cata)
  {
    this.cata = cata;
  }

  String getClassurl()
  {
    return classurl;
  }

  void setClassurl(String classurl)
  {
    this.classurl = classurl;
  }

  int getCloseable()
  {
    return closeable;
  }

  void setCloseable(int closeable)
  {
    this.closeable = closeable;
  }

  String getDir()
  {
    return dir;
  }

  void setDir(String dir)
  {
    this.dir = dir;
  }

  String getIcon()
  {
    return icon;
  }

  void setIcon(String icon)
  {
    this.icon = icon;
  }

  dynamic getInitpara()
  {
    return initpara;
  }

  void setInitpara(dynamic initpara)
  {
    this.initpara = initpara;
  }

  String getName()
  {
    return name;
  }

  void setName(String name)
  {
    this.name = name;
  }

  int getRunn()
  {
    return runn;
  }

  void setRunn(int runn)
  {
    this.runn = runn;
  }

  String getSys()
  {
    return sys;
  }

  void setSys(String sys)
  {
    this.sys = sys;
  }

  String getTitlec()
  {
    return titlec;
  }

  void setTitlec(String titlec)
  {
    this.titlec = titlec;
  }

  String getTitlee()
  {
    return titlee;
  }

  void setTitlee(String titlee)
  {
    this.titlee = titlee;
  }

  String getPname()
  {
    return pname;
  }

  void setPname(String pname)
  {
    this.pname = pname;
  }

  int getUitype()
  {
    return uitype;
  }

  void setUitype(int uitype)
  {
    this.uitype = uitype;
  }


  String getService()
  {
    return service;
  }

  void setService(String service)
  {
    this.service = service;
  }

  int getHeight()
  {
    return height;
  }

  void setHeight(int height)
  {
    this.height = height;
  }

  int getWidth()
  {
    return width;
  }

  void setWidth(int width)
  {
    this.width = width;
  }

  int getModal()
  {
    return modal;
  }

  void setModal(int modal)
  {
    this.modal = modal;
  }

  /*
   * 反序列化
   */
  factory Plugin.fromJson(Map<String, dynamic> json) => _$PluginFromJson(json);

  /*
   * 序列化
   */
  Map<String, dynamic> toJson()
  => _$PluginToJson(this);
}
