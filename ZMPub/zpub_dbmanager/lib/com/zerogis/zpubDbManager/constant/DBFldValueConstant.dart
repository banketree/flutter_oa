/*
 * 类描述：sys=99等等通用相关的数据库字段值相关
 * 作者：郑朝军 on 2019/5/17
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/17
 * 修改备注：
 */
class DBFldValueConstant
{
  // ----------------常用---------------------------
  static final String SYSROLEOBJ_TYPE_DISPE_FUNC = "func";

  static final String SYSROLEOBJ_TYPE_DISPE_BTN = "btn";

  static final String SYSROLEOBJ_TYPE_DISPE_LAYER = "layer";

  static final String SYSROLEOBJ_TYPE_DISPE_REGION = "region";

  static final String SYSROLEOBJ_TYPE_DISPE_ETTCATVIEW = "ettCatView";

  static final String SYSROLEOBJ_TYPE_DISPE_ETTCATEDIT = "ettCatEdit";

  static final String SYSROLEOBJ_TYPE_DISPE_ORGAN = "organ";

  static final String MEDIA_BUSYTPE_DBVALUE_1 = "1";// 多媒体-bustype字段dbvalue值为1   普通文件
  static final String MEDIA_BUSYTPE_DBVALUE_2 = "2";// 多媒体-bustype字段dbvalue值为2   成果图件
  static final String MEDIA_BUSYTPE_DBVALUE_3 = "3";// 多媒体-bustype字段dbvalue值为3   成果数据库
  static final String MEDIA_BUSYTPE_DBVALUE_4 = "4";// 多媒体-bustype字段dbvalue值为4   成果报告
  static final String MEDIA_BUSYTPE_DBVALUE_5 = "1";// 多媒体-bustype字段dbvalue值为5   视频

  static final String MEDIA_BUSYTPE_DBVALUE_6 = "6";// 多媒体-bustype字段dbvalue值为6   缩略图
  static final String MEDIA_BUSYTPE_DBVALUE_7 = "7";// 多媒体-bustype字段dbvalue值为7   规划图
  static final String MEDIA_BUSYTPE_DBVALUE_8 = "2";// 多媒体-bustype字段dbvalue值为8   现场照片
  static final String MEDIA_BUSYTPE_DBVALUE_9 = "9";// 多媒体-bustype字段dbvalue值为9   全景展示
  static final String MEDIA_BUSYTPE_DBVALUE_10 = "10";// 多媒体-bustype字段dbvalue值为10   项目动态附件图片
}
