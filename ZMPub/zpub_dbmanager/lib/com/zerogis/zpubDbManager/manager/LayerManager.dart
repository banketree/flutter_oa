import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'LayerManagerConstant.dart';

/*
 * 类描述：整个应用程序的layer的管理类：主要针对layer做方法的封装
 * 作者：郑朝军 on 2019/5/10
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/5/10
 * 修改备注：
 */
class LayerManager
    implements LayerManagerConstant
{
  static LayerManager mInstance;

  dynamic layers = [];

  static LayerManager getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new LayerManager();
    }

    return mInstance;
  }

  LayerManager()
  {
    // 取图层数据
    dynamic layers = InitSvrMethod.getLayer();
    if (layers is List)
    {
      layers.forEach((item)
      {
        if (item['visible'] != 0)
        {
          this.layers.add(item);
        }
      });
    }
  }

  /*
   * 根据节点找孩子
   *
   * @param name 名称
   * @param glid 关联ID父节点
   * @return 符合条件的集合
   */
  List queryChild(String name, int glid)
  {
    List result = [];
    layers.forEach((item)
    {
      if (item[name] == glid)
      {
        result.add(item);
      }
    });
    return result;
  }

  /*
   * 查询第一个layer孩子
   */
  dynamic queryFirstChildLayer()
  {
    List result = queryChild('glid', 0);
    result = queryChild('glid', result.first['layer']);
    return result.first;
  }

  /*
   * 查询layer树且添加check=true
   */
  dynamic queryTreeLayerCheck()
  {
    dynamic layers = ObjUtil.assign(this.layers);
    ArrayUtil.createNode(layers, 'check', true);
    return TreeUtil.buildTreeKey(layers, 0, 'layer', 'glid');
  }

  /*
   * 清空本地数据
   */
  static void clear()
  {
    mInstance = null;
  }
}
