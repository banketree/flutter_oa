
import 'package:zpub_bas/com/zerogis/zpubbase/util/BasUtil.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Syscfg.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';

import '../constant/FldConstant.dart';
import 'SysCfgManagerConstant.dart';

/**
 * 类描述：整个应用程序的SysCfg的管理类：主要针对SysCfg做方法的封装
 * 作者：郑朝军 on 2018/8/29
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2018/8/29
 * 修改备注：
 */
class SysCfgManager implements SysCfgManagerConstant 
{
  static SysCfgManager mInstance;

  List<Syscfg> _mSysCfg; // 整个程序的所有syscfg对象

  static SysCfgManager getInstance() 
  {
    if (mInstance == null) 
    {
      mInstance = new SysCfgManager();
    }
    return mInstance;
  }

  SysCfgManager()
  {
    this._mSysCfg = InitSvrMethod.getInitSvrSyscfg();
  }

  List<Syscfg> queryAllSyscfg()
  {
    return _mSysCfg;
  }

  @override
  Syscfg querySysCfg(String keyno)
  {
    List<Syscfg> syscfgList = queryAllSyscfg();
    for (int i = 0, size = syscfgList.length; i < size; i++)
    {
      Syscfg syscfg = syscfgList[i];
      if (syscfg.getKeyno() == keyno)
      {
        return syscfg;
      }
    }
    return null;
  }

  @override
  dynamic querySysCfgValue(String keyno)
  {
    List<Syscfg> syscfgList = queryAllSyscfg();
    for (int i = 0, size = syscfgList.length; i < size; i++)
    {
      Syscfg syscfg = syscfgList[i];
      if (syscfg.getKeyno() == keyno)
      {
        return syscfg.getValue();
      }
    }
    return null;
  }

  void setSysCfg(List<Syscfg> value)
  {
    _mSysCfg = value;
  }

  /*
   * 查询出给定格式，来判断是否是png还是video
   * @param list 查询media表里面返回的数组
   * @param keyno syscfg中的【图片格式-文档格式-视频格式-音频格式】
   */
  List<dynamic> queryForamt(List<dynamic> list, String keyno)
  {
    List<dynamic> result = [];
    BasUtil basUtil = BasUtil.getInstance();
    dynamic imgTypes = querySysCfgValue(keyno);
    list.forEach((item)
    {
      if(basUtil.isContains(imgTypes, item[FldConstant.FLD_FILENAME_]))
      {
        result.add(item);
      }
    });
    return result;
  }
}
