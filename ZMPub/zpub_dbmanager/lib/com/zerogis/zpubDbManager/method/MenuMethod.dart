import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/FldValue.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/constant/DBEntityConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/constant/DBFldConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/constant/DBFldValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldValuesManager.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/manager/FldValuesManagerConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_sqflite/zpub_sqflite.dart';
import 'InitSvrMethod.dart';
import 'UserMethod.dart';

/*
 * 类描述：菜单相关方法：此处可能需要修改，部分代码不适用于A，B，C类型的项目，如果不适用则可不放此处
 * 作者：郑朝军 on 2019/7/22
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/22
 * 修改备注：
 */
class MenuMethod
{
  static MenuMethod mInstance;

  /*
   * 全部功能
   */
  List<dynamic> mFunc;

  /*
   * 功能组
   */
  List<dynamic> mFuncgrp;

  /*
   * 用户角色功能
   */
  List<dynamic> sysroleobj;

  static MenuMethod getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new MenuMethod();
    }
    return mInstance;
  }

  MenuMethod()
  {
    mFunc = InitSvrMethod.getInitSvrMap()["func"];
    mFuncgrp = InitSvrMethod.getInitSvrMap()["funcgrp"];
    dynamic obj = (InitSvrMethod.getInitSvrMap()["access"])["obj"];
    FldValuesManagerConstant fldValuesManagerConstant = FldValuesManager.getInstance();
    FldValue result = fldValuesManagerConstant.queryFldValue(
        "sysroleobj", DBFldConstant.FLD_TYPE, DBFldValueConstant.SYSROLEOBJ_TYPE_DISPE_FUNC);
    sysroleobj = obj[result.getDbvalue()];
    ArrayUtil.filterMapKey(sysroleobj, "objectid");
  }

  /*
   * 查询菜单项
   * retrun [{{id: 1720, sys: 11, name: Task, namee: , namec: 流程中心, icon: icon-flow, disporder: 1, glgrp: , target: menu}:
    * [{id: 5472, sys: 11, namee: , namec: 我的待办箱, disporder: 1, grp: Task, plugin: MyTask, icon: icon-needto}}}}]
   */
  List<Map<dynamic, List<dynamic>>> queryMenu()
  {
    List<Map<dynamic, List<dynamic>>> menu = new List();
    mFuncgrp.forEach((value)
    {
      if (value["target"] == "menu")
      {
        Map<dynamic, List<dynamic>> map = {};
        List<dynamic> list = <dynamic>[];
        mFunc.forEach((funcVal)
        {
          if (funcVal["grp"] == value["name"])
          {
            for(int i =0;i< sysroleobj.length; i ++)
            {
              if (sysroleobj[i]["objectid"] == funcVal["plugin"])
              {
                list.add(funcVal);
                break;
              }
            }
          }
        });

        if (!CxTextUtil.isEmptyList(list))
        {
          map[value] = list;
          menu.add(map);
        }
      }
    });
    return menu;
  }

  /*
   * 查询所有功能
   */
  List<dynamic> queryFunc()
  {
    return mFunc;
  }

  /*
   * 创建自定义菜单项
   */
  void createMenu()
  {
    updateProcsUserID();
    mFuncgrp.forEach((value)
    {
      if (value["target"] == "menu")
      {
        mFunc.forEach((funcVal)
        {
          if (funcVal["grp"] == value["name"])
          {
            sysroleobj.forEach((func)
            {
              if (func["objectid"] == funcVal["plugin"])
              {
                createOrUpdate(funcVal);
              }
            });
          }
        });
      }
    });
  }

  /*
   * 添加或者更新
   */
  void createOrUpdate(dynamic value)
  {
    String exp = DBFldConstant.FLD_NAMEC + " = '" + value[DBFldConstant.FLD_NAMEC] + "' and " +
        BasMapKeyConstant.MAP_KEY_USERID + " = " + UserMethod.getUserId();
    SQLiteDBMethod.getInstance().queryByExp(DBEntityConstant.ENTITY_TABATT_PROCS, exp).then((result)
    {
      if (CxTextUtil.isEmptyList(result))
      {
        String name = "INSERT INTO  procs(sys ,namee ,namec ,disporder ,grp ,prockey ,bill ,procid ,tabname ,proccls ,plugins ,title ,icon, visible, attach, hascomp, proces,userid)";
        String sql =
            name + " VALUES(11, '', '" + value[DBFldConstant.FLD_NAMEC] +
                "',    2, 'grp',      'bustripProcess', 'CL',  'bustripProcess:1:527605', 'hr_bustrip1', 'oa.proc.ProcHrBustrip', '" +
                value[DBFldConstant.FLD_PLUGIN] + "',   '" +
                value[DBFldConstant.FLD_NAMEC] + "',    'assets/images/" + value[DBFldConstant.FLD_ICON] +
                ".png', 0, 0, 1, 1," + UserMethod.getUserId() + ");";
        SQLiteDBMethod.getInstance().creates(sql);
      }
    });
  }

  /*
   * 更新常用功能表的userid
   */
  void updateProcsUserID()
  {
    SQLiteDBMethod.getInstance().update(
        DBEntityConstant.ENTITY_TABATT_PROCS, BasMapKeyConstant.MAP_KEY_USERID + "=" + UserMethod.getUserId(), "grp='hr'");
  }

  /*
   * 清空本地数据
   */
  static void clear()
  {
    mInstance = null;
  }
}
