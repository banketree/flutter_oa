import 'package:flutter/material.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatFldConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatFldValueConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatMajorMinorConstant.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/widget/WidgetStatefulBase.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_track/zpub_track.dart';
import 'package:weui/weui.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

import '../plugin/PatReportPlugin.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'CardMapWidget', '开启巡检卡片', '开启巡检卡片', 1, 3, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'CardMapWidget', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 地图巡检开启卡片组件 <br/>
 * @param {Object 必传}  属性值 举例：{id: 3265, complete: 0, department: 浦东公安二期, descs: , endday: 2020-05-20 23:59:59.0, isgllx: 1, lx: 1,
 * planingid: 883, pjsd: , weekday: 1, username: admin, startday: 2020-05-20 00:00:01.0, userid: 101, pst: 1, areaid: 823}
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CardMapWidget extends WidgetStatefulBase
{
  dynamic mInitPara;

  CardMapWidget({Key key, plugin, this.mInitPara}) : super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new CardMapWidgetState();
  }

  static String toStrings()
  {
    return "CardMapWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class CardMapWidgetState extends WidgetBaseState<CardMapWidget>
{
  /*
   * 巡检管理类
   */
  PatrolManager mPatrolManager;
  /*
   * 巡检记录表:巡检人员提交报告的表
   */
  Map<String,dynamic> mPatPlanrec = {};

  void initData()
  {
    super.initData();
    mPatrolManager = PatrolManager.getInstance();
    query();
  }

  Widget build(BuildContext context)
  {
    return Card(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: <Widget>[
      Column(crossAxisAlignment:CrossAxisAlignment.start,children: <Widget>[
        Padding(child: Text('执行人：${widget.mInitPara[BasMapKeyConstant.MAP_KEY_USERNAME]}',style: BasTextStyleRes.text_color_text1_larger,),padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),),
        Padding(child: Text('所属部门：${widget.mInitPara[BasMapKeyConstant.MAP_KEY_DEPARTMENT]}',style: BasTextStyleRes.text_color_text1_larger),padding: EdgeInsets.symmetric(vertical: 5,horizontal: 10),),
        Padding(child: Text('巡检日期：${widget.mInitPara[PatFldConstant.PAT_PLANS_STARTDAY]}',style: BasTextStyleRes.text_color_text1_larger),padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),),
      ],),
      createPatBtn(),
    ],),) ;
  }

  void onNetWorkSucceed(String method, Object values)
  {
    if (method == 'querys')
    {
    }
    else if(method == 'queryplanrec')
    {
      mPatPlanrec = values;
      initShowModal();
    }
  }

  /*
   * 初始化提示用户是否需要开启巡检
   */
  initShowModal()
  {
    // 如果开启巡检则不提示
    if (mPatrolManager.isTrack())
    {
      return;
    }

    WeDialog.confirm(context)(
        '﻿开启巡检记录回传吗?',
        title: '温馨提示',
        onConfirm: ()
        {
          setState(()
          {
            mPatrolManager.start(widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID]);
          });
        }
    );
  }

  /*
   * 初始化巡检是否关闭或者打开
   */
  initPatStartOrStop()
  {
    if (mPatrolManager.isTrack())
    {
      WeDialog.confirm(context)(
          '当前正在收集轨迹,需要停止上一任务才能开启此任务,是否继续？',
          title: '温馨提示',
          onConfirm: ()
          {
            setState(()
            {
              mPatrolManager.stop();
            });
          }
      );
    }
  }

  /*
   * 查询巡检任务记录表:校验是否已经提交报告
   * @method 方法
   */
  void query()
  {
    // 取类型主类型
    int major = SysMajMinConstant.MAJOR_LINE;
    if(PatFldValueConstant.PAT_PLANS_LX_1 == widget.mInitPara[PatFldConstant.PAT_PLANS_LX])
    {
      major = SysMajMinConstant.MAJOR_LINE;
    }
    else if(PatFldValueConstant.PAT_PLANS_LX_2 == widget.mInitPara[PatFldConstant.PAT_PLANS_LX])
    {
      major = SysMajMinConstant.MAJOR_REG;
    }
    else if(PatFldValueConstant.PAT_PLANS_LX_3 == widget.mInitPara[PatFldConstant.PAT_PLANS_LX])
    {
      major = SysMajMinConstant.MAJOR_PNT;
    }

    Map<String, Object> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: SysMajMinConstant.MAJOR_PAT,
      HttpParamKeyValue.PARAM_KEY_MINOR: PatMajorMinorConstant.MINOR_PAT_PLANREC,
      HttpParamKeyValue.PARAM_KEY_EXP: 'planid=? and major=?',
      HttpParamKeyValue.PARAM_KEY_TYPES: 'i,s',
      HttpParamKeyValue.PARAM_KEY_VALS: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID].toString()+", "+ major.toString(),
    };
    SvrAreaSvrService.querys(this, param: param,method: 'queryplanrec');
  }

  /*
   * 查询当前轨迹是否有效
   */
  queryTrackState()
  {
    Map<String, Object> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: SysMajMinConstant.MAJOR_PAT,
      HttpParamKeyValue.PARAM_KEY_MINOR: PatMajorMinorConstant.MINOR_PAT_LOCATEREC,
      HttpParamKeyValue.PARAM_KEY_EXP: 'planid=?',
      HttpParamKeyValue.PARAM_KEY_TYPES: 'i',
      HttpParamKeyValue.PARAM_KEY_VALS: widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID],
    };

    SvrAreaSvrService.querys(this, param: param);
  }

  /*
   * 创建底部按钮
   */
  Widget createPatBtn()
  {
    bool isConfirm = canConfirm(mPatPlanrec);
    if(isConfirm)
    {
      return CxTextUtil.isEmptyMap(mPatPlanrec)? mEmptyView : IconButton(iconSize: 60, icon: Image.asset('assets/images/pat/ic_report.png',), onPressed: ()
      {
        doClickStartPat();
      });
    }
    else
    {
      return Row(mainAxisAlignment: MainAxisAlignment.end,children: <Widget>[
        IconButton(iconSize: 40, icon: Image.asset(mPatrolManager.isTrack()?'assets/images/pat/ic_timeout.png':'assets/images/pat/ic_goon.png',), onPressed: ()
        {
          setState(()
          {
            doClickStartPat();
          });
        }),
        IconButton(iconSize: 40, icon: Image.asset('assets/images/pat/ic_complete.png',), onPressed: ()
        {
          setState(()
          {
            doClickOk();
          });
        })
      ],);
    }
  }

  /*
   * 开启巡检
   */
  doClickStartPat()
  {
    if (mPatrolManager.isTrack())
    {
      WeDialog.confirm(context)(
          '当前正在收集轨迹，是否停止并提交报告?',
          title: '温馨提示',
          onConfirm: ()
          {
            setState(()
            {
              mPatrolManager.stop();
            });
          }
      );
    }
    else
    {
      mPatrolManager.start(widget.mInitPara[BasMapKeyConstant.MAP_KEY_ID]);
    }
  }

  /*
   * 提交巡检报告
   */
  doClickOk()
  {
    mChildUniversalInitPara.addAll(widget.mInitPara);
    runPluginName(PatReportPlugin.toStrings()).then((result)
    {
      print('更新状态');
    });
  }

  doClickOkOld()
  {
    if (mPatrolManager.isTrack())
    {
      WeDialog.confirm(context)(
          '当前正在收集轨迹，是否停止并提交报告?',
          title: '温馨提示',
          onConfirm: ()
          {
            setState(()
            {
              mPatrolManager.stop();
            });
          }
      );
    }
    else
    {
      queryTrackState();
    }
  }

  /*
   * 判断是否提交过报告[true=提交过报告]
   * @param values ={planres: , cjrq: , cjr: ,  entityid: 0}
   */
  bool canConfirm(values)
  {
    return !CxTextUtil.isEmpty(values[PatFldConstant.PAT_PLANS_CJR]);
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class CardMapWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new CardMapWidget(mInitPara: initPara,);
  }
}
