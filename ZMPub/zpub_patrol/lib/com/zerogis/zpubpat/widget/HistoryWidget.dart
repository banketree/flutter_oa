import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/constant/PatMajorMinorConstant.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatDetailPlugin.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:common_utils/common_utils.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'HistoryWidget', '已办箱', '已办箱', 1, 3, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'HistoryWidget', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 历史组件 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class HistoryWidget extends AttIsMcardListBas
{
  HistoryWidget({Key key, plugin, mInitPara}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new HistoryWidgetState();
  }

  static String toStrings()
  {
    return "HistoryWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class HistoryWidgetState extends AttIsMcardListBasState<HistoryWidget>
{
  /*
   * 是否使用入栈操作
   */
  bool usePushStack()
  {
    return false;
  }

  void initState()
  {
    widget.mInitPara[DBFldConstant.FLD_MAJOR] = PatMajorMinorConstant.MAJOR_PAT;
    widget.mInitPara[DBFldConstant.FLD_MINOR] = PatMajorMinorConstant.MINOR_PAT_PLANS;
    super.initState();
  }

  /*
   * 初始化相关
   */
  void init()
  {
    initData();
  }

  /*
   * 初始化数据相关
   */
  void initData()
  {
    queryInitPara();
    queryInitParaPlugins();
    queryWidgetsList();
  }

  Widget build(BuildContext context)
  {
    Widget widget = createCommonRefresh(mEmptyView);
    return widget;
  }


  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryPage")
    {
      dealQuery(values);
    }
  }

  void query(int page)
  {
    SvrAreaSvrService.queryPage(
        widget.mInitPara[DBFldConstant.FLD_MAJOR],
        widget.mInitPara[DBFldConstant.FLD_MINOR],
        'userid=? and pst=0 and startday<=?',
        'i,t',
        '${UserMethod.getUserId()},${DateUtil.getDateStrByTimeStr(
            DateUtil.getNowDateStr(), format: DateFormat.YEAR_MONTH_DAY)} 12:00:00',
        m_pager,
        this,
        param: {'orderby': 'startday desc'},
        page: page,
        separator: BasStringValueConstant.STR_COMMON_COMMA);
  }

  /*
   * 根据名称启动插件且携带参数
   */
  @override
  Future<T> runPluginParam<T extends Object>(String pluginName, dynamic att)
  {
    mChildUniversalInitPara[DBFldConstant.FLD_MAJOR] = PatMajorMinorConstant.MAJOR_PAT;
    mChildUniversalInitPara[DBFldConstant.FLD_MINOR] = PatMajorMinorConstant.MINOR_PAT_PLANS;
    mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_ATT] = att;
    return super.runPluginName(PatDetailPlugin.toStrings());
  }
}


/*
 * 类描述：模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class HistoryWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new HistoryWidget(mInitPara: initPara, key: initPara[BasMapKeyConstant.MAP_KEY_GLOBAL_KEY],);
  }
}
