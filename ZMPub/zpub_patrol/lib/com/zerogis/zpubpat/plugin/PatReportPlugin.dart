import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import '../constant/PatMajorMinorConstant.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PatReportPlugin', '报告详情', '报告详情', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'PatReportPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 巡检任务完成上报页面(外面传属性过来) <br/>
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PatReportPlugin extends AttFjPlugin
{
  PatReportPlugin({Key key, mInitPara, plugin}) : super(key: key, mInitParam: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PatReportPluginState();
  }

  static String toStrings()
  {
    return "PatReportPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PatReportPluginState extends AttFjPluginState<PatReportPlugin>
{
  void initState()
  {
    widget.mInitParam[DBFldConstant.FLD_MAJOR] = PatMajorMinorConstant.MAJOR_PAT;
    widget.mInitParam[DBFldConstant.FLD_MINOR] = PatMajorMinorConstant.MINOR_PAT_PLANREC;
    super.initState();
  }

  /*
   * 确定按钮
   */
  void doClickOk()
  {
    // 编辑
    updateAtt();
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatReportPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PatReportPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PatReportPlugin();
  }
}
