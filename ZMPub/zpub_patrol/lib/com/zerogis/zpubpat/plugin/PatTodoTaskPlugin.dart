import 'package:flutter/material.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/zpub_ui.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'PatTodoTaskPlugin', '任务箱', '任务箱', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'PatTodoTaskPlugin', '', '', '{"tabs":["待办任务","已办任务"],"widgets":["TodoWidget","HistoryWidget"]}', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 展示待办巡检任务 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PatTodoTaskPlugin extends PluginStatefulBase
{
  dynamic mInitPara;

  PatTodoTaskPlugin({Key key, this.mInitPara, plugin}) :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new PatTodoTaskPluginState();
  }

  static String toStrings()
  {
    return "PatTodoTaskPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PatTodoTaskPluginState extends PluginBaseState<PatTodoTaskPlugin>
{
  final tab = GlobalKey();

  void initState()
  {
    super.initState();
  }

  Widget build(BuildContext context)
  {
    return buildBody(context, TabBarViewWidget(createTabs(), createChildrenKeyWidget(), key: tab,));
  }

  /*
   * 创建tabs组件集合
   */
  List<Widget> createTabs()
  {
    List<Widget> list = <Widget>[];
    mInitParaMap['tabs'].forEach((tab)
    {
      list.add(Tab(text: tab,));
    });
    return list;
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PatTodoTaskPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new PatTodoTaskPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new PatTodoTaskPlugin();
  }
}
