import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/DigitValueConstant.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/method/InitSvrMethod.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatCalendarPlugin.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatDetailPlugin.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatMapPlugin.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatSelectDayPlugin.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatTodoTaskPlugin.dart';
import 'package:zpub_patrol/com/zerogis/zpubpat/plugin/PatTrackPlugin.dart';

import '../plugin/PatReportPlugin.dart';

/*
 * 类描述：巡检模块启动注册模块：可以理解成Android中的AndroidManifest.xml
 * 作者：郑朝军 on 2019/6/1
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/1
 * 修改备注：
 */
class PatPluginApplication
{
  void onCreate()
  {
    List<Plugin> plugin = InitSvrMethod.getInitSvrPlugins();
    plugin.forEach((plugin)
    {
      if (plugin.classurl == PatCalendarPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 巡检任务日历页面
        PluginsFactory.getInstance().add(plugin.name, new PatCalendarPluginService());
      }
      else if (plugin.classurl == PatTrackPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 巡检轨迹-今日轨迹
        PluginsFactory.getInstance().add(plugin.name, new PatTrackPluginService());
      }
      else if (plugin.classurl == PatDetailPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 巡检任务详情页面
        PluginsFactory.getInstance().add(plugin.name, new PatDetailPluginService());
      }
      else if (plugin.classurl == PatMapPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 巡检地图页面
        PluginsFactory.getInstance().add(plugin.name, new PatMapPluginService());
      }
      else if (plugin.classurl == PatSelectDayPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 当天任务
        PluginsFactory.getInstance().add(plugin.name, new PatSelectDayPluginService());
      }
      else if (plugin.classurl == PatTodoTaskPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 展示待办巡检任务
        PluginsFactory.getInstance().add(plugin.name, new PatTodoTaskPluginService());
      }
      else if (plugin.classurl == PatReportPlugin.toStrings() &&
          plugin.getUitype() == DigitValueConstant.APP_DIGIT_VALUE_4)
      { // 巡检任务完成上报页面
        PluginsFactory.getInstance().add(plugin.name, new PatReportPluginService());
      }
    });
  }
}
