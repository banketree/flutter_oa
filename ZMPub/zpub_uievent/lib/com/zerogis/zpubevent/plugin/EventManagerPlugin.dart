import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';
import 'package:zpub_uievent/com/zerogis/zpubevent/constant/EventMajMinConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/plugin/AttFjPlugin.dart';
import 'package:zpub_dbmanager/com/zerogis/zpubDbManager/bean/Plugin.dart';
import 'package:weui/weui.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttConstant.dart';
import 'package:zpub_att/com/zerogis/zpubatt/constant/AttFldValueConstant.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'EventManagerPlugin', '问题管理', '问题管理', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'EventManagerPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 问题管理，多条列表界面，ismcard页 <br/>
 * 需要传入的键：<br/>
 * @param {Object 选传} exp  条件
 * @param {Object 选传} types  类型
 * @param {Object 选传} vals   值
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class EventManagerPlugin extends AttIsMcardListBas
{
  EventManagerPlugin({Key key, mInitPara, plugin}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new EventManagerPluginState();
  }

  static String toStrings()
  {
    return "EventManagerPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class EventManagerPluginState extends AttIsMcardListBasState<EventManagerPlugin>
{
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryPage")
    {
      dealQuery(values);
    }
  }

  void query(int page)
  {
    SvrAreaSvrService.queryPage(
        EventMajMinConstant.MAJOR_PATEVENT,
        EventMajMinConstant.MINOR_PATEVENT,
        widget.mInitPara['exp'] ?? 'cjrid=?',
        widget.mInitPara['types'] ?? 'i',
        widget.mInitPara['vals'] ?? UserMethod.getUserId(),
        m_pager,
        this,
        page: page);
  }

  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    Plugin plugin = new Plugin();
    mList.forEach((item)
    {
      widgets.add(Stack(children: <Widget>[
        GestureDetector(child: AttIsMCardWidget(EventMajMinConstant.MAJOR_PATEVENT,
          EventMajMinConstant.MINOR_PATEVENT, attVals: item, plugin: plugin,), onTap: ()
        {
          runPluginParam(AttFjPlugin.toStrings(), item);
        },),

        Positioned(child: Row(children: <Widget>[
          WeButton(
            '定位',
            theme: WeButtonType.primary,
            size: WeButtonSize.mini,
            onClick: ()
            {
              print('定位===${item}');
            },
          ),
          SizedBox(width: 5,),
          WeButton(
            '删除',
            theme: WeButtonType.primary,
            size: WeButtonSize.mini,
            onClick: ()
            {
              print('定位===${item}');
            },
          )
        ],), bottom: 50, right: 10,)
      ],));
    });
    return widgets;
  }

  /*
   * 根据名称启动插件且携带参数
   */
  @override
  Future<T> runPluginParam<T extends Object>(String pluginName, dynamic att)
  {
    mChildUniversalInitPara[DBFldConstant.FLD_MAJOR] =  EventMajMinConstant.MAJOR_PATEVENT;
    mChildUniversalInitPara[DBFldConstant.FLD_MINOR] =  EventMajMinConstant.MINOR_PATEVENT;
    mChildUniversalInitPara[DBFldConstant.FLD_ID] = att[DBFldConstant.FLD_ID];
    mChildUniversalInitPara[BasMapKeyConstant.MAP_KEY_ATT] = att;
    mChildUniversalInitPara[AttConstant.ATT_STATE] = AttFldValueConstant.ATT_STATE_EDIT_ABLE;
    return super.runPluginName(pluginName);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class EventManagerPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new EventManagerPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new EventManagerPlugin();
  }
}
