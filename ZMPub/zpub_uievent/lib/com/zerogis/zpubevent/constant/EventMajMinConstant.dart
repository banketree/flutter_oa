/*
 * 类描述：事件主子类型定义
 * 作者：郑朝军 on 2019/6/14
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/14
 * 修改备注：
 */
class EventMajMinConstant
{
  /*
   * -------------------------事件级别相关------------------------------------
   */
//  String MAJOR_PAT7 = "7";
//  String MAJOR_PAT61 = "61";
//  String MINOR_PAT61_PNT = "0";
//  String MINOR_PAT61_EVENT = "1";
//  String MINOR_PAT61_EVENT_CONFIRM_MINOR = "24";

  /*
   * 主类型相关
   */
  static final int MAJOR_PATEVENT = 61; // 巡检事件主类型

  /*
   * 子类型相关
   */
  static final int MINOR_PATEVENT = 1;// 巡检事件子类型
}
