import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_att/zpub_att.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'RangeQueryListPlugin', '查询结果', '查询结果', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'RangeQueryListPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);
/*
 * 范围查询结果，多条列表界面，ismcard页 <br/>
 * 需要传入的键：<br/>
 * @param {Object 必传} major 主类型
 * @param {Object 必传} minor 子类型
 * @param {Object 必传} layer 图层
 * @param {Object 必传} xy    坐标
 * @param {Object 必传} r     半径
 * @param {Object 选传} gtype 默认值为1
 * @param {Object 选传} gr     默认值为0
 * @param {Object 选传} regids 默认值为0
 * @param {Object 选传} rangeType 默认值为1
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 *
 */
class RangeQueryListPlugin extends AttIsMcardListBas
{
  RangeQueryListPlugin({Key key, mInitPara, plugin}) :super(key: key, mInitPara: mInitPara, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new RangeQueryListPluginState();
  }

  static String toStrings()
  {
    return "RangeQueryListPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class RangeQueryListPluginState extends AttIsMcardListBasState<RangeQueryListPlugin>
{
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == "queryBybuffer")
    {
      dealQuery(values);
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void query(int page)
  {
    Map<String, Object> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: widget.mInitPara[DBFldConstant.FLD_MAJOR],
      HttpParamKeyValue.PARAM_KEY_MINOR: widget.mInitPara[DBFldConstant.FLD_MINOR],
      HttpParamKeyValue.PARAM_KEY_LAYER: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_LAYER],
      HttpParamKeyValue.PARAM_KEY_XY: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_XY],
      HttpParamKeyValue.PARAM_KEY_R: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_R],

      HttpParamKeyValue.PARAM_KEY_GTYPE: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_GTYPE] ?? 1,
      HttpParamKeyValue.PARAM_KEY_GR: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_GR] ?? 0,
      'regids': widget.mInitPara['regids'] ?? 0,
      'rangeType': widget.mInitPara['rangeType'] ?? 1,
    };
    SvrStatisticService.queryBybuffer(param, m_pager, this, page: page, pageSize: BasSoftwareConstant.MAX_PAGER_SIZE);
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class RangeQueryListPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new RangeQueryListPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new RangeQueryListPlugin();
  }
}
