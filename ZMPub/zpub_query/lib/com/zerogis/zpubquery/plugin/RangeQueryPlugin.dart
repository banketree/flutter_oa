import 'package:flutter/material.dart';
import 'package:zpub_plugin/zpub_plugin.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_query/com/zerogis/zpubquery/plugin/RangeQueryListPlugin.dart';
import 'package:zpub_svr/zpub_svr.dart';
import 'package:zpub_dbmanager/zpub_dbmanager.dart';

// INSERT INTO plugin(sys, cata, name, titlee, titlec, closeable, uitype, runn, dir, classurl, icon, service, initpara, height, width, modal, autorun, autoobj, prereq, container, button, align) VALUES('21', 'common', 'RangeQueryPlugin', '范围查询', '范围查询', 1, 4, 1, 'package:mmdx/com/zerogis/mmdx/scene', 'RangeQueryPlugin', '', '', '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0);

/*
 * 范围查询列表页 <br/>
 * @param {String 必传} xy 坐标[xy=121.78980003576372,30.8725662343195]
 * @param {Object 必传} r 搜索半径
 * @param {Object 选传} gtype  默认值为1
 * @param {Object 选传} gr     默认值为0
 * @param {Object 选传} regids 默认值为0
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class RangeQueryPlugin extends PluginStatefulBase
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> mInitPara;

  RangeQueryPlugin({Key key, this.mInitPara, plugin}) :super(key: key, plugin: plugin);

  State<StatefulWidget> createState()
  {
    return new RangeQueryPluginState();
  }

  static String toStrings()
  {
    return "RangeQueryPlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class RangeQueryPluginState extends PluginBaseState<RangeQueryPlugin>
{
  /*
   * 查询过来的范围列表集合
   */
  List mList = new List();

  void initState()
  {
    super.initState();
    this.query();
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(context, mEmptyView);
    return widget;
  }

  @override
  void onNetWorkSucceed(String method, Object values)
  {
    if (method == 'queryBufferstat')
    {
      mList.addAll(values);
      mEmptyView = SingleChildScrollView(child: Column(children: createBodyWidget()),);
    }
    else
    {
      super.onNetWorkSucceed(method, values);
    }
  }

  void query()
  {
    Map<String, Object> param = {
      HttpParamKeyValue.PARAM_KEY_MAJOR: SysMajMinConstant.MAJOR_PNT,
      HttpParamKeyValue.PARAM_KEY_MINOR: 0,
      HttpParamKeyValue.PARAM_KEY_XY: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_XY],
      HttpParamKeyValue.PARAM_KEY_R: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_R],
      HttpParamKeyValue.PARAM_KEY_GTYPE: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_GTYPE] ?? 1,
      HttpParamKeyValue.PARAM_KEY_GR: widget.mInitPara[HttpParamKeyValue.PARAM_KEY_GR] ?? 0,
      'regids': widget.mInitPara['regids'] ?? 0,
    };
    SvrStatisticService.queryBufferstat(param, this);
  }

  /*
   * 创建内容
   */
  List<Widget> createBodyWidget()
  {
    List<Widget> widgets = new List();
    mList.forEach((item)
    {
      //给item设置参数
      item[HttpParamKeyValue.PARAM_KEY_XY] = widget.mInitPara[HttpParamKeyValue.PARAM_KEY_XY];
      item[HttpParamKeyValue.PARAM_KEY_R] = widget.mInitPara[HttpParamKeyValue.PARAM_KEY_R];

      // 添加布局
      widgets.add(GestureDetector(child: Card(child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(
              width: 5,
              color: BasUtil.getInstance().randomColor(),
            ),
          ),
        ),
        child:
        Padding(child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          Text('类型：${item[BasMapKeyConstant.MAP_KEY_NAME]}'),
          SizedBox(height: 12,),
          Text('数量：${item[BasMapKeyConstant.MAP_KEY_COUNT]}'),
        ],), padding: EdgeInsets.symmetric(vertical: 15, horizontal: 5),),
      ),), onTap: ()
      {
        PluginsFactory.getInstance().get(RangeQueryListPlugin.toStrings()).runPlugin(this, initPara: item);
      },));
    });
    return widgets;
  }
}

/*
 * 类描述：**模块提供的Service其他模块调用本模块对外提供的相关方法
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class RangeQueryPluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(new RangeQueryPlugin(mInitPara: initPara,), state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return new RangeQueryPlugin();
  }
}
