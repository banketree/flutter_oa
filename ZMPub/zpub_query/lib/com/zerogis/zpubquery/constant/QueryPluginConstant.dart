/*
 * 功能：插件名称和插件相关定义的常量相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class QueryPluginConstant
{
  /*
  * 普通属性插件名称
  */
  static const String ATT_FJ_PLUGIN = "AttFjPlugin";
}
