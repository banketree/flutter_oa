import 'LocationManager.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：巡检管理类:负责管理巡检相关的业务逻辑
 * 作者：郑朝军 on 2020/5/20
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2020/5/20
 * 修改备注：
 */
class PatrolManager
{
  static PatrolManager mInstance;

  dynamic plantID;

  LocationManager mLocationManager;

  static PatrolManager getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new PatrolManager();
    }
    return mInstance;
  }

  PatrolManager()
  {
    mLocationManager = LocationManager.getInstance();
  }

  /*
   * 开启小程序进入前后台时均接收位置消息， 需引导用户开启授权。 授权以后， 小程序在运行中或进入后台均可接受位置消息变化。且开启巡检
	 * @param {Object} plantID = 计划ID
   */
  void start(plantID)
  {
    this.plantID = plantID.toString();
    mLocationManager.startListenLocation(isPush: true);
  }

  /*
   * 停止巡检
   */
  void stop()
  {
    plantID = null;
    mLocationManager.destroyLocation();
  }

  /*
   * 判断是否正在巡检
   * [true = 正在收集轨迹]
   */
  bool isTrack()
  {
    return !CxTextUtil.isEmpty(plantID);
  }

  /*
   * 更新开启巡检开关按钮
   */
  updateTrackState()
  {
//    ﻿this.data.patrolOpen = this.patrolOpen();
  }
}
