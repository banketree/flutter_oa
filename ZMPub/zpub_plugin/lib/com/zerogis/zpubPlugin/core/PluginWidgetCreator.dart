import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

/*
 * 功能：组件库,用于创建常用的组件：类似Android中layout文件中common_progressbar.xml相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class PluginWidgetCreator
{
  /*
   * 创建公共可以缩放的图片<br/>
   */
  static Widget createCommonZoomableImage(ImageProvider image)
  {
    return PhotoView(imageProvider: image);
  }

  /*
   * 创建公共可以缩放的图片<br/>
   */
  static GridView createCommonGridViewWrap(List<Widget> children)
  {
    return new GridView.count(
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        children: children);
  }

  /*
   * 创建公共可以缩放的图片<br/>
   */
  static GridView createMiddleGridViewWrap(List<Widget> children)
  {
    return new GridView.count(
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: 3,
        padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        children: children);
  }
}
