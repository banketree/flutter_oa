import 'dart:io';

import 'package:flutter/material.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_plugin/com/zerogis/zpubPlugin/core/PluginWidgetCreator.dart';

/*
 * 图片浏览页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PhotoBrowsePlugin extends FuctionStateFulBase
{
  /*
   * 要显示图片的位置
   */
  int mIndex;

  /*
   * 要显示图片的集合
   */
  List<String> list;

  PhotoBrowsePlugin(this.list,
      this.mIndex, {
        Key key,
      })
      : assert(list != null),
        assert(mIndex != null),
        super(key: key);

  State<StatefulWidget> createState()
  {
    return new PhotoBrowsePluginState();
  }

  static String toStrings()
  {
    return "PhotoBrowsePlugin";
  }
}

/*
 * 页面功能 <br/>
 */
class PhotoBrowsePluginState extends FuctionStateBase<PhotoBrowsePlugin>
{
  PhotoBrowsePluginState()
  {
    mTitle = '相片浏览';
  }

  void initState()
  {
    super.initState();
  }

  Widget build(BuildContext context)
  {
    Widget widget = buildBody(
        context,
        new WillPopScope(
            child: createSwiper(),
            onWillPop: ()
            {
              finish();
            }));
    return widget;
  }

  Widget createSwiper()
  {
    return new WeSwipe(
        autoPlay: false,
        defaultIndex: widget.mIndex,
        cycle: false,
        itemCount: CxTextUtil.isEmptyList(widget.list) ? 1 : widget.list.length,
        itemBuilder: (index)
        {
          return PluginWidgetCreator.createCommonZoomableImage(
              networkOrSdcard(widget.list[index]));
        }
    );
  }

  /*
   * 网络图片或者本地图片
   */
  ImageProvider networkOrSdcard(String path)
  {
    ImageProvider body;
    if (path.contains(BasSoftwareConstant.NETWORK_HTTP) ||
        path.contains(BasSoftwareConstant.NETWORK_HTTPS))
    {
      body = new NetworkImage(path);
    }
    else
    {
      body = new FileImage(new File(path));
    }
    return body;
  }
}


/*
 * 类描述：相册浏览提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class PhotoBrowsePluginService extends InterfaceBaseImpl
{
  @override
  Future<T> runPlugin<T extends Object>(State<StatefulWidget> state,
      {dynamic initPara})
  {
    return StateManager.getInstance().startWidegtState(
        new PhotoBrowsePlugin(
          initPara[BasMapKeyConstant.MAP_KEY_LIST],
          initPara[BasMapKeyConstant.MAP_KEY_POSITION],
        ),
        state);
  }

  @override
  Widget runWidget({dynamic initPara})
  {
    return null;
  }
}
