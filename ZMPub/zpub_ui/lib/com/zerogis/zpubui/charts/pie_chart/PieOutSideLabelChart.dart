import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasMapKeyConstant.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/util/ScreenUtil.dart';
import 'package:zpub_ui/com/zerogis/zpubui/charts/constant/ChartKeyConstant.dart';
import 'package:charts_flutter/flutter.dart';

/*
 * 饼图 <br/>
 * @param {Object 必传} initParam['series'] text  总投资
 * @param {Object 必传} initParam['animate'] bool  动画效果
 * @param {Object 必传} initParam['vertical'] bool  垂直或水平
 * @param {Object 必传} initParam['width'] bool  宽度
 * @param {Object 必传} initParam['height'] bool  高度
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PieOutSideLabelChart extends StatefulWidget {
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  PieOutSideLabelChart({
    this.initParam,
    Key key,
  }) : super(key: key);

  @override
  PieOutSideLabelChartState createState() => PieOutSideLabelChartState();

  /*
   * 例子
   */
  static Widget withSampleData({List<charts.Series<LinearSales, String>> series}) {
    Map<String, dynamic> param = {
      ChartKeyConstant.ANIMATE: true,
      ChartKeyConstant.SERIES: createSampleData(),
      BasMapKeyConstant.MAP_KEY_WIDTH: ScreenUtil.getInstance().getScreenWidth(),
      BasMapKeyConstant.MAP_KEY_HEIGHT: 200.0,
      ChartKeyConstant.VERTICAL: false,
    };
    return PieOutSideLabelChart(
      initParam: param,
    );
  }

  /*
   * 例子数据
   */
  static List<charts.Series<LinearSales, int>> createSampleData() {
    final data = [
      new LinearSales(0, 0),
      new LinearSales(0, 0),
      new LinearSales(0, 0),
      new LinearSales(0, 0),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (sales, __) => sales.color == null?charts.MaterialPalette.green.shadeDefault:sales.color,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
        labelAccessorFn: (LinearSales row, _) => '${row.year}: ${row.sales}',
      )
    ];
  }

  /*
   * 例子数据
   */
  static List<charts.Series<LinearSales, int>> createSampleData2(data) {
    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (sales, __) => sales.color == null?charts.MaterialPalette.green.shadeDefault:sales.color,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
        labelAccessorFn: (LinearSales row, _) => '${row.sales}%',
      )
    ];
  }
}

class PieOutSideLabelChartState<T extends PieOutSideLabelChart> extends State<T> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: charts.PieChart(widget.initParam[ChartKeyConstant.SERIES],
          animate: widget.initParam[ChartKeyConstant.ANIMATE],
          defaultRenderer: new charts.ArcRendererConfig(
              arcRendererDecorators: [new charts.ArcLabelDecorator(labelPosition: charts.ArcLabelPosition.outside)])),
      height: widget.initParam[BasMapKeyConstant.MAP_KEY_HEIGHT] ?? ScreenUtil.getInstance().getScreenWidth() * 0.5,
      width: widget.initParam[BasMapKeyConstant.MAP_KEY_WIDTH] ?? ScreenUtil.getInstance().getScreenWidth(),
    );
  }
}

/// 模型数据
class LinearSales {
  final int year;
  final int sales;
  Color color;

  LinearSales(this.year, this.sales);
}