import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 功能：组件库,用于创建常用的组件：类似Android中layout文件中common_progressbar.xml相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class UICreator
{
  /*
   * 创建Container修饰对象<br/>
   */
  static Decoration createBoxDecoration({dynamic colors})
  {
    Color color = Color(0xfff6ba53);
    if(!CxTextUtil.isEmptyObject(colors))
    {
      if(colors is List)
      {
        color = Color.fromARGB(255, colors[0], colors[1], colors[2]);
      }
      else
      {
        color = Color(colors);
      }
    }
    return BoxDecoration(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(12.0),
        bottomLeft: Radius.circular(12.0),
      ),
      color: const Color(0xfffffbe6),
      border: Border.all(width: 1.0, color: color),
    );
  }

  /*
   * 创建Container修饰对象<br/>
   */
  static Decoration createBoxDecoration2({dynamic colors})
  {
    Color color = Color(0xfff6ba53);
    if(!CxTextUtil.isEmptyObject(colors))
    {
      if(colors is List)
      {
        color = Color.fromARGB(255, colors[0], colors[1], colors[2]);
      }
      else
      {
        color = Color(colors);
      }
    }
    return BoxDecoration(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(12.0),
        bottomRight: Radius.circular(12.0),
      ),
      color: const Color(0xfffffbe6),
      border: Border.all(width: 1.0, color: color),
    );
  }
}
