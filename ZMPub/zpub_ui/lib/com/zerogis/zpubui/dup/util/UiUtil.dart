import 'package:url_launcher/url_launcher.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 类描述：所有界面调用的公有代码
 * 作者：郑朝军 on 2019/4/27
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/4/27
 * 修改备注：
 */
class UiUtil
{
  static UiUtil mInstance;

  static UiUtil getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new UiUtil();
    }
    return mInstance;
  }

  /*
   * url跳转
   * @param appUrl 跳转的url地址
   * @param fuctionStateBase 基类对象
   */
  void launchInBrowser(String appUrl, {FuctionStateBase context})
  async
  {
    if (await canLaunch(appUrl))
    {
      await launch(appUrl);
    }
    else if (context != null)
    {
      context.showToast(appUrl);
    }
  }
}
