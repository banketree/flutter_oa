import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/constant/UiPluginConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/core/UiWidgetCreator.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/font/IconFont.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/util/UiUtil.dart';

import 'UIDocWidgetBas.dart';

/*
 * 视频附件组件,支持本地，网络图片显示，支持查看，编辑 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class UIVideoWidget extends UIDocWidgetBas
{
  UIVideoWidget({
    Key key,
    mMediaList,
    mIsEdit = true,
    mIsLocalEdit = true,
    valueChangedMethod,
    valueDeleteMethod,
    mNullable,
    mValueChangedTap,
  }) : super(
      key: key,
      mMediaList: mMediaList,
      mIsEdit: mIsEdit,
      mIsLocalEdit: mIsLocalEdit,
      valueChangedMethod: valueChangedMethod,
      valueDeleteMethod: valueDeleteMethod,
      mNullable: mNullable,mValueChangedTap: mValueChangedTap,);

  State<StatefulWidget> createState()
  {
    return new UIVideoWidgetState();
  }

  static String toStrings()
  {
    return "UIVideoWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class UIVideoWidgetState extends UIDocWidgetBasState<UIVideoWidget>
{
  Widget build(BuildContext context)
  {
    return UiWidgetCreator.createCommonDuplicate(createGridViewItem(), nullable: widget.mNullable,text: '视频');
  }

  /*
   * 创建选择相册还是相机的一个BottomSheet
   */
  void createShowModalBottomSheet()
  {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context)
        {
          return new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new ListTile(
                leading: UiWidgetCreator.createCommonImage(
                    "", icon: Icons.photo_camera, color: Colors.grey),
                title: new Text("拍视频"),
                onTap: ()
                {
                  doClickPhotoCamera();
                  Navigator.pop(context);
                },
              ),
              new ListTile(
                leading: UiWidgetCreator.createCommonImage(
                    "", icon: Icons.insert_drive_file, color: Colors.grey),
                title: new Text("从手机文档选择"),
                onTap: ()
                {
                  doClickFileExplorer();
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  /*
   * 创建GridView条目内容
   */
  Widget createGridViewItemContent(String path, int position)
  {
    Widget content = new Container(
      margin: EdgeInsets.only(
        right: MarginPaddingHeightConstant.APP_MARGIN_PADDING_2,
      ),
      child: new GestureDetector(
        onLongPress: gridViewItemEdit(path) ? ()
        {
          WeDialog.confirm(context)('确认删除这个视频吗?', title: '提示信息', onConfirm: ()
          {
            setState(()
            {
              widget.mMediaList.removeAt(position);
              widget.valueDeleteMethod(
                  {BasMapKeyConstant.MAP_KEY_PATH: path, BasMapKeyConstant.MAP_KEY_POSITION: position});
            });
          });
        } : null,
        onTap: ()
        {
          doClickPhotoTap(path, position);
        },
        child: networkOrSdcard(path, position),
      ),
    );
    return content;
  }

  /*
   * 图片浏览点击事件
   */
  void doClickPhotoTap(String path, int position)
  {
    if(widget.mValueChangedTap != null)
    {
      widget.mValueChangedTap({
        BasMapKeyConstant.MAP_KEY_URL: path,
        BasMapKeyConstant.MAP_KEY_POSITION: position,
      });
    }
  }

  /*
   * 相机拍视频
   */
  Future<File> doClickPhotoCamera()
  async {
    File file = await ImagePicker.pickVideo(source: ImageSource.camera, maxDuration: Duration(seconds: 60));
    if (file != null)
    {
      setState(()
      {
        widget.valueChangedMethod(file);
        widget.mMediaList.add(file.path);
      });
    }
  }

  /*
   * 从文档中选取文件
   */
  Future<File> doClickFileExplorer()
  async {
    File file = await FilePicker.getFile(type: FileType.video);
    if (file != null)
    {
      setState(()
      {
        widget.valueChangedMethod(file);
        widget.mMediaList.add(file.path);
      });
    }
  }

  /*
   * 是否可以编辑条目
   * @param path 图片的路径
   * retrun【true=只是本地文件可编辑】 false本地文件和远程文件都可以编辑
   */
  bool gridViewItemEdit(String path)
  {
    bool edit = true;
    if (!widget.mIsEdit && widget.mIsLocalEdit)
    {
      if (path.contains(BasSoftwareConstant.NETWORK_HTTP) ||
          path.contains(BasSoftwareConstant.NETWORK_HTTPS))
      {
        edit = false;
        return edit;
      }
      else
      {
        return edit;
      }
    }
    return edit;
  }
}

/*
 * 类描述：附件组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class UIVideoWidgetService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new UIVideoWidget();
  }

  @override
  Widget runWidgetParam({List<String> list, bool isEdit = true, valueChangedMethod})
  {
    return new UIVideoWidget(mMediaList: list, mIsEdit: isEdit);
  }
}
