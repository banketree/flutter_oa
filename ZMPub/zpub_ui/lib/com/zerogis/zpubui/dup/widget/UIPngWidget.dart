import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/constant/UiPluginConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/core/UiWidgetCreator.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/font/IconFont.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/util/UiUtil.dart';

import 'UIDocWidgetBas.dart';

/*
 * 图片附件组件,支持本地，网络图片显示，支持查看，编辑 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class UIPngWidget extends UIDocWidgetBas {
  UIPngWidget({
    Key key,
    mMediaList,
    mIsEdit = true,
    mIsLocalEdit = true,
    valueChangedMethod,
    valueDeleteMethod,
    mNullable,
  }) : super(
            key: key,
            mMediaList: mMediaList,
            mIsEdit: mIsEdit,
            mIsLocalEdit: mIsLocalEdit,
            valueChangedMethod: valueChangedMethod,
            valueDeleteMethod: valueDeleteMethod,
            mNullable: mNullable);

  State<StatefulWidget> createState() {
    return new UIPngWidgetState();
  }

  static String toStrings() {
    return "UIPngWidget";
  }
}

/*
 * 组件功能 <br/>
 */
class UIPngWidgetState extends UIDocWidgetBasState<UIPngWidget> {

  Widget build(BuildContext context)
  {
    return UiWidgetCreator.createCommonDuplicate(createGridViewItem(), nullable: widget.mNullable, text: '图片');
  }

  /*
   * 创建选择相册还是相机的一个BottomSheet
   */
  void createShowModalBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new ListTile(
                leading: UiWidgetCreator.createCommonImage("", icon: Icons.photo_camera, color: Colors.grey),
                title: new Text("拍照"),
                onTap: () {
                  doClickPhotoCamera();
                  Navigator.pop(context);
                },
              ),
              new ListTile(
                leading: UiWidgetCreator.createCommonImage("", icon: Icons.photo_library, color: Colors.grey),
                title: new Text("从手机相册选择"),
                onTap: () {
                  doClickPhotoGallery();
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  /*
   * 创建GridView条目内容
   */
  Widget createGridViewItemContent(String path, int position)
  {
    Widget content = new Container(
      margin: EdgeInsets.only(
        right: MarginPaddingHeightConstant.APP_MARGIN_PADDING_2,
      ),
      child: new GestureDetector(
        onLongPress: gridViewItemEdit(path) ? ()
        {
          WeDialog.confirm(context)('确认删除这个图片吗?', title: '提示信息', onConfirm: ()
          {
            setState(()
            {
              widget.mMediaList.removeAt(position);
              widget.valueDeleteMethod(
                  {BasMapKeyConstant.MAP_KEY_PATH: path, BasMapKeyConstant.MAP_KEY_POSITION: position});
            });
          });
        } : null,
        onTap: ()
        {
          doClickPhotoTap(path, position);
        },
        child: networkOrSdcard(path, position),
      ),
    );
    return content;
  }


  /*
   * 是否可以编辑条目
   * @param path 图片的路径
   * retrun【true=只是本地文件可编辑】 false本地文件和远程文件都可以编辑
   */
  bool gridViewItemEdit(String path)
  {
    bool edit = true;
    if (!widget.mIsEdit && widget.mIsLocalEdit)
    {
      if (path.contains(BasSoftwareConstant.NETWORK_HTTP) ||
          path.contains(BasSoftwareConstant.NETWORK_HTTPS))
      {
        edit = false;
        return edit;
      }
      else
      {
        return edit;
      }
    }
    return edit;
  }
}

/*
 * 类描述：附件组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class UIPngWidgetService extends InterfaceBaseImpl {
  @override
  Widget runWidget({dynamic initPara}) {
    return new UIPngWidget();
  }

  @override
  Widget runWidgetParam({List<String> list, bool isEdit = true, valueChangedMethod}) {
    return new UIPngWidget(mMediaList: list, mIsEdit: isEdit);
  }
}
