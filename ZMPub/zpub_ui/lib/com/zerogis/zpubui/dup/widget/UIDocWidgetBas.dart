import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/BasStringValueConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/constant/UiPluginConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/core/UiWidgetCreator.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/font/IconFont.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/util/UiUtil.dart';

/*
 * 附件组件,支持本地，网络图片显示，支持查看，编辑 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class UIDocWidgetBas extends FuctionStateFulBase
{
  /*
   * 所有图片的集合
   */
  List<String> mMediaList;

  /*
   * 是否为编辑【true=编辑状态】 
   */
  final bool mIsEdit;

  /*
   * 是否只是本地文件可编辑【true=只是本地文件可编辑】 false本地文件和远程文件都可以编辑
   */
  final bool mIsLocalEdit;

  /*
   * 是否为可空[1=不可空，必须要上传附件]
   */
  final int mNullable;

  /*
   * 点击图片回调事件
   */
  ValueChanged<dynamic> valueChangedMethod;

  /*
   * 点击删除图片回调事件
   */
  ValueChanged<dynamic> valueDeleteMethod;

  /*
   * 点击附件回调事件，跳转到另一个页面查看
   */
  ValueChanged<dynamic> mValueChangedTap;

  UIDocWidgetBas({Key key,
    @required this.mMediaList,
    @required this.mIsEdit = true,
    @required this.mIsLocalEdit = true,
    @required this.valueChangedMethod,
    this.valueDeleteMethod,
    this.mValueChangedTap,
    this.mNullable,})
      : super(key: key)
  {
    if (mMediaList == null)
    {
      this.mMediaList = new List();
    }
  }

  State<StatefulWidget> createState()
  {
    return new UIDocWidgetBasState();
  }

  static String toStrings()
  {
    return "UIDocWidgetBas";
  }
}

/*
 * 组件功能 <br/>
 */
class UIDocWidgetBasState<T extends UIDocWidgetBas> extends FuctionStateBase<T>
{
  /*
   * 当前组件是否入栈
   */
  bool usePushStack()
  {
    return false;
  }

  UIDocWidgetBasState()
  {}

  void initState()
  {
    super.initState();
  }

  Widget build(BuildContext context)
  {
    return UiWidgetCreator.createCommonDuplicate(createGridViewItem(), nullable: widget.mNullable);
  }

  /*
   * 创建GridView条目
   */
  List<Widget> createGridViewItem()
  {
    List<Widget> listWidget = <Widget>[];
    createDefalutAddPhotoWidget(listWidget);
    for (int i = 0; i < widget.mMediaList.length; i++)
    {
      listWidget.add(createGridViewItemContent(widget.mMediaList[i], i));
    }
    return listWidget;
  }

  /*
   * 创建默认的添加照片的组件
   */
  void createDefalutAddPhotoWidget(List<Widget> list)
  {
    Widget content = new Container(
      margin: EdgeInsets.only(
        right: MarginPaddingHeightConstant.APP_MARGIN_PADDING_4,
      ),
      child: new GestureDetector(
        onTap: ()
        {
          doClickAddPhotoTap();
        },
        child: UiWidgetCreator.createCommonImage(
            "", icon: IconFont.icon_addto, color: Colors.grey),
      ),
    );
    list.add(content);
  }

  /*
   * 创建GridView条目内容
   */
  Widget createGridViewItemContent(String path, int position)
  {
    Widget content = new Container(
      margin: EdgeInsets.only(
        right: MarginPaddingHeightConstant.APP_MARGIN_PADDING_2,
      ),
      child: new GestureDetector(
        onLongPress: gridViewItemEdit(path) ? ()
        {
          WeDialog.confirm(context)('确认删除?', title: '提示信息', onConfirm: ()
          {
            setState(()
            {
              widget.mMediaList.removeAt(position);
              widget.valueDeleteMethod(
                  {BasMapKeyConstant.MAP_KEY_PATH: path, BasMapKeyConstant.MAP_KEY_POSITION: position});
            });
          });
        } : null,
        onTap: ()
        {
          doClickPhotoTap(path, position);
        },
        child: networkOrSdcard(path, position),
      ),
    );
    return content;
  }

  /*
   * 创建选择相册还是相机的一个BottomSheet
   */
  void createShowModalBottomSheet()
  {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context)
        {
          return new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new ListTile(
                leading: UiWidgetCreator.createCommonImage(
                    "", icon: Icons.photo_camera, color: Colors.grey),
                title: new Text("拍照"),
                onTap: ()
                {
                  doClickPhotoCamera();
                  Navigator.pop(context);
                },
              ),
              new ListTile(
                leading: UiWidgetCreator.createCommonImage(
                    "", icon: Icons.photo_library, color: Colors.grey),
                title: new Text("从手机相册选择"),
                onTap: ()
                {
                  doClickPhotoGallery();
                  Navigator.pop(context);
                },
              ),
              new ListTile(
                leading: UiWidgetCreator.createCommonImage(
                    "", icon: Icons.insert_drive_file, color: Colors.grey),
                title: new Text("从手机文档选择"),
                onTap: ()
                {
                  doClickFileExplorer();
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  /*
   * 网络图片或者本地图片
   */
  Widget networkOrSdcard(String path, int position)
  {
    Widget body;
    if (path.contains(BasSoftwareConstant.NETWORK_HTTP) ||
        path.contains(BasSoftwareConstant.NETWORK_HTTPS))
    {
      List<String> url = path.split(BasStringValueConstant.STR_COMMON_COMMA);
      if (isPic(path))
      {
        body = UiWidgetCreator.createCommonImage(url[0], third: 2);
      }
      else
      {
        body = Stack(children: <Widget>[
          Icon(Icons.videocam, size: 80, color: Colors.black87,),
          Align(alignment: Alignment.bottomCenter, child: Text(url[1]
              .split(BasStringValueConstant.STR_COMMON_SLASH)
              .last, maxLines: 1,)),
        ],);
      }
    }
    else if (FileUtil.containPic(new File(path)))
    {
      body = Image.file(new File(path));
    }
    else
    {
      body = Stack(children: <Widget>[
        Icon(Icons.insert_drive_file, size: 80, color: Colors.greenAccent,),
        Align(alignment: Alignment.bottomCenter, child: Text(FileUtil.getFileName(File(path)), maxLines: 1,)),
      ],);
    }
    return body;
  }

  /*
   * 图片浏览点击事件 
   */
  void doClickPhotoTap(String path, int position)
  {
    if (path.contains(BasSoftwareConstant.NETWORK_HTTP) ||
        path.contains(BasSoftwareConstant.NETWORK_HTTPS))
    { // 判断是否是网络文件或者本地文件
      List<String> url = path.split(BasStringValueConstant.STR_COMMON_COMMA);
      if (isPic(path))
      { // 图片
        Map map = getPhotoBrowseData(path);
        PluginsFactory.getInstance().get(UiPluginConstant.PHOTO_BROWSE).runPlugin(
            this, initPara: map);
      }
      else
      { // 远程服务器文件下载
        UiUtil.getInstance().launchInBrowser(url[0]);
      }
    }
    else if (FileUtil.containPic(new File(path)))
    { // 本地图片文件
      Map map = getPhotoBrowseData(path);
      PluginsFactory.getInstance().get(UiPluginConstant.PHOTO_BROWSE).runPlugin(
          this, initPara: map);
    }
    else
    { // 本地word文件
      showToast('请选择其他工具打开！');
    }
  }

  /*
   * 图片添加事件
   */
  void doClickAddPhotoTap()
  {
    if (widget.mIsEdit)
    {
      createShowModalBottomSheet();
    }
  }

  /*
   * 从相册中选取照片
   */
  Future<File> doClickPhotoGallery()
  async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 80);
//    String path =
//        await ImageJpeg.encodeJpeg(file.path, file.path, 100, 5000, 5000);
    if (file != null)
    {
      setState(()
      {
        widget.valueChangedMethod(file);
        widget.mMediaList.add(file.path);
      });
    }
  }

  /*
   * 从文档中选取文件
   */
  Future<File> doClickFileExplorer()
  async {
    File file = await FilePicker.getFile();
    if (file != null)
    {
      setState(()
      {
        widget.valueChangedMethod(file);
        widget.mMediaList.add(file.path);
      });
    }
  }

  /*
   * 相机拍照选择照片
   */
  Future<File> doClickPhotoCamera()
  async {
    File file = await ImagePicker.pickImage(source: ImageSource.camera, imageQuality: 80);
//    String path =
//        await ImageJpeg.encodeJpeg(file.path, file.path, 100, 5000, 5000);
    if (file != null)
    {
      setState(()
      {
        widget.valueChangedMethod(file);
        widget.mMediaList.add(file.path);
      });
    }
  }

  /*
   * 是否可以编辑条目
   * @param path 图片的路径
   * retrun【true=只是本地文件可编辑】 false本地文件和远程文件都可以编辑
   */
  bool gridViewItemEdit(String path)
  {
    bool edit = true;
    if (widget.mIsEdit && widget.mIsLocalEdit)
    {
      if (path.contains(BasSoftwareConstant.NETWORK_HTTP) ||
          path.contains(BasSoftwareConstant.NETWORK_HTTPS))
      {
        edit = false;
        return edit;
      }
      else
      {
        return edit;
      }
    }
    return edit;
  }

  /*
   * 判断是否是pic文件
   */
  bool isPic(String path)
  {
    List<String> url = path.split(BasStringValueConstant.STR_COMMON_COMMA);
    return FileUtil.containFilePic(url[1]);
  }

  /*
   * 获取图片路径
   * @param path 图片路径
   */

  /*
   * 获取图片路径
   * @param path 图片路径
   */
  Map getPhotoBrowseData(String path)
  {
    Map map = {};
    // 取图片
    List<String> list = new List();
    widget.mMediaList.forEach((item)
    {
      if (item.contains(BasSoftwareConstant.NETWORK_HTTP) ||
          item.contains(BasSoftwareConstant.NETWORK_HTTPS))
      {
        if (isPic(item))
        {
          list.add(item.split(BasStringValueConstant.STR_COMMON_COMMA)[0]);
        }
      }
      else if (FileUtil.containPic(new File(item)))
      {
        list.add(item);
      }
    });

    // 取被点击的位置
    int pos = 0;
    for (int i = 0; i < list.length; i ++)
    {
      String item = list[i];
      if (path.contains(item))
      {
        pos = i;
      }
    }
    map[BasMapKeyConstant.MAP_KEY_LIST] = list;
    map[BasMapKeyConstant.MAP_KEY_POSITION] = pos;
    return map;
  }
}


/*
 * 类描述：附件组件提供的Service
 * 作者：郑朝军 on 2019/6/6
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/6/6
 * 修改备注：
 */
class UIDocWidgetBasService extends InterfaceBaseImpl
{
  @override
  Widget runWidget({dynamic initPara})
  {
    return new UIDocWidgetBas();
  }

  @override
  Widget runWidgetParam({List<String> list, bool isEdit = true, valueChangedMethod})
  {
    return new UIDocWidgetBas(
        mMediaList: list, mIsEdit: isEdit);
  }
}

