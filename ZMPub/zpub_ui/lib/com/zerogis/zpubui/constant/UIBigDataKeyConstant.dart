/*
 * 功能：大数据小组件相关定义的常量相关
 * 需要传入的键：
 * 传入的值类型：
 * 传入的值含义：
 * 是否必传 ：
 * 作者：郑朝军 on 2019/4/7 23:23
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 */
class UIBigDataKeyConstant
{
  /*
   * ----------------------常用-----------------------------------------------------
   */
  static const String HEAD = "head"; // 头部
  static const String FOOT = "foot"; // 底部
  static const String CONTENT = "content"; // 内容
  static const String COLOR = "color"; // 颜色
  static const String LEFT_COLOR = "leftColor"; // 颜色
  static const String DW = "dw"; // 单位
  static const String EXPEND_INDEX = "expandIndex"; // 展开位置
  static const String FONT_SIZE = "fontSize"; // 字体大小
  static const String TEXT = "text"; // 文本
  static const String TEXT_COLOR = "textColor"; // 文本
  static const String COOLAPSE = "collapse"; // 折叠

  static const String RATIO = "ratio"; // 比率
}
