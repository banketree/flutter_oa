import 'package:flutter/material.dart';
import 'package:weui/weui.dart';

///支持搜索功能
class ImageText extends StatefulWidget
{
  final double mPadding;
  final double mWidth;
  final String mImage;
  final String mTitle;
  final Color mTextColor;
  final double mTextSize;

  //switch相关
  // 颜色
  final Color switchColor;

  // 大小
  final double switchSize;

  // 选中状态
  bool switchChecked;

  // 禁用状态
  bool switchDisabled;

  // 禁用状态
  bool switchOffstage;

  // 点击变化的回调函
  final ValueChanged<dynamic> onChange;

  ImageText(this.mImage, this.mTitle,
      {this.mWidth = double.infinity, this.mPadding = 0, this.mTextColor = Colors
          .black, this.mTextSize = 14, this.switchColor = Colors.blue,
        this.switchSize = 20.0,
        this.switchChecked = true,
        this.switchDisabled = false,
        this.switchOffstage = false,
        this.onChange, Key key,}) :super(key: key);

  @override
  State<StatefulWidget> createState()
  {
    return ImageTextState();
  }
}

class ImageTextState extends State<ImageText>
{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      width: widget.mWidth,
      alignment: AlignmentDirectional.bottomCenter,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(padding: EdgeInsets.fromLTRB(widget.mPadding, 0, 10, 0),
                    child: _buildImage()),
                _buildText(),
                _buildSwitch(),
              ],
            ),
          ),
          Container(color: Colors.black, height: 0.5,),
        ],
      ),
    );
  }

  Widget _buildText()
  {
    return Expanded(child: Text(
      widget.mTitle, style: TextStyle(color: widget.mTextColor, fontSize: widget.mTextSize,),));
  }

  Widget _buildImage()
  {
    return Image.asset(widget.mImage, width: 20, height: 20,);
  }

  Widget _buildSwitch()
  {
    return Offstage(child: WeSwitch(
      color: widget.switchColor,
      size: widget.switchSize,
      checked: widget.switchChecked,
      disabled: widget.switchDisabled,
      onChange: (value)
      {
        setState(()
        {
          widget.switchChecked = !widget.switchChecked;
        });

        // 回调
        if (widget.onChange != null)
        {
          widget.onChange(widget.switchChecked);
        }
      },), offstage: widget.switchOffstage,);
  }
}
