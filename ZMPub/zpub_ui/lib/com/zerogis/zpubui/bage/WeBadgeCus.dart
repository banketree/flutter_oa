import 'package:flutter/material.dart';
import 'package:weui/weui.dart';

class WeBadgeCus extends StatefulWidget
{
  final child;
  final Color defalutColor, chooseColor, dynamicColor;
  final TextStyle textStyle;
  final Border border;
  final bool hollow;
  ValueChanged<dynamic> onTap;

  WeBadgeCus({
    this.child,
    this.defalutColor,
    this.chooseColor,
    this.dynamicColor,
    this.textStyle,
    this.border,
    this.hollow = false,
    this.onTap
  });

  @override
  WeBadgeCusState createState()
  => WeBadgeCusState();
}

class WeBadgeCusState extends State<WeBadgeCus>
{
  Color color;

  @override
  void didChangeDependencies()
  {
    super.didChangeDependencies();
    final WeTheme theme = WeUi.getTheme(context);
    if (widget.dynamicColor == null)
    {
      color = widget.defalutColor == null ? theme.warnColor : widget.defalutColor;
    }
    else
    {
      color = widget.dynamicColor;
    }
  }

  @override
  Widget build(BuildContext context)
  {
    Color textColor;
    Color boxColor;
    Border border;

    if (widget.hollow)
    {
      textColor = color;
      boxColor = Colors.transparent;
      border = Border.all(width: 1, color: color);
    }
    else
    {
      textColor = Colors.white;
      boxColor = color;
      border = null;
    }

    return new GestureDetector(child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          DecoratedBox(
              decoration: BoxDecoration(
                  color: boxColor,
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  border: widget.border == null ? border : widget.border
              ),
              child: Padding(
                  padding: EdgeInsets.only(top: 2.0, right: 7.0, bottom: 2.0, left: 7.0),
                  child: DefaultTextStyle(
                      style: TextStyle(
                          fontSize: 13.0,
                          color: textColor
                      ),
                      child: Text(widget.child, style: widget.textStyle)
                  )
              )
          )
        ]
    ), onTap: ()
    {
      setState(()
      {
        if (color == null)
        {
          color = widget.chooseColor ?? Colors.red;
        }
        else
        {
          if (color == Colors.red)
          {
            color = widget.defalutColor;
          }
          else
          {
            color = widget.chooseColor ?? Colors.red;
          }
        }

        if (widget.onTap != null)
        {
          widget.onTap(color);
        }
      });
    },);
  }
}
