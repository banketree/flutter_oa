import 'package:flutter/material.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import './multi_manager/flick_multi_manager.dart';
import './multi_manager/flick_multi_player.dart';
import 'package:zpub_bas/zpub_bas.dart';

/*
 * 列表播放-支持网络播放和本地播放 <br/>
 * @param {Object 必传} initParam['url'] url        播放地址
 * @param {Object 必传} initParam['height'] height  高度-默认高度是300
 * @param {Object 必传} initParam['width']  width   高度-默认高度是300
 *
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class ListPlayer2 extends StatefulWidget {
  /*
   * 初始化参数
   */
  Map<dynamic, dynamic> initParam;

  ListPlayer2({this.initParam, Key key}) : super(key: key);

  @override
  _ListPlayer2State createState() => _ListPlayer2State();
}

class _ListPlayer2State extends State<ListPlayer2> {
  FlickMultiManager flickMultiManager;

  @override
  void initState() {
    super.initState();
    flickMultiManager = FlickMultiManager();
  }

  @override
  Widget build(BuildContext context) {
    return VisibilityDetector(
      key: ObjectKey(flickMultiManager),
      onVisibilityChanged: (visibility) {
        if (visibility.visibleFraction == 0 && this.mounted) {
          flickMultiManager.pause();
        }
      },
      child: Container(
          height: widget.initParam[BasMapKeyConstant.MAP_KEY_HEIGHT] ?? 300,
          width: widget.initParam[BasMapKeyConstant.MAP_KEY_WIDTH] ?? 300,
          margin: EdgeInsets.all(2),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: FlickMultiPlayer(
              url: widget.initParam[BasMapKeyConstant.MAP_KEY_URL],
              flickMultiManager: flickMultiManager,
              image: '',
            ),
          )),
    );
  }
}
