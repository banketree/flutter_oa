import 'package:flutter/material.dart';
import 'package:weui/weui.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_ui/com/zerogis/zpubui/constant/UIBigDataKeyConstant.dart';

/*
 * 总投资：2080万元 <br/>
 * @param {Object 必传} initParam['head'] text  总投资
 * @param {Object 选传} initParam['head'] fontSize 大小
 * @param {Object 选传} initParam['head'] color    颜色 [默认=0xfff69c1b]
 *
 * @param {Object 必传} initParam['foot'] text   2080
 * @param {Object 必传} initParam['foot'] fontSize 大小
 * @param {Object 选传} initParam['foot'] color    颜色 [默认=0xfff69c1b]
 *
 * @param {Object 必传} initParam['foot']['dw'] text   万元
 * @param {Object 选传} initParam['foot']['dw'] fontSize  大小
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CardData extends StatefulWidget
{
  /*
   * 初始化参数
   */
  Map<String, dynamic> initParam;

  CardData({this.initParam,Key key,}) :super(key: key);

  @override
  CardDataState createState()
  => CardDataState();
}

class CardDataState extends State<CardData>
{
  @override
  Widget build(BuildContext context)
  {
    return Card(child: Padding(child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      Text(
        '${widget.initParam[UIBigDataKeyConstant.HEAD][UIBigDataKeyConstant.TEXT]}',
        style: TextStyle(
          fontFamily: 'SourceHanSansCN-Medium',
          fontSize: widget.initParam[UIBigDataKeyConstant.HEAD][UIBigDataKeyConstant.FONT_SIZE]??BasSizeRes.textview_text_small_middle,
          color: Color(widget.initParam[UIBigDataKeyConstant.HEAD][UIBigDataKeyConstant.COLOR]??0xfff69c1b),
        ),
        textAlign: TextAlign.center,
      ),

      SizedBox(height: 5,),

      Text.rich(
        TextSpan(
          style: TextStyle(
            fontFamily: 'SourceHanSansCN-Bold',
            fontSize: widget.initParam[UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.FONT_SIZE]??BasSizeRes.textview_text_small_middle,
            color: Color(widget.initParam[UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.COLOR]??0xfff69c1b),
          ),
          children: [
            TextSpan(
              text: '${widget.initParam[UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.TEXT]}',
            ),
            TextSpan(
              text: '${widget.initParam[UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.DW][UIBigDataKeyConstant.TEXT]}',
              style: TextStyle(
                fontFamily: 'SourceHanSansCN-Regular',
                fontSize: widget.initParam[UIBigDataKeyConstant.FOOT][UIBigDataKeyConstant.DW][UIBigDataKeyConstant.FONT_SIZE]??BasSizeRes.textview_text_small,
              ),
            ),
          ],
        ),
        textAlign: TextAlign.left,
      )
    ],), padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),),);
  }
}
