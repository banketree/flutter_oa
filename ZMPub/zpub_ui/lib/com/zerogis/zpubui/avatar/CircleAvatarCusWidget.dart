import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:zpub_bas/zpub_bas.dart';
import 'package:zpub_bas/com/zerogis/zpubbase/constant/MarginPaddingHeightConstant.dart';
import 'package:zpub_ui/com/zerogis/zpubui/dup/widget/DuplicateWidget.dart';

/*
 * 类描述：头像组件
 * 作者：郑朝军 on 2019/7/24
 * 邮箱：1250393285@qq.com
 * 公司：武汉智博创享科技有限公司
 * 修改人：郑朝军 on 2019/7/24
 * 修改备注：
 */
class CircleAvatarCusWidget extends DuplicateWidget
{
  /*
   * 图片路径
   */
  String mImgUrl;

  CircleAvatarCusWidget({Key key, this.mImgUrl}) : super(key: key);

  State<StatefulWidget> createState()
  {
    return new CircleAvatarCusWidgetState();
  }
}

/*
 * 组件功能 <br/>
 */
class CircleAvatarCusWidgetState extends DuplicateWidgetState<CircleAvatarCusWidget>
{
  /*
   * 图片提供类
   */
  ImageProvider mImageProvider;

  void initState()
  {
    super.initState();
    mImageProvider =
    widget.mImgUrl.contains(BasSoftwareConstant.SOFTWARE_ASSETS) ? new AssetImage(widget.mImgUrl) : new NetworkImage(
        widget.mImgUrl);
  }

  Widget build(BuildContext context)
  {
    return new GestureDetector(child: new CircleAvatar(
        radius: MarginPaddingHeightConstant.APP_MARGIN_PADDING_50,
        backgroundImage: mImageProvider), onTap: ()
    {
      createShowModalBottomSheet();
    },);
  }


  /*
   * 从相册中选取照片
   */
  Future<File> doClickPhotoGallery()
  async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 80);
    if (file != null)
    {
      setState(()
      {
        mImageProvider = new FileImage(file);
      });
    }
  }

  /*
   * 相机拍照选择照片
   */
  Future<File> doClickPhotoCamera()
  async {
    File file = await ImagePicker.pickImage(source: ImageSource.camera, imageQuality: 80);
    if (file != null)
    {
      setState(()
      {
        mImageProvider = new FileImage(file);
      });
    }
  }
}