package com.zerogis.plugin;

import android.app.Activity;
import android.content.Context;
import com.zerogis.zcommon.third.eventbus.EventBus;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BasicMessageChannel;
import io.flutter.plugin.common.StringCodec;

/**
 * BasicMessageChannel（双向通信）
 */
public class FlutterBasicMessagePlugin implements BasicMessageChannel.MessageHandler<String>, FlutterPlugin
{
    private Context ctx;

    private BasicMessageChannel<String> basicMessageChannel;

    @Override
    public void onAttachedToEngine( FlutterPluginBinding binding)
    {
        basicMessageChannel = new BasicMessageChannel(binding.getBinaryMessenger(), "FlutterBasicMessageChannel", StringCodec.INSTANCE);
        basicMessageChannel.setMessageHandler(this);
        ctx = binding.getApplicationContext();
    }

    @Override
    public void onDetachedFromEngine( FlutterPluginBinding binding)
    {
        basicMessageChannel.setMessageHandler(null);
        basicMessageChannel = null;
    }

    @Override
    public void onMessage(String message,  BasicMessageChannel.Reply<String> reply)
    {
        reply.reply("回调到flutter原生中去");
    }

    /**
     * 发送消息
     * @param message 消息对象
     * @param callback 回调
     */
    public void send(String message, final BasicMessageChannel.Reply<String> callback)
    {
        basicMessageChannel.send(message, callback);
    }
}
